<?php
/**
 * Image helper functions
 *
 * @author Chris
 */
class ImageHelper {

    /**
     * Create a thumbnail of an image and returns relative path in webroot
     *
     * @param int $width
     * @param int $height
     * @param string $img
     * @param int $quality
     * @return string $path
     */
    public static function thumb($width, $height, $img, $quality = 75)
    {
        $pathinfo = pathinfo($img);
        $thumb_name = "thumb_".$pathinfo['filename'].'_'.$width.'_'.$height.'.'.$pathinfo['extension'];
        $thumb_path = $pathinfo['dirname'].DIRECTORY_SEPARATOR.'.tmb'.DIRECTORY_SEPARATOR;
        if(!file_exists($thumb_path))
        {
            //mkdir($thumb_path);
            self::rmkdir($thumb_path, 0777);
        }
		
        if(!file_exists($thumb_path.$thumb_name) || filemtime($thumb_path.$thumb_name) > filemtime($img)){

            $image = Yii::app()->image->load($img);

            $image->resize($width, $height)->quality($quality);
            $image->save($thumb_path.$thumb_name);
        }
        $relative_path = str_replace(YiiBase::getPathOfAlias('webroot'), '', $thumb_path.$thumb_name);
        
        return $relative_path;
    }
	
    public static function rmkdir($path, $mode = 0755) {
        $path = rtrim(preg_replace(array("/\\\\/", "/\/{2,}/"), "/", $path), "/");
        $e = explode("/", ltrim($path, "/"));
        if(substr($path, 0, 1) == "/") {
                $e[0] = "/".$e[0];
        }
        $c = count($e);
        $cp = $e[0];
        for($i = 1; $i < $c; $i++) {
                if(!is_dir($cp) && !@mkdir($cp, $mode)) {
                        return false;
                }
                $cp .= "/".$e[$i];
        }
        return @mkdir($path, $mode);
    }
}