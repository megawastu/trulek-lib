<?php

class UniqueAttributeValueValidator extends CValidator {
    
    protected function validateAttribute($model, $attribute) {
        $tableName = $model->tableName();
        $value = $model->$attribute;
        $id = $model->id;

        $sql  = "SELECT COUNT(`id`) FROM `".$tableName."` AS `t`";
        $sql .= " WHERE `t`.`".$attribute."`='".$value."'";
        $sql .= " AND `t`.`id` <> '".$id."'";

        try {
            $numberOfRecord = Yii::app()->db->createCommand($sql)->queryScalar();
            
            if ($numberOfRecord >= 1)
            {
               $errorMessage = Yii::t('system', $attribute.' '.'__ERRORUNIQUENAME');
               $model->addError($attribute, $errorMessage);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
        }
    }
    
}
