<?php

class DataDeleteBehavior extends CActiveRecordBehavior
{
    public $mode = null;
    
    //example: $xrefModels['ArticleCategoryXref']='article_category_id';
    public $xrefModels = array();
    public $relatedModel = null;
    public $relatedModelForeignKey = null;
    
    public function beforeDelete($event)
    {
        try {
            if ($this->mode == "cascade") {
               $this->_deleteXrefs();
            } else {
                $event->isValid = false;
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }

        return parent::beforeDelete($event);
    }

    private function _deleteXrefs()
    {
        if ($this->xrefModels != null && is_array($this->xrefModels) && count($this->xrefModels) > 0)
        {
            foreach ($this->xrefModels as $className => $fieldName)
            {
                $refMethod = new ReflectionMethod($className, 'model');
                if ($this->mode == "cascade") {
                    $xrefs = $refMethod->invoke(NULL)->findAllByAttributes(array($fieldName => $this->owner->id));
                    $relatedModelForeignKey = $this->relatedModelForeignKey;
                    foreach ($xrefs as $xref)
                    {
                        $rm = new ReflectionMethod($this->relatedModel, 'model');
                        $rm->invoke(NULL)->deleteByPk($xref->$relatedModelForeignKey);

                    }
                }
                $refMethod->invoke(NULL)->deleteAllByAttributes(array($fieldName => $this->owner->id));
            }
        }
    }
}
