<?php

class MappingGeneratorBehavior extends CActiveRecordBehavior
{
    public function beforeSave($event)
    {
        if ($this->owner->isNewRecord) {
           $this->owner->id = md5(uniqid());
        }
    }
}
