<?php

class AutoTimestampBehavior extends CActiveRecordBehavior {

    public function beforeSave($event) {

        if ($this->owner->isNewRecord === true) {
           $this->owner->date_created = date('Y-m-d H:i:s');
        }
        
        if ($this->owner->isNewRecord === false) {
           $this->owner->last_updated = date('Y-m-d H:i:s');
        }
    }
}
