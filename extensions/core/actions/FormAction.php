<?php

class FormAction extends CAction
{
    public $modelFormName;
    public $modelName;
    public $relations = array();
    public $listData = array();
	public $showButtons = true;
    
    public function run()
    {
       $formClass = new ReflectionClass($this->modelFormName);
       $form = $formClass->newInstance();

       $id = isset($_GET['id']) ? $_GET['id'] : null;       

       if ($id != null && strlen($id) == 32) {
           $refMethod = new ReflectionMethod($this->modelName, 'model');
           $modelInstance = $refMethod->invoke(NULL)->findByPk($id);
           $form->setAttributes($modelInstance->attributes, false);
           $form->id = $modelInstance->id;

           if (count($this->relations) > 0) {
               foreach ($this->relations as $relation)
               {
                   if (isset($modelInstance->$relation) && is_array($modelInstance->$relation)) //RELATIONSHIP: MANY_MANY & HAS_MANY
                   {
                       $rel_ids = $modelInstance->$relation;
                       $arr_ids = array();
                       foreach ($rel_ids as $rel_id)
                       {
                           $arr_ids[] = $rel_id;
                       }
                       $form->$relation = $arr_ids;
                   } 
                   else if (isset($modelInstance->$relation) && !is_array($modelInstance->$relation))
                   {
                       $obj = $modelInstance->$relation;
                       $form->$relation = $obj->id;
                   }
                   else if (!isset($modelInstance->$relation))
                   {
                       echo '<pre>';
                          print_r($modelInstance->attributes);
                       echo '</pre>';
                       exit();
                   }
               }
           }
       }

       $data['form'] = $form;
       $data['messages'] = array($form);
       //handle list data
       if (count($this->listData) > 0)
       {
           foreach ($this->listData as $name => $listData)
           {
               $data[$name] = $listData;
           }
       }
	   if ($this->showButtons === true) {
          Yii::app()->controller->listFormButtonClient();
	   }
       Yii::app()->controller->render('form', ( isset($data) && count($data) > 0 ) ? $data : '');
    }
}
