<?php

class PublishAction extends CAction
{
    public $modelName;
    public $redirectUrl = null;
    
    public function run()
    {
       $id = $_GET['id'];

       Yii::app()->crudService->publish($this->modelName, $id);
       
       Yii::app()->user->setFlash('success', Yii::t('system', 'Data was published'));

       if ($this->redirectUrl == null) {
          Yii::app()->controller->redirect(Yii::app()->homeUrl.Yii::app()->controller->module->name.'/'.Yii::app()->controller->id.'/index');
       } else {
           Yii::app()->controller->redirect($this->redirectUrl);
       }
    }
}
