<?php

class UnpublishAction extends CAction
{
    public $modelName;
    public $redirectUrl = null;
    
    public function run()
    {
       $id = $_GET['id'];

       Yii::app()->crudService->unpublish($this->modelName, $id);
       
       Yii::app()->user->setFlash('success', Yii::t('system', 'Data was unpublished'));
       
       if ($this->redirectUrl == null) {
          Yii::app()->controller->redirect(Yii::app()->homeUrl.Yii::app()->controller->module->name.'/'.Yii::app()->controller->id.'/index');
       } else {
           Yii::app()->controller->redirect($this->redirectUrl);
       }
    }
}
