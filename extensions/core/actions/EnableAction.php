<?php

class EnableAction extends CAction
{
    public $modelName;
    
    public function run()
    {
       $id = $_GET['id'];

       Yii::app()->crudService->enable($this->modelName, $id);
       
       Yii::app()->user->setFlash('success', Yii::t('system', 'Data was published'));
       Yii::app()->controller->redirect(Yii::app()->homeUrl.Yii::app()->controller->module->name.'/'.Yii::app()->controller->id.'/index');
    }
}
