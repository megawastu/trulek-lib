<?php

class CRUDService extends CApplicationComponent {

    /**
     *
     * @param String $className
     * @param String $id 
     * @return Object $modelInstance
     */
    public function create(/**String**/ $className, $params=array() )
    {
        $refClass = new ReflectionClass($className);
        $modelInstance = $refClass->newInstance();
        $modelInstance->setAttributes($params, false);
        
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    /**
     *
     * @param String $className
     * @param String $id 
     * @return Object $modelInstance
     */
    public function update(/**String**/ $className, $params=array())
    {
        $id = $params['id'];
        $refMethod = new ReflectionMethod($className, 'model');
        $modelInstance = $refMethod->invoke(NULL)->findByPk($id);
        $modelInstance->setAttributes($params, false);
        
        try 
        {
            $modelInstance->save();
            
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    /**
     *
     * @param String $className
     * @param String $id 
     * @return void
     */
    public function delete( /**String**/ $className, $id ) {
        try {
            $refMethod = new ReflectionMethod($className, 'model');            
            $refMethod->invoke(NULL)->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
    
    /**
     *
     * @param String $className
     * @param String $id 
     * @return void
     */
    public function publish(/**String**/ $className, $id)
    {
        try {
            
            $refMethod = new ReflectionMethod($className, 'model');
            $modelInstance = $refMethod->invoke(NULL)->findByPk($id);
            $modelInstance->published = 1;
            $modelInstance->save();
        
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
    
    /**
     *
     * @param String $className
     * @param String $id 
     * @return void
     */
    public function unpublish(/**String**/ $className, $id)
    {
        try {
            
            $refMethod = new ReflectionMethod($className, 'model');
            $modelInstance = $refMethod->invoke(NULL)->findByPk($id);
            $modelInstance->published = 0;
            $modelInstance->save();
        
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    /**
     *
     * @param String $className
     * @param String $id
     * @return void
     */
    public function enable(/**String**/ $className, $id)
    {
        try {

            $refMethod = new ReflectionMethod($className, 'model');
            $modelInstance = $refMethod->invoke(NULL)->findByPk($id);
            $modelInstance->enabled = 1;
            $modelInstance->save();

        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    /**
     *
     * @param String $className
     * @param String $id
     * @return void
     */
    public function disable(/**String**/ $className, $id)
    {
        try {

            $refMethod = new ReflectionMethod($className, 'model');
            $modelInstance = $refMethod->invoke(NULL)->findByPk($id);
            $modelInstance->enabled = 0;
            $modelInstance->save();

        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
    
    /**
     *
     * @param String $tableName
     * @return void 
     */
    public function getNextOrdering(/**String**/ $tableName)
    {
        $sql = "SELECT MAX(`ordering`) FROM `".$tableName."`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
