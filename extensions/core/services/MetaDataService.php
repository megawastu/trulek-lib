<?php

class MetaDataService extends CApplicationComponent {

    public function defaultPageTitle($separator="::")
    {
        $pageTitle = null;
        
        $appName = Yii::app()->name;
        $module = null;
        $controller_id = ucfirst(Yii::app()->controller->id);
        $action_id = null;
                
        if (Yii::app()->controller->module != null) {
            $module = ucfirst(Yii::app()->controller->module->name);
        }
        
        if ($module != null)
        {
            if (strtolower($controller_id) != "index") {
               $pageTitle = $module.' '.$separator.' '.$controller_id;
            } else {
               $pageTitle = $module;   
            }            
        }
        else
        {
            $pageTitle = $controller_id;
        }
        
        return $appName.' '.'&raquo;'.' '.$pageTitle;        
    }
    
    public function getSitePageTitle()
    {
        $controller = strtolower(Yii::app()->controller->id);
        $module = null;
        if (Yii::app()->controller->module != null)
        {
            $module = Yii::app()->controller->module->id;
        }
        $urlkey = isset($_GET['urlkey']) ? $_GET['urlkey'] : null;
		$brand = isset($_GET['brand']) ? $_GET['brand'] : null;
        $pageTitle = '';
        
        if ($module == null) {
            switch ($controller) {
                case "event":
                    if (Yii::app()->controller->action->id == "view") {
                        if ($urlkey != null) {
                            $obj = Event::model()->findByAttributes(array('urlkey' => $urlkey));
                            $pageTitle = $obj->name;
                        } else {
                            $pageTitle = Yii::t('system', 'Event');
                        }
                    } else if (Yii::app()->controller->action->id == "index" || Yii::app()->controller->action->id == "browse") {
                        $pageTitle = Yii::t('system', 'See All Events');
                    }
                    break;
                case "article":
                    if (Yii::app()->controller->action->id == "index") {
                        if ($urlkey != null) {
                            $obj = Article::model()->findByAttributes(array('urlkey' => $urlkey));
                            if ($obj != null) {
                                $pageTitle = $obj->title;
                            }
                        } else {
                            $pageTitle = Yii::t('system', 'Article');
                        }
                    } else if (Yii::app()->controller->action->id == "category") {
                        if ($urlkey != null) {
                            $obj = ArticleCategory::model()->findByAttributes(array('urlkey' => $urlkey));
                            if ($obj != null) {
                                $pageTitle = $obj->name;
                            }
                        } else {
                            $pageTitle = Yii::t('system', 'Article Category');
                        }

                    }
                    break;
                case "client":
                case "contact":
                case "sitemap":
                    $pageTitle = Yii::t('system', ucfirst($controller));
                    break;
                case "index":
                default:
                    $pageTitle = Yii::t('system', 'Home');
                    break;
            }
        } else {
            switch ($module)
            {
                case "catalog":
                    if (Yii::app()->controller->id == "product" && Yii::app()->controller->action->id == "index") {
                        if ($urlkey != null) {
                            $obj = CatalogProduct::model()->findByAttributes(array('urlkey' => $urlkey));
                            if ($obj != null) {
                                $pageTitle = $obj->name;
                            }
                        } else {
                            $pageTitle = Yii::t('system', 'Catalog Product');
                        }
                    } else if (Yii::app()->controller->id == "category" && Yii::app()->controller->action->id == "index") {
                        if ($urlkey != null) {							
							$obj = CatalogCategory::model()->findByAttributes(array('urlkey' => $urlkey));
						    if ($obj != null) {
								$pageTitle = $obj->name;								
						    }
                        } else {
                            $pageTitle = Yii::t('system', 'Catalog Category');
                        }

                    }
                    break;
                case "":
                default:
                    break;
            }
        }
        return $pageTitle;
    }
    
    public function getSiteMetaDescription()
    {
        $metaDesc = null;
        
        $controller = strtolower(Yii::app()->controller->id);
        $module = null;
        if (Yii::app()->controller->module != null)
        {
            $module = Yii::app()->controller->module->id;
        }
        $urlkey = isset($_GET['urlkey']) ? $_GET['urlkey'] : null;

        if ($module == null) {
            if ($urlkey != null) {
                switch ($controller) {
                    case "event":
                        if (Yii::app()->controller->action->id == "view") {
                            $obj = Event::model()->findByAttributes(array('urlkey' => $urlkey));
                            if (isset($obj->meta_desc)) {
                               $metaDesc = $obj->meta_desc;
                            }
                        }
                        break;
                    case "article":
                        if (Yii::app()->controller->action->id == "index") {
                            $obj = Article::model()->findByAttributes(array('urlkey' => $urlkey));
                            if (isset($obj->meta_desc)) {
                               $metaDesc = $obj->meta_desc;
                            }
                        } else if (Yii::app()->controller->action->id == "category") {
                            $obj = ArticleCategory::model()->findByAttributes(array('urlkey' => $urlkey));
                            if (isset($obj->meta_desc)) {
                               $metaDesc = $obj->meta_desc;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        } else {
            switch ($module)
            {
                case "catalog":
                    if (Yii::app()->controller->id == "product" && Yii::app()->controller->action->id == "index") {
                        $obj = CatalogProduct::model()->findByAttributes(array('urlkey' => $urlkey));
                        if (isset($obj->meta_desc)) {
                           $metaDesc = $obj->meta_desc;
                        }
                    } else if (Yii::app()->controller->id == "category" && Yii::app()->controller->action->id == "index") {
                        $obj = CatalogCategory::model()->findByAttributes(array('urlkey' => $urlkey));
                        if (isset($obj->meta_desc)) {
                           $metaDesc = $obj->meta_desc;
                        }
                    }
                    break;
                case "":
                default:
                    break;
            }
        }
        
        if ($metaDesc == null) {
             $metaDesc = Yii::app()->settingService->getSetting()->meta_desc;
        }
        
        return $metaDesc;
    }
    
    public function getSiteMetaKeyword()
    {
        $metaKeyword = null;
        
        $controller = strtolower(Yii::app()->controller->id);
        $module = null;
        if (Yii::app()->controller->module != null)
        {
            $module = Yii::app()->controller->module->id;
        }
        $urlkey = isset($_GET['urlkey']) ? $_GET['urlkey'] : null;

        if ($module == null) {
            if ($urlkey != null) {
                switch ($controller) {
                    case "event":
                        if (Yii::app()->controller->action->id == "view") {
                            $obj = Event::model()->findByAttributes(array('urlkey' => $urlkey));
                            if (isset($obj->meta_keyword)) {
                                $metaKeyword = $obj->meta_keyword;
                            }
                        }
                        break;
                    case "article":
                        if (Yii::app()->controller->action->id == "index") {
                            $obj = Article::model()->findByAttributes(array('urlkey' => $urlkey));
                            if (isset($obj->meta_keyword)) {
                                $metaKeyword = $obj->meta_keyword;
                            }
                        } else if (Yii::app()->controller->action->id == "category") {
                            $obj = ArticleCategory::model()->findByAttributes(array('urlkey' => $urlkey));
                            if (isset($obj->meta_keyword)) {
                                $metaKeyword = $obj->meta_keyword;
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        } else {
            switch ($module)
            {
                case "catalog":
                    if (Yii::app()->controller->id == "product" && Yii::app()->controller->action->id == "index") {
                        $obj = CatalogProduct::model()->findByAttributes(array('urlkey' => $urlkey));
                        if (isset($obj->meta_keyword)) {
                            $metaKeyword = $obj->meta_keyword;
                        }
                    } else if (Yii::app()->controller->id == "category" && Yii::app()->controller->action->id == "index") {
                        $obj = CatalogCategory::model()->findByAttributes(array('urlkey' => $urlkey));
                        if (isset($obj->meta_keyword)) {
                            $metaKeyword = $obj->meta_keyword;
                        }
                    }
                    break;
                default:
                    break;
            }
        }
        
        if ($metaKeyword == null) {
            $metaKeyword = Yii::app()->settingService->getSetting()->meta_keyword;
        }
        
        return $metaKeyword;
    }
}
