<?php

class MessageLoggingService extends CApplicationComponent {

    public function log($messages = array(), $output_filename)
    {
        $outputTemplate = "Logged on ".date("Y-m-d H:i:s").": {msg}\n";
        $stringData = '';
        $previousData = '';

        //uncomment to see output log file
        $myFile = Yii::app()->basePath.'/'."runtime".'/'.$output_filename.".log";

        //get previous data
        $fh = fopen($myFile, 'r') or die("can't open file");
        if (filesize($myFile) > 0) {
            $previousData = fread($fh, filesize($myFile));
        }
        fclose($fh);

        if (count($messages) > 0) {
            foreach ($messages as $message) {
                $stringData .= str_replace("{msg}", $message, $outputTemplate);
            }
        }

        $data = $previousData.$stringData;

        $fh = fopen($myFile, 'w') or die("can't open file");
        fwrite($fh, $data);
        fclose($fh);
    }
}
