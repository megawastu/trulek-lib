<?php

class RoleEnum {
    const ROLE_SUPER_ADMINISTRATOR = "ROLE_SUPER_ADMINISTRATOR";
    const ROLE_ADMINISTRATOR = "ROLE_ADMINISTRATOR";

    static function getList()
    {
        return array(self::ROLE_SUPER_ADMINISTRATOR => self::ROLE_SUPER_ADMINISTRATOR,
                     self::ROLE_ADMINISTRATOR => self::ROLE_ADMINISTRATOR);
    }
}
