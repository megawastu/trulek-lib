<?php

class BrowserNavEnum {
    const _BLANK = "_blank";
    const _PARENT = "_parent";
    const _SELF = "_self";

    static function getList()
    {
        return array(self::_SELF => self::_SELF,
                     self::_BLANK => self::_BLANK,
                     self::_PARENT => self::_PARENT);
    }
}