<?php

Yii::import('trulek.cms.core.setting.models.Setting');

class ContactForm extends CFormModel {

    public $first_name;
    public $last_name;
    public $email;
    public $subject;
    public $message;
    public $recaptcha;

    public function rules()
    {
        return array(
            array('first_name, last_name, email, subject, message', 'required'),
            array('email', 'email'),
            array('recaptcha',
                    'application.extensions.recaptcha.EReCaptchaValidator',
                    'privateKey'=>'6Ld90sQSAAAAAIbZ3hv1DXbA2VRm_H9AkUiV70Cw')
			);
    }
    
    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'first_name' => Yii::t('system', 'First Name'),
           'last_name' => Yii::t('system', 'Last Name'),
           'email' => Yii::t('system', 'Email'),
           'subject' => Yii::t('system', 'Subject'),
           'message' => Yii::t('system', 'Message'),
           'recaptcha' => Yii::t('system', 'Recaptcha'),
       );
   }
    
    /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'first_name' => 'first_name',
           'last_name' => 'last_name',
           'email' => 'email',
           'subject' => 'subject',
           'message' => 'message',
           'recaptcha' => 'recaptcha',
       );
   }
    
}
