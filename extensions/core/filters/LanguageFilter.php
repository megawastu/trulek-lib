<?php

class LanguageFilter extends CFilter
{
    protected function preFilter($filterChain) // logic being applied before the action is executed
    {    
        if (Yii::app()->user->hasState('_lang'))
        {
            Yii::app()->setLanguage(Yii::app()->user->_lang);
        }
		
        //check languages
        if (Yii::app()->request->isPostRequest)
        {
		    if (isset($_POST['_lang'])) {
                            Yii::app()->setLanguage($_POST['_lang']);
                            Yii::app()->user->setState('_lang', Yii::app()->language);
                    }
        }
		
        return true; // false if the action should not be executed
    }

    protected function postFilter($filterChain)
    {
        // logic being applied after the action is executed
    }
}
