<?php

class TemplateFilter extends CFilter
{
    protected function preFilter($filterChain) // logic being applied before the action is executed
    {
        Yii::app()->setTheme('default');
        
        return true; // false if the action should not be executed
    }

    protected function postFilter($filterChain)
    {
        // logic being applied after the action is executed
    }
}
