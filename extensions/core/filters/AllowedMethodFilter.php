<?php

class AllowedMethodFilter extends CFilter {

    public $allowedMethods = array();
    
    protected function preFilter($filterChain) // logic being applied before the action is executed
    {
        $allowed = true;
        if (count($this->allowedMethods) > 0) {
            foreach ($this->allowedMethods as $actionName => $allowedMethods) {
                if (Yii::app()->controller->action->id == $actionName) {
                    foreach ($allowedMethods as $allowedMethod) {
                        if ($allowedMethod == "GET") {
                            if (count($_GET) > 0) {
                                $allowed = $allowed && true;
                            } else {
                                $allowed = $allowed && false;
                            }
                        }

                        if ($allowedMethod == "POST") {
                            if (Yii::app()->request->isPostRequest) {
                                $allowed = $allowed && true;
                            } else {
                                $allowed = $allowed && false;
                            }
                        }
                    }

                    if ($allowed === false) {
                        throw new CHttpException('402', Yii::t('system', 'Method not allowed'));
                    }
                }
            }
        }

        return true; // false if the action should not be executed
    }

    protected function postFilter($filterChain)
    {
        // logic being applied after the action is executed
    }
    
}