<?php

Yii::import('trulek.extensions.fileuploader.enums.UploadTypeEnum');
Yii::import('trulek.extensions.fileuploader.forms.FileUploadForm');

class FileUploadService extends CApplicationComponent {

    public $uploadType = UploadTypeEnum::IMAGE;

    public function uploadFile(CUploadedFile $file, $uploadPath, $new_file_name='')
    {
        $saved = false;
        if ($file != null) {
            if (file_exists($uploadPath) && is_dir($uploadPath))
            {
                $fname = $file->name;
                if ($new_file_name != "")
                {
                    $fname = $new_file_name;
                }
                $source = $uploadPath.DIRECTORY_SEPARATOR.$fname;
                $saved = $file->saveAs($source) && $saved;
            }
            else
            {
              throw new Exception("Upload directory not found. Please review: ".$uploadPath);
            }
        }

        return $saved;
    }

    public function deleteFile($fileName, $uploadPath)
    {
        $filePath = $uploadPath.DIRECTORY_SEPARATOR.$fileName;
        if (is_file($filePath) && file_exists($filePath)) {
            unlink($filePath);
        }
        else {
            throw new Exception('Invalid file was specified. Check your file path: '.$filePath);
        }
    }
    
    /**
     * @deprecated
     * @param type $files
     * @param type $uploadPath
     * @param type $uploadType
     * @return type
     */
    public function uploadFiles($files = array(), $uploadPath, $uploadType=UploadTypeEnum::IMAGE)
    {
        $this->uploadType = $uploadType;

        $valid = $this->_validateUploadedFiles($files);

        if ($valid === true)
        {
            $saved = $this->_saveUploadedFilesToDisk($files, $uploadPath);
            return $saved;
        }
        else
        {
            $messages = array();
            $messages = $valid;
            return $messages; //array of error messages
        }
    }

    /**
     * @deprecated
     * @param type $files
     * @return FileUploadForm 
     */
    private function _validateUploadedFiles($files=array())
    {
        $messages = array();
        if ($files != null && count($files) > 0)
        {
            foreach ($files as $file)
            {
                $form = new FileUploadForm();
                switch ($this->uploadType)
                {
                    case UploadTypeEnum::DOCUMENT:
                        $form->document = $file;
                        break;
                    case UploadTypeEnum::VIDEO:
                        $form->video = $file;
                        break;
                    case UploadTypeEnum::IMAGE:
                    default:
                        $form->image = $file;
                        break;
                }

                $check = $form->validate();
                if ($check === false)
                {
                    $messages[] = $form;
                }
                else
                {
                    $name = $file->name;
                    switch ($this->uploadType)
                    {
                        case UploadTypeEnum::DOCUMENT:
                            $form->document = $name;
                            break;
                        case UploadTypeEnum::VIDEO:
                            $form->video = $name;
                            break;
                        case UploadTypeEnum::IMAGE:
                        default:
                            $form->image = $name;
                            break;
                    }
                }
            }
        }

        if (count($messages) > 0)
        {
            return $messages; //return array
        }
        else
        {
            return true; //return boolean
        }
    }

    /**
     * @deprecated
     * @param type $files
     * @param type $uploadPath
     * @return type 
     */
    private function _saveUploadedFilesToDisk($files, $uploadPath)
    {
        $saved = false;
        if ($files != null && count($files) > 0)
        {
            if (file_exists($upload_path) && is_dir($upload_path))
            {
                //could cause trouble in non-windows OS
//                if (!is_writeable($upload_path)) {
//                    $chmoded = chmod($upload_path, 0644);
//                }

                foreach ($files as $file)
                {
                    //saves the file first if there are any
                    if ($file != null)
                    {
                        $source = $uploadPath.DIRECTORY_SEPARATOR.$file->name;
                        $saved = $file->saveAs($source) && $saved;
                    }
                }

                return $saved; //return boolean
            }
            else
            {
                echo Yii::t('system', 'Upload path not found. Please review the upload path: '.$uploadPath);
                exit();
            }
        }
    }
}
