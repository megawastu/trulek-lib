<?php

class UploadableFormBehavior extends CModelBehavior {
    
    public $old_file = null;
    
    public function beforeValidate($event)
    {
        //deal with old file
        if ($this->owner->old_file != null)
        {
            $this->owner->file = $this->owner->old_file;
        }
        else {
            $this->owner->file = constant("NULL");
        }
        
        //deal with new file
        $new_file = CUploadedFile::getInstance($this->owner, 'file');        
        if (!is_null($new_file))
        {
            $this->owner->file = $new_file;
        }
        
        //if we have old file but also a new file, replace the old one with the new.
        if ($this->owner->old_file != null && $new_file != null)
        {
            $this->_deleteOldFile();
        }        
        
        //the file is going to be deleted
        if ($this->owner->delete_file > 0)
        {
            try 
            {
                $this->_deleteOldFile();
            } catch (Exception $e) {
                echo $e->getMessage();
                exit();
            }
            $this->owner->file = constant("NULL");
        }
    }
    
    private function _deleteOldFile()
    {
        $objOldFile = UploadedFile::model()->findByPk($this->owner->old_file);
        if ($objOldFile != null) {
            $file_type = null;
            $arr = explode("/", $objOldFile->file_type);
            switch ($arr[0])
            {
                case "image":
                default:
                    $file_type = "images";
                    break;
            }
            $uploadPath = '../'.Yii::app()->params['uploadFolder']."/".Yii::app()->user->saasClient->site_address."/".$file_type;
            Yii::app()->fileUploadService->deleteFile($objOldFile->file_name, $uploadPath);
            $objOldFile->delete();
        }
    }
}