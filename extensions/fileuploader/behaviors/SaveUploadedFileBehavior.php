<?php

Yii::import('trulek.extensions.fileuploader.models.UploadedFile');

class SaveUploadedFileBehavior extends CActiveRecordBehavior {
    
    public function beforeSave($event)
    {
        $this->_savefile();
    }
    
    public function afterDelete($event)
    {
        $this->_deleteFile();
    }
    
    private function _saveFile()
    {
        $file = $this->owner->file;
        if ($file != null && ($file instanceof CUploadedFile) === true) {
            
            //get original file name
            $original_file_name = $file->name;
            
            //rename file
            $salt = time();
            $new_name = md5($salt.$this->_getFileName($file->name));
            $ext = $this->_getFileExtension($file->name);
            $new_name = $new_name . "." . $ext;
            
            //save to disk
            $file_type = null;
            $arr = explode("/", $file->type);
            switch ($arr[0])
            {
                case "image":
                default:
                    $file_type = "images";
                    break;
            }
            
            $uploadPath = "../".Yii::app()->params['uploadFolder'].'/'.Yii::app()->user->saasClient->site_address."/".$file_type;

            Yii::app()->fileUploadService->uploadFile($file, $uploadPath, $new_name);
            
            //save to database
            $uploadedFile = new UploadedFile();
            $uploadedFile->original_file_name = $original_file_name;
            $uploadedFile->file_name = $new_name;
            $uploadedFile->file_type = $file->type;
            $uploadedFile->file_size = $file->size;
            $uploadedFile->upload_path = constant("NULL");
            $uploadedFile->caption = constant("NULL");
            
            if ($uploadedFile->save()) {
               $this->owner->file = $uploadedFile->id;
            }
            return true;
        }        
        return false;
    }
    
    private function _deleteFile()
    {
        $objFile = UploadedFile::model()->findByPk($this->owner->file);
        if ($objFile != null) {
            $file_type = null;
            $arr = explode("/", $objFile->file_type);
            switch ($arr[0])
            {
                case "image":
                default:
                    $file_type = "images";
                    break;
            }
            $uploadPath = '../'.Yii::app()->params['uploadFolder']."/".Yii::app()->user->saasClient->site_address."/".$file_type;
            Yii::app()->fileUploadService->deleteFile($objFile->file_name, $uploadPath);
            $objFile->delete();
        }
    }
    
    private function _getFileName($file_name)
    {
        $str = null;
        $arr = explode(".", $file_name);
        $str = $arr[0];
        return $str;
    }
    
    private function _getFileExtension($file_name)
    {
        $str = null;
        $arr = explode(".", $file_name);
        $str = $arr[1];
        return $str;
    }
}