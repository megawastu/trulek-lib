<?php

Yii::import('application.modules.security.filters.SecurityMapFilter');
Yii::import('application.extensions.components.Button');
Yii::import('trulek.cms.modules.catalog.models.*');
Yii::import('trulek.cms.modules.catalog.services.*');
Yii::import('trulek.extensions.fileuploader.enums.UploadTypeEnum');
Yii::import('trulek.extensions.fileuploader.models.UploadedFile');
Yii::import('trulek.extensions.core.behaviors.MappingGeneratorBehavior');
Yii::import('trulek.extensions.core.behaviors.AutoTimestampBehavior');

class UploadifyFrontController extends CExtController {

    public function init()
    {
       if (Yii::app()->request->isPostRequest) {
           if(isset($_POST['SESSION_ID'])) {
                $session=Yii::app()->getSession();
                $session->close();
                $session->sessionID = $_POST['SESSION_ID'];
                $session->open();
           }    
       }
    }
    
    public function actionUpload()
    {
        if (isset($_POST['uploaded_files'])) {
            $this->_multipleUploads();
        } else if (isset($_POST['uploaded_file'])) {
            $this->_singleUpload();
        }
    }
    
    public function actionCheck()
    {
        if (Yii::app()->request->isPostRequest) {
            $fileArray = array();
            foreach ($_POST as $key => $value) {
                if ($key != 'folder') 
                {
                    $file = urldecode($_POST['folder']). '/' . $value;
                    if (file_exists($file)) //if file existed
                    {   
                        $fileArray[$key] = $value;
                    }
                }
            }
            $data['fileArray'] = $fileArray;
            $this->renderPartial('upload/_check', ( isset($data) && count($data) > 0 ) ? $data : '');
        }
    }

    public function filters()
    {
        return array(
            array(
                'application.modules.security.filters.SecurityMapFilter',
            ),
            array(
                'trulek.extensions.core.filters.AllowedMethodFilter',
                    'allowedMethods' => array(
                                            'upload' => array('POST'),
                                            'check' => array('POST'),
                    ),
            ),
        );
    }
    
    private function _multipleUploads()
    {
        //$targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
        $targetPath = str_replace('/catalog/product/form/id', '', urldecode($_REQUEST['folder'])) . '/';
        $uploadedFiles = CUploadedFile::getInstancesByName('uploaded_files');
        $output = '';
        if ($uploadedFiles != null && count($uploadedFiles) > 0)
        {
            foreach ($uploadedFiles as $uploadedFile)
            {
                $original_file_name = $uploadedFile->name;
                $salt = time();
                $renamed = md5($salt.$uploadedFile->name);
                $file_name = $renamed.'.'.$uploadedFile->extensionName;
                $new_file = $targetPath.$file_name;
                
                $saved = $uploadedFile->saveAs($new_file);
                $output .= str_replace($_SERVER['DOCUMENT_ROOT'], '', $new_file).'<br />';
//                if ($saved) {                    
//                    $uf = new UploadedFile();
//                    $uf->original_file_name = $original_file_name;
//                    $uf->file_name = $file_name;
//                    $uf->file_type = $uploadedFile->type;
//                    $uf->file_size = $uploadedFile->size;
//                    $uf->upload_path = constant("NULL");
//                    $uf->caption = constant("NULL");
//                    $uf->save();
//                }
            }
            echo $output;
        }
    }
    
    private function _singleUpload()
    {
        $targetPath = $_SERVER['DOCUMENT_ROOT'] . $_REQUEST['folder'] . '/';
        $uploadedFile = CUploadedFile::getInstanceByName('uploaded_file');
        $output = '';
        if ($uploadedFile != null && $uploadedFile instanceof CUploadedFile)
        {
            $file_name = md5($uploadedFile->name);
            $new_file = $targetPath.'/'.$file_name.'.'.$uploadedFile->extensionName;
            $uploadedFile->saveAs($new_file);
            $output = str_replace($_SERVER['DOCUMENT_ROOT'], '', $new_file).'<br />';
            echo $output;
        }
    }
}
