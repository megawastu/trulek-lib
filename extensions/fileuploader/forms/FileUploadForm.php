<?php

class FileUploadForm extends CFormModel {

   public $document;
   public $image;
   public $video;

   /**
    *
    * @return <Array>
    */
   public function rules()
   {
       return array(
          array('image', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
          array('document', 'file', 'types'=> 'pdf',
                                'maxSize'=>1024 * 1024 * 25, // 25MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .pdf'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 25MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
          array('video', 'file', 'types'=> 'avi, mp4, swf, mpg, flv, mov',
                                'maxSize'=>1024 * 1024 * 80, // 80MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .avi, .mp4, .swf, .mpg, .flv, .mov'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 80MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
       );
   }

   /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'image' => Yii::t('catalogs', 'Image'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'image' => 'image',
       );
   }

}
