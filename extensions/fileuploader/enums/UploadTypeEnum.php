<?php

class UploadTypeEnum {
    const DOCUMENT = "DOCUMENT";
    const IMAGE = "IMAGE";
    const VIDEO = "VIDEO";

    static function getList()
    {
        return array(self::DOCUMENT => self::DOCUMENT,
                     self::IMAGE => self::IMAGE,
                     self::VIDEO => self::VIDEO
                    );
    }
}
