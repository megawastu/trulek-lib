<?php

class UploadedFile extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'core_uploaded_file';
    }

    /**
     * Method to defined rules for validation
     * @return Array
     */
    public function rules()
    {
        return array(
            array('original_file_name, file_name, file_type, file_size', 'required'),
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}