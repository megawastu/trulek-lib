<?php

class PhotoForm extends CFormModel
{
    public $id;
    public $label;
    public $album_id;
    public $file;
    public $delete_file;
    
    public function rules()
    {
        return array(
           array('label, album_id', 'required'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'label' => Yii::t('system', 'Label'),
           'album_id' => Yii::t('system', 'Album'),
           'file' => Yii::t('system', 'File'),
           'delete_file' => Yii::t('system', 'Delete'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'label' => 'label',
           'album_id' => 'album',
           'file' => 'file',
           'delete_file' => 'delete_file',
       );
   }
   
   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
}
