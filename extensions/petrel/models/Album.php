<?php

Yii::import('application.modules.petrel.models.Photo');
class Album extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'petrel_album';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
           'account'=>array(self::BELONGS_TO, 'Account', 'account_id'),
           'photos' => array(self::HAS_MANY, 'Photo', 'album_id'),
           'photoCount' => array(self::STAT, 'Photo', 'album_id'), 
        );
    }
    
    public function scopes()
    {
        return array(
            
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}