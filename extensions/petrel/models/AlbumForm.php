<?php

class AlbumForm extends CFormModel
{
    public $id;
    public $name;
    public $account_id;

    public function rules()
    {
        return array(
           array('name, account_id', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'), 
           'account_id' => Yii::t('system', 'Account'),   
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'account_id' => 'account_id',
       );
   }
}
