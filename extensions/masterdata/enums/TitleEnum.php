<?php

class TitleEnum {
    const MISS = "Ms.";
    const MRS = "Mrs.";
    const MR = "Mr.";

    static function getList()
    {
        return array(self::MISS => self::MISS,
                     self::MRS => self::MRS,
                     self::MR => self::MR
                    );
    }
}
