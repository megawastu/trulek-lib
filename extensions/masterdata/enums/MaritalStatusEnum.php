<?php

class MaritalStatusEnum {
    const SINGLE = "Single";
    const MARRIED = "Married";
    const SEPARATED = "Separated";
    const DIVORCED = "Divorced";

    static function getList()
    {
        return array(self::SINGLE => self::SINGLE,
                     self::MARRIED => self::MARRIED,
                     self::SEPARATED => self::SEPARATED,
                     self::DIVORCED => self::DIVORCED
                    );
    }
}
