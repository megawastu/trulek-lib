<?php

class GenderEnum {
    const FEMALE = "Female";
    const MALE = "Male";

    static function getList()
    {
        return array(self::FEMALE => self::FEMALE,
                     self::MALE => self::MALE
                    );
    }
}
