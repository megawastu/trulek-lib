<?php

class BloodTypeEnum {
    const A = "A";
    const B = "B";
    const AB = "AB";
    const O = "O";

    static function getList()
    {
        return array(self::A => self::A,
                     self::B => self::B,
                     self::AB => self::AB,
                     self::O => self::O
                    );
    }
}
