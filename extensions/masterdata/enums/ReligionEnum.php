<?php

class ReligionEnum {
    const MOSLEM = "Moslem";
    const CHRISTIAN = "Christian";
    const CATHOLIC = "Catholic";
    const HINDHU = "Hindhu";
    const BUDHISM = "Budhist";

    static function getList()
    {
        return array(self::MOSLEM => self::MOSLEM,
                     self::CHRISTIAN => self::CHRISTIAN,
                     self::CATHOLIC => self::CATHOLIC,
                     self::HINDHU => self::HINDHU,
                     self::BUDHISM => self::BUDHISM
                    );
    }
}
