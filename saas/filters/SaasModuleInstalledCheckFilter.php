<?php

Yii::import('application.modules.security.components.SecurityConfigTypeEnum');
Yii::import('application.modules.security.components.InterceptUrlMapTypeEnum');

Yii::import('trulek.saas.models.*');

class SaasModuleInstalledCheckFilter extends CFilter
{    
    public $unique_name;
    
    protected function preFilter($filterChain) // logic being applied before the action is executed
    {            
        $installed = Yii::app()->saasService->isModuleInstalled($this->unique_name);
        
        if (!$installed) 
        {
            throw new CHttpException("404", "Sorry, this module is not installed on your site.");
        }
        
        return true; // false if the action should not be executed
    }

    protected function postFilter($filterChain)
    {
        // logic being applied after the action is executed
    }
}
