<?php

Yii::import('application.modules.security.components.SecurityConfigTypeEnum');
Yii::import('application.modules.security.components.InterceptUrlMapTypeEnum');

Yii::import('trulek.saas.models.*');

class SaasChangeDatabaseFilter extends CFilter
{    
    protected function preFilter($filterChain) // logic being applied before the action is executed
    {            
        $host = Yii::app()->user->saasClient->database_host;
        $dbname = Yii::app()->user->saasClient->database_name;
        $user = Yii::app()->user->saasClient->database_username;
        $pass = Yii::app()->user->saasClient->database_password;
        Yii::app()->saasService->changeDatabase($host, $dbname, $user, $pass);
        
        return true; // false if the action should not be executed
    }

    protected function postFilter($filterChain)
    {
        // logic being applied after the action is executed
    }
}
