<?php

class SaasService extends CApplicationComponent {
    
    /**
     * Method to Change Database on the fly
     * @param String $host
     * @param String $dbname
     * @param String $username
     * @param String $password 
     */
    public function changeDatabase($host, $dbname, $username, $password)
    {        
        Yii::app()->db->setActive(false);
        Yii::app()->db->connectionString = 'mysql:host='.$host.';dbname='.$dbname;
        Yii::app()->db->username = $username;
        Yii::app()->db->password = $password;
		
        Yii::app()->db->setActive(true);
    }
    
    /**
     * Method to check if the module is installed
     * @param String $unique_name 
     */
    public function isModuleInstalled($unique_name)
    {
        $total = SaasCmsMenu::model()->countByAttributes(array("unique_name" => $unique_name));
        
        if ($total > 0) {
            return true;
        }
        
        return false;
    }
}

