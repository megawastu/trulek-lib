<?php

Yii::import('application.extensions.components.UserIdentity');

class SaasLoginForm extends CFormModel
{
	public $site_address;
	public $email;
	public $password;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
            return array(
                // site address, email, and password are required
                array('site_address, email, password', 'required'),
                array('email', 'email'),
                array('site_address', 'validateSiteAddress'),
                // password needs to be authenticated
                array('password', 'authenticate'),
            );
	}

	/**
	 * Declares the attribute labels.
	 * If an attribute is not delcared here, it will use the default label
	 * generation algorithm to get its label.
	 */
	public function attributeLabels()
	{
            return array(
                'site_address'=>Yii::t('system', 'Site Address'),
                'email' => Yii::t('system', 'Email'),
                'password' => Yii::t('system', 'Password'),
            );
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
		if(!$this->hasErrors())  // we only want to authenticate when no input errors
		{                    
			$saasClient = SaasClientCAR::model()->findByAttributes(array('site_address' => $this->site_address)); 
			
			if (Yii::app()->user->hasState('saasClient')) {
				$saasClient = Yii::app()->user->saasClient;
			}
			
			if ($saasClient != null) {
				Yii::app()->user->setState('saasClient', $saasClient);
				
				$host = $saasClient->database_host;
				$dbname = $saasClient->database_name;
				$username = $saasClient->database_username;
				$password = $saasClient->database_password;
				Yii::app()->saasService->changeDatabase($host, $dbname, $username, $password);
			}     
                        
			$identity=new UserIdentity($this->email,$this->password);
			$identity->authenticate();
			switch($identity->errorCode)
			{
                            case UserIdentity::ERROR_NONE:
                                //$duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
                                Yii::app()->user->login($identity);
                                break;
                            case UserIdentity::ERROR_USERNAME_INVALID:
                                $this->addError('email','Email is incorrect.');
                                break;
                            case UserIdentity::ERROR_ACCOUNT_DISABLED:
                                $this->addError('email','Your account is disabled, please contact Web Administrator.');
                                break;
                            case UserIdentity::ERROR_ACCOUNT_LOCKED:
                                $this->addError('email','Your account is locked, please contact Web Administrator.');
                                break;
                            case UserIdentity::ERROR_ACCOUNT_EXPIRED:
                                $this->addError('email','Your account has expired, please contact Web Administrator.');
                                break;
                            default: 
                                // UserIdentity::ERROR_PASSWORD_INVALID
                                $this->addError('password','Password is incorrect.');
                                break;
			}
		}
	}
        
        public function validateSiteAddress()
        {
            $siteAddress = SaasClientCAR::model()->findByAttributes(array('site_address' => $this->site_address));
            if ($siteAddress == null)
            {
               $this->addError('site_address', Yii::t('system', 'Sorry, we cannot find the {site_address}', array('{site_address}' => $this->site_address)));     
            }
        }
}
