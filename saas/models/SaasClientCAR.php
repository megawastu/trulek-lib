<?php

class SaasClientCAR extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'saas_client';
    }

    /**
     * Method to define the rules
     * @return Array
     */
    public function rules()
    {
        return array(
            array('name, site_address, database_host, database_name, database_username, database_password', 'required'),
            array('site_address', 'unique')
        );
    }

    public function relations()
    {
        return array(
           'keys'=>array(self::HAS_MANY, 'SaasKey', 'client_id'),
           'keyCount'=>array(self::STAT, 'SaasKey', 'client_id'),
           'menus'=>array(self::MANY_MANY, 'SaasCmsMenu', 'saas_client_menu(client_id, menu_id)'),
           'menuCount'=>array(self::STAT, 'SaasCmsMenu', 'saas_client_menu(client_id, menu_id)'),
        );
    }

    public function scopes()
    {
        return array(
            
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }

}