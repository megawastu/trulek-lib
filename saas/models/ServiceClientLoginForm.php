<?php

Yii::import('application.extensions.components.ServiceClientUserIdentity');

class ServiceClientLoginForm extends CFormModel
{
        public $id;
        public $site_address;
	public $email;
	public $password;
        public $rememberMe;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
            return array(
                // site address, email, and password are required
                array('email, site_address, password', 'required'),
                array('email', 'email'),
                // password needs to be authenticated
                array('password', 'authenticate'),
            );
	}

	/**
	 * Declares the attribute labels.
	 * If an attribute is not delcared here, it will use the default label
	 * generation algorithm to get its label.
	 */
	public function attributeLabels()
	{
            return array(
                'site_address' => Yii::t('system', 'Site Address'),
                'email' => Yii::t('system', 'Email'),
                'password' => Yii::t('system', 'Password'),
                'rememberMe'=>Yii::t('system', 'Remember me next time'),
            );
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute,$params)
	{
            if(!$this->hasErrors())  // we only want to authenticate when no input errors
            {
                $identity=new ServiceClientUserIdentity($this->email,$this->password);
                $identity->set_id($this->id);
                $identity->authenticate();
                switch($identity->errorCode)
                {
                    case ServiceClientUserIdentity::ERROR_NONE:
                    default:
                        $duration=$this->rememberMe ? 3600*24*30 : 0; // 30 days
                        
                        Yii::app()->user->login($identity, $duration);
                        break;
                }
            }
	}
}
