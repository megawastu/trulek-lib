<?php

class SaasCmsMenu extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'saas_cms_menu';
    }

    /**
     * Method to define the rules
     * @return Array
     */
    public function rules()
    {
        return array(
            array('title', 'required'),
            array('unique_name', 'unique')
        );
    }

    public function relations()
    {
        return array(
           'clients'=>array(self::MANY_MANY, 'SaasClient', 'saas_client_menu(client_id, menu_id)'),
           'clientCount'=>array(self::STAT, 'SaasClient', 'saas_client_menu(client_id, menu_id)'),
        );
    }

    public function scopes()
    {
        return array(
            
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }

}