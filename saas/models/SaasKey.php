<?php

class ArticleCategoryForm extends CFormModel
{
    public $id;
    public $name;
    public $urlkey;
    public $description;
    public $meta_desc;
    public $meta_keyword;
    public $ordering;
    public $published;    

    public function rules()
    {
        return array(
           array('name, urlkey, ordering, published', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
           'description' => Yii::t('system', 'Description'),
           'meta_desc' => Yii::t('system', 'Meta Description'),
           'meta_keyword' => Yii::t('system', 'Meta Keyword'),
           'ordering' => Yii::t('system', 'Sort Order'),
           'published' => Yii::t('system', 'Published'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'urlkey' => 'urlkey',
           'description' => 'description',
           'meta_desc' => 'meta_desc',
           'meta_keyword' => 'meta_keyword',
           'ordering' => 'ordering',
           'published' => 'published',
       );
   }
}
