<?php

class SaasClientInformationForm extends CFormModel
{
    public $id;
    public $address;
    public $city;
    public $postalcode;
    public $country;
    public $phone;
    public $newsletter;
    public $tos_read_agreed;
    public $promo_code;
    public $referer;

    public function rules()
    {
        return array(
           array('address, city, postalcode, country, phone, newsletter, tos_read_agreed', 'required'),
        );
    }
    
    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'address' => Yii::t('system', 'Address'),
           'city' => Yii::t('system', 'City'),
           'postalcode' => Yii::t('system', 'Postal Code'),
           'country' => Yii::t('system', 'Country'),
           'phone' => Yii::t('system', 'Phone'),
           'newsletter' => Yii::t('system', 'Newsletter'),
           'tos_read_agreed' => Yii::t('system', 'I have read and agreed to the Terms of Services'),
           'promo_code' => Yii::t('system', 'Promo Code'),
           'referer' => Yii::t('system', 'Referer'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'address' => 'address',
           'city' => 'city',
           'postalcode' => 'postalcode',
           'country' => 'country',
           'phone' => 'phone',
           'newsletter' => 'newsletter',
           'tos_read_agreed' => 'tos_read_agreed',
           'promo_code' => 'promo_code',
           'referer' => 'referer',
       );
   }
   
}
