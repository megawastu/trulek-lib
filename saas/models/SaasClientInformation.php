<?php

class SaasClientInformation extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'saas_client_information';
    }

    public function relations()
    {
        return array(
           'websites'=>array(self::MANY_MANY, 'SaasClient', 'saas_client_site_info_xref(saas_client_id, saas_client_info_id)'),
           'websiteCount'=>array(self::STAT, 'SaasClient', 'saas_client_site_info_xref(saas_client_id, saas_client_info_id)'),
        );
    }

    public function scopes()
    {
        return array(
            
        );
    }

    public function behaviors()
    {
        return array(
//            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }

}