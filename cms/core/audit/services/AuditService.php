<?php
/**
 * @deprecated Since Version 1.0 because CRUD already could be handled by CRUDService
 */
class AuditService extends CApplicationComponent {
    
    public function create($params=array())
    {
        $modelInstance = new Trail();
        $modelInstance->setAttributes($params);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
    
    public function update($params=array())
    {
        $modelInstance = Trail::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
    
    public function delete($id)
    {
        try {
            Trail::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
