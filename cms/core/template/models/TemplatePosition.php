<?php

class TemplatePosition extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'template_position';
    }

    /**
     * Method to define the rules
     * @return Array
     */
    public function rules()
    {
        return array(
            array('position', 'required'),
            array('position', 'unique')
        );
    }

    public function scopes()
    {
        return array(
            'dropdownList' => array('order' => 'position ASC'),
        );
    }

    public function relations()
    {
        return array(
           'widgets'=>array(self::HAS_MANY, 'MyWidget', 'position_id'),
           'widgetCount'=>array(self::STAT, 'MyWidget', 'position_id'),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }

}