<?php

class TemplatePositionForm extends CFormModel
{
    public $id;
    public $position;

    public function rules()
    {
        return array(
           array('position', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'position' => Yii::t('system', 'Position'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'position' => 'position',
       );
   }
}
