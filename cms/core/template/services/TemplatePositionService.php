<?php
/**
 * @deprecated Since Version 1.0 because CRUD already could be handled by CRUDService
 */
class TemplatePositionService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new TemplatePosition();
        $modelInstance->setAttributes($params, false);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = TemplatePosition::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
//            Widget::model()->deleteAllByAttributes(array('position_id' => $id));
            TemplatePosition::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
