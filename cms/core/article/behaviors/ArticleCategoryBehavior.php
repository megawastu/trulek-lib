<?php

class ArticleCategoryBehavior extends CActiveRecordBehavior
{
        public function onBeforeDelete($event)
        {
            ArticleCategoryXref::model()->deleteAllByAttributes(array('article_category_id' => $this->owner->id));
        }
}