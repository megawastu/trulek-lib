<?php

class ArticleCategoryXref extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'article_category_articles';
    }
    
    public function rules()
    {
        return array(
            array('article_id, article_category_id', 'required')
        );
    }
}

