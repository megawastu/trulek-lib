<?php

class ArticleCategory extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'article_category';
    }

    /**
     * Method to define the rules
     * @return Array
     */
    public function rules()
    {
        return array(
            array('name, urlkey, ordering, published', 'required'),
            array('urlkey', 'unique')
        );
    }

    public function relations()
    {
        return array(
           'articles'=>array(self::MANY_MANY, 'Article', 'article_category_articles(article_id, article_category_id)'),
           'articleCount'=>array(self::STAT, 'Article', 'article_category_articles(article_id, article_category_id)'),
        );
    }

    public function scopes()
    {
        return array(
            "published" => array("condition" => "`t`.`published`='1'"),
            "unpublished" => array("condition" => "`t`.`published`='0'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
            'DataDeleteBehavior'=> array(
                                    'class' => 'trulek.extensions.core.behaviors.DataDeleteBehavior',
                                    'mode' => 'cascade',
                                    'xrefModels' => array(
                                        'ArticleCategoryXref' => 'article_category_id',
                                    ),
                                    'relatedModel' => 'Article',
                                    'relatedModelForeignKey' => 'article_id',
                                 ),
        );
    }

}