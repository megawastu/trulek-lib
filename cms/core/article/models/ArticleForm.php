<?php

class ArticleForm extends CFormModel
{
    public $id;
    public $title;
    public $urlkey;
    public $intro;
    public $main;
    public $meta_desc;
    public $meta_keyword;
    public $ordering;
    public $published;
    public $hits;
    public $revision;
    public $revision_comment;
    public $author;
    public $categories;
    
    public $file;
    public $old_file;
    public $delete_file;
    
    public function rules()
    {
        return array(
           array('title, urlkey, intro, ordering, published, revision, categories', 'required'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'title' => Yii::t('system', 'Title'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
           'intro' => Yii::t('system', 'Intro'),
           'main' => Yii::t('system', 'Main'),
           'meta_desc' => Yii::t('system', 'Meta Description'),
           'meta_keyword' => Yii::t('system', 'Meta Keyword'),
           'ordering' => Yii::t('system', 'Sort Order'),
           'published' => Yii::t('system', 'Published'),
           'hits' => Yii::t('system', 'Hits'),
           'revision' => Yii::t('system', 'Revision'),
           'revision_comment' => Yii::t('system', 'Revision Comment'),
           'author' => Yii::t('system', 'Author'),
           'categories' => Yii::t('system', 'Categories'),
           'file' => Yii::t('system', 'File Name'),
           'old_file' => Yii::t('system', 'Old File Name'),
           'delete_file' => Yii::t('system', 'Delete File'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'title' => 'title',
           'urlkey' => 'urlkey',
           'intro' => 'intro',
           'main' => 'main',
           'meta_desc' => 'meta_desc',
           'meta_keyword' => 'meta_keyword',
           'ordering' => 'ordering',
           'published' => 'published',
           'hits' => 'hits',
           'revision' => 'revision',
           'revision_comment' => 'revision_comment',
           'author' => 'author',
           'categories' => 'categories',
           'file' => 'file',
           'old_file' => 'old_file',
           'delete_file' => 'delete_file',
       );
   }
   
   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
}
