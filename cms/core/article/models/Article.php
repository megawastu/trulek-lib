<?php

class Article extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'article';
    }

    /**
     * Method to defined rules for validation
     * @return Array
     */
    public function rules()
    {
        return array(
            array('title, urlkey, intro, published, ordering, revision', 'required'),
            array('urlkey', 'unique'),
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'categories' =>array(self::MANY_MANY, 'ArticleCategory', 'article_category_articles(article_id, article_category_id)'),
            'categoryCount' =>array(self::STAT, 'ArticleCategory', 'article_category_articles(article_id, article_category_id)'),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
            'SaveUploadedFileBehavior'=> 'trulek.extensions.fileuploader.behaviors.SaveUploadedFileBehavior',
        );
    }
}