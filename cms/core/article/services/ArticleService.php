<?php

class ArticleService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new Article();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();

            foreach ($params['categories'] as $category_id) {
                $xref = new ArticleCategoryXref();
                $xref->article_category_id = $category_id;
                $xref->article_id = $modelInstance->id;
                $xref->save();
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = Article::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            ArticleCategoryXref::model()->deleteAllByAttributes(array('article_id' => $modelInstance->id));

            $modelInstance->save();

            foreach ($params['categories'] as $category_id) {
                $xref = new ArticleCategoryXref();
                $xref->article_category_id = $category_id;
                $xref->article_id = $modelInstance->id;
                $xref->save();
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop all xref
            ArticleCategoryXref::model()->deleteAllByAttributes(array('article_id' => $id));
            $object = Article::model()->findByPk($id);
            $object->delete();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    /**
     * @deprecated since version 1.2: replaced by api.extensions.core.actions.PublishAction
     * @param <String> $id
     */
    public function publish($id) {
        $categories = array();
        $modelInstance = Article::model()->findByPk($id);
        $modelInstance->published = 0x01;

        $categories = $modelInstance->categories;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            ArticleCategoryXref::model()->deleteAllByAttributes(array('article_id' => $id));

            $modelInstance->save();
            
            foreach ($categories as $category) {
                $xref = new ArticleCategoryXref();
                $xref->article_category_id = $category->id;
                $xref->article_id = $modelInstance->id;
                $xref->save();
            }
            
            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    /**
     * @deprecated since version 1.2: replaced by api.extensions.core.actions.UnpublishAction
     * @param <String> $id
     */
    public function unpublish($id) {
        $categories = array();
        $modelInstance = Article::model()->findByPk($id);
        $modelInstance->published = 0x00;

        $categories = $modelInstance->categories;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            ArticleCategoryXref::model()->deleteAllByAttributes(array('article_id' => $id));

            //save
            $modelInstance->save();

            //add xref
            foreach ($categories as $category) {
                $xref = new ArticleCategoryXref();
                $xref->article_category_id = $category->id;
                $xref->article_id = $modelInstance->id;
                $xref->save();
            }

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function getNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `article`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
