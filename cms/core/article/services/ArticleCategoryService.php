<?php
/**
 * @deprecated Since Version 1.0 because CRUD already could be handled by CRUDService
 */
class ArticleCategoryService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new ArticleCategory();
        $modelInstance->setAttributes($params, false);
        $modelInstance->ordering = $this->getNextOrdering();
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = ArticleCategory::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            ArticleCategory::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    /**
     * @deprecated since version 1.2: replaced by api.extensions.core.actions.PublishAction
     * @param <String> $id
     */
    public function publish($id) {
        $articles = array();
        $modelInstance = ArticleCategory::model()->findByPk($id);
        $modelInstance->published = 0x01;

        $articles = $modelInstance->articles;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            ArticleCategoryXref::model()->deleteAllByAttributes(array('article_category_id' => $id));

            //save
            $modelInstance->save();

            //add xref
            foreach ($articles as $article) {
                $xref = new ArticleCategoryXref();
                $xref->article_id = $article->id;
                $xref->article_category_id = $modelInstance->id;
                $xref->save();
            }

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    /**
     * @deprecated since version 1.2: replaced by api.extensions.core.actions.UnpublishAction
     * @param <String> $id
     */
    public function unpublish($id) {
        $articles = array();
        $modelInstance = ArticleCategory::model()->findByPk($id);
        $modelInstance->published = 0x00;

        $articles = $modelInstance->articles;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            ArticleCategoryXref::model()->deleteAllByAttributes(array('article_category_id' => $id));

            //save
            $modelInstance->save();

            //add xref
            foreach ($articles as $article) {
                $xref = new ArticleCategoryXref();
                $xref->article_id = $article->id;
                $xref->article_category_id = $modelInstance->id;
                $xref->save();
            }

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function getNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `article_category`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
