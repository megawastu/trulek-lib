<?php

class SettingForm extends CFormModel
{
    public $id;
    public $address;
    public $city;
    public $country;
    public $description;
    public $email;
    public $fax;
    public $google_map;
    public $file;
    public $phone;
    public $postal_code;
    public $site_name;
    public $state;
    public $url;
    public $meta_desc;
    public $meta_keyword;

    public $delete_file;


    public function rules()
    {
        return array(
           array('site_name', 'required'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'address' => Yii::t('system', 'Address'),
           'city' => Yii::t('system', 'City'),
           'country' => Yii::t('system', 'Country'),
           'description' => Yii::t('system', 'Description'),
           'email' => Yii::t('system', 'Email'),
           'fax' => Yii::t('system', 'Fax'),
           'google_map' => Yii::t('system', 'Google Map'),
           'file' => Yii::t('system', 'Logo'),
           'phone' => Yii::t('system', 'Phone'),
           'postal_code' => Yii::t('system', 'Postal Code'),
           'site_name' => Yii::t('system', 'Site Name'),
           'state' => Yii::t('system', 'State'),
           'url' => Yii::t('system', 'Url'),
           'meta_desc' => Yii::t('system', 'Default Meta Description'),
           'meta_keyword' => Yii::t('system', 'Default Meta Keyword'),
           'delete_file' => Yii::t('system', 'Delete Logo'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'address' => 'address',
           'city' => 'city',
           'country' => 'country',
           'description' => 'description',
           'email' => 'email',
           'fax' => 'fax',
           'google_map' => 'google_map',
           'file' => 'file',
           'phone' => 'phone',
           'postal_code' => 'postal_code',
           'site_name' => 'site_name',
           'state' => 'state',
           'url' => 'url',
           'meta_desc' => 'meta_desc',
           'meta_keyword' => 'meta_keyword',
           'delete_file' => 'delete_file',
       );
   }
   
   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
}
