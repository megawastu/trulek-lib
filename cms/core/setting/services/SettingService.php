<?php

class SettingService extends CApplicationComponent {

    public function save($params=array())
    {
        $modelInstance = null;
        $modelInstance = Setting::model()->last()->find();
        if ($modelInstance == null)
        {
            $modelInstance = new Setting();
        }
        
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }
}
