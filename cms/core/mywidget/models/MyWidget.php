<?php

class MyWidget extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'widget';
    }

    /**
     * Method to define the rules
     * @return Array
     */
    public function rules()
    {
        return array(
            array('title, unique_name, published, ordering, position_id', 'required'),
            array('unique_name', 'unique')
        );
    }

    public function scopes()
    {
        return array(
            'dropdownList' => array('order' => 'title ASC'),
        );
    }

    public function relations()
    {
        return array(
           'position'=>array(self::BELONGS_TO, 'TemplatePosition', 'position_id'),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }

}