<?php

class MyWidgetForm extends CFormModel
{
    public $id;
    public $title;
    public $unique_name;
    public $position_id;
    public $published;
    public $ordering;
    public $description;

    public function rules()
    {
        return array(
           array('title, unique_name, position_id, published, ordering', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'title' => Yii::t('system', 'Title'),
           'unique_name' => Yii::t('system', 'Unique Name'),
           'position_id' => Yii::t('system', 'Position'),
           'published' => Yii::t('system', 'Published'),
           'ordering' => Yii::t('system', 'Sort Order'),
           'description' => Yii::t('system', 'Unique Name'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'title' => 'title',
           'unique_name' => 'unique_name',
           'position_id' => 'position_id',
           'published' => 'published',
           'ordering' => 'ordering',
           'description' => 'description',
       );
   }
}
