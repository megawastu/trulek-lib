<?php

class RoleService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new Role();
        $modelInstance->setAttributes($params, false);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = Role::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            UserRole::model()->deleteAllByAttributes(array('role_id' => $id));
            
            Role::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
