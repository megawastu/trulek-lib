<?php

/**
 * @deprecated Since Version 1.0 because CRUD already could be handled by CRUDService
 */

class UserGroupService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new UserGroup();
        $modelInstance->setAttributes($params);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = UserGroup::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //remove users
            UserGroupXref::model()->deleteAllByAttributes(array('user_group_id' => $id));

            UserGroup::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
