<?php

class UserService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new User();
        $modelInstance->setAttributes($params, false);

        $modelInstance->password = md5($modelInstance->password);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();
            
            $xref = new UserGroupXref();
            $xref->user_group_id = $params['user_group_id'];
            $xref->user_id = $modelInstance->id;
            $xref->save();

            $xref = new UserRole();
            $xref->role_id = $params['role_id'];
            $xref->user_id = $modelInstance->id;
            $xref->save();
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = User::model()->findByPk($params['id']);
        $oldPassword = $modelInstance->password;
        $modelInstance->setAttributes($params, false);
        $modelInstance->password = $oldPassword;
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            UserGroupXref::model()->deleteAllByAttributes(array('user_id' => $modelInstance->id));
            UserRole::model()->deleteAllByAttributes(array('user_id' => $modelInstance->id));

            $modelInstance->save();
            
            $xref = new UserGroupXref();
            $xref->user_group_id = $params['user_group_id'];
            $xref->user_id = $modelInstance->id;
            $xref->save();
            
            $xref = new UserRole();
            $xref->role_id = $params['role_id'];
            $xref->user_id = $modelInstance->id;
            $xref->save();           
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop all xref
            UserGroupXref::model()->deleteAllByAttributes(array('user_id' => $id));
            UserRole::model()->deleteAllByAttributes(array('user_id' => $id));

            User::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
