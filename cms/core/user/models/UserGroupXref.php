<?php

class UserGroupXref extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'user_group_users';
    }
    
    public function rules()
    {
        return array(
            array('user_id, user_group_id', 'required')
        );
    }
}

