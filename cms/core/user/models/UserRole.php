<?php

class UserRole extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'user_role';
    }

    /**
     * Method to defined rules for validation
     * @return Array
     */
    public function rules()
    {
        return array(
            
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'users' => array(self::MANY_MANY, 'User', 'user_role(role_id, user_id)'),
            'roles' => array(self::MANY_MANY, 'Role', 'user_role(role_id, user_id)'),
        );
    }
}