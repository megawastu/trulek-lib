<?php

class UserGroup extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'user_group';
    }

    /**
     * Method to define the rules
     * @return Array
     */
    public function rules()
    {
        return array(
            array('name', 'required'),
            array('name', 'api.extensions.core.validator.UniqueAttributeValueValidator')
        );
    }

    public function relations()
    {
        return array(
           'users'=>array(self::MANY_MANY, 'User', 'user_group_users(user_group_id, user_id)'),
           'userCount'=>array(self::STAT, 'User', 'user_group_users(user_group_id, user_id)'),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }

}