<?php

class RoleForm extends CFormModel
{
    public $id;
    public $authority;

    public function rules()
    {
        return array(
           array('authority', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'authority' => Yii::t('system', 'Authority'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'authority' => 'authority',
       );
   }
}
