<?php

class UserForm extends CFormModel
{
    public $id;
    public $first_name;
    public $last_name;
    public $email;
    public $password;
    public $repeat_password;
    public $password_expired;
    public $enabled;
    public $account_locked;
    public $account_expired;
    public $user_group_id;
    public $role_id;
    
    public function rules()
    {
        return array(
           array('first_name, last_name, email, enabled, account_locked, account_expired, password_expired', 'required'),
           array('password', 'required', 'on' => 'new'),
           array('password', 'compare', 'compareAttribute'=>'repeat_password', 'on' => 'new'),
           array('email', 'email'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'first_name' => Yii::t('system', 'First Name'),
           'last_name' => Yii::t('system', 'Last Name'),
           'email' => Yii::t('system', 'Email'),
           'password' => Yii::t('system', 'Password'),
           'repeat_password' => Yii::t('system', 'Repeat Password'),
           'password_expired' => Yii::t('system', 'Password Expired'),
           'enabled' => Yii::t('system', 'Enabled'),
           'account_locked' => Yii::t('system', 'Account Locked'),
           'account_expired' => Yii::t('system', 'Account Expired'),
           'user_group_id' => Yii::t('system', 'User Group'),
           'role_id' => Yii::t('system', 'Role'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'first_name' => 'first_name',
           'last_name' => 'last_name',
           'email' => 'email',
           'password' => 'password',
           'repeat_password' => 'repeat_password',
           'password_expired' => 'password_expired',
           'enabled' => 'enabled',
           'account_locked' => 'account_locked',
           'account_expired' => 'account_expired',
           'user_group_id' => 'user_group_id',
           'role_id' => 'role_id',
       );
   }
}
