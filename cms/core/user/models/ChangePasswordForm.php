<?php

class ChangePasswordForm extends CFormModel
{
    public $current_password;
    public $new_password;
    public $repeat_password;
    
    public function rules()
    {
        return array(
           array('current_password, new_password, repeat_password', 'required'),
           array('current_password', 'validateCurrentPassword'),
           array('new_password', 'compare', 'compareAttribute'=>'repeat_password'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'current_password' => Yii::t('system', 'Current Password'),
           'new_password' => Yii::t('system', 'New Password'),
           'repeat_password' => Yii::t('system', 'Repeat Password'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'current_password' => 'current_password',
           'new_password' => 'new_password',
           'repeat_password' => 'repeat_password',
       );
   }

   /**
    * @return void
    */
   public function validateCurrentPassword()
    {
       $id = Yii::app()->user->id;
       $current_password = md5($this->current_password);
       
       $criteria = new CDbCriteria();
       $criteria->condition = "`password` = '".$current_password."' AND `id` = '".$id."'";
       
       $total = User::model()->count($criteria);
       if ($total <= 0)
       {
         $this->addError('current_password', Yii::t('system', 'Wrong {fieldName}, please check again.', array('{fieldName}' => 'Current Password')));
       }
   }
}
