<?php

class User extends CActiveRecord
{
    public $password_repeat;
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'user';
    }

    /**
     * Method to defined rules for validation
     * @return Array
     */
    public function rules()
    {
        return array(
            array('email, password, password_repeat', 'required', 'on' => 'registration, update'),
            array('email', 'email', 'on' => 'registration, update'),
            array('email', 'unique', 'on' => 'registration, update'),
            array('password', 'compare', 'on' => 'registration, update')
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'groups' =>array(self::MANY_MANY, 'UserGroup', 'user_group_users(user_group_id, user_id)'),
            'roles'     =>array(self::MANY_MANY, 'Role', 'user_role(role_id, user_id)'),
            'rolesCount' => array(self::STAT, 'Role', 'user_role(role_id, user_id)'),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}