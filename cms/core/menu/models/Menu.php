<?php

class Menu extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'menu';
    }

    /**
     * Method to define the rules
     * @return Array
     */
    public function rules()
    {
        return array(
            array('title, published, unique_name', 'required'),
            array('unique_name', 'unique')
        );
    }

    public function relations()
    {
        return array(
           'items'=>array(self::HAS_MANY, 'MenuItem', 'menu_id'),
           'itemCount'=>array(self::STAT, 'MenuItem', 'menu_id'),
        );
    }
    
    public function scopes()
    {
        return array(
            "published" => array("condition" => "`t`.`published`='1'"),
            "unpublished" => array("condition" => "`t`.`published`='0'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }

}