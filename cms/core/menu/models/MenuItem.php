<?php

class MenuItem extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'menu_item';
    }

    /**
     * Method to defined rules for validation
     * @return Array
     */
    public function rules()
    {
        return array(
            array('title, menu_id, menu_link, browser_nav, published', 'required'),
//            array('urlkey', 'unique'),
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'menu' =>array(self::BELONGS_TO, 'Menu', 'menu_id'),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}