<?php

class MenuForm extends CFormModel
{
    public $id;
    public $title;
    public $published;
    public $unique_name;  

    public function rules()
    {
        return array(
           array('title, published, unique_name', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'title' => Yii::t('system', 'Name'),
           'published' => Yii::t('system', 'Published'),
           'unique_name' => Yii::t('system', 'Unique Name'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'title' => 'title',
           'published' => 'published',
           'unique_name' => 'unique_name',
       );
   }
}
