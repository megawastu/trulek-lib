<?php

class MenuItemForm extends CFormModel
{
    public $id;
    public $parent_id;
    public $title;
    public $urlkey;
    public $published;
    public $ordering;
    public $menu_id;
    public $menu_link;
    public $browser_nav;
    public $homepage;

    public $meta_desc;
    public $meta_keyword;
    
    public $widgets = array();

    public function rules()
    {
        return array(
           array('title, menu_id, menu_link, browser_nav, published, homepage', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'parent_id' => Yii::t('system', 'Parent'),
           'title' => Yii::t('system', 'Title'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
           'published' => Yii::t('system', 'Published'),
           'ordering' => Yii::t('system', 'Sort Order'),           
           'menu_id' => Yii::t('system', 'Menu Type'),
           'menu_link' => Yii::t('system', 'Link'),
           'browser_nav' => Yii::t('system', 'Browser Nav'),
           'homepage' => Yii::t('system', 'Home Page'),
           'widgets' => Yii::t('system', 'Widgets'),
           'meta_desc' => Yii::t('system', 'Meta Description'),
           'meta_keyword' => Yii::t('system', 'Meta Keywords'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'parent_id' => 'parent_id',
           'title' => 'title',
           'urlkey' => 'urlkey',
           'published' => 'published',
           'ordering' => 'ordering',
           'menu_id' => 'menu_id',
           'menu_link' => 'menu_link',
           'browser_nav' => 'browser_nav',
           'homepage' => 'homepage',
           'widgets' => 'widgets',
           'meta_desc' => 'meta_desc',
           'meta_keyword' => 'meta_keyword',
       );
   }
}
