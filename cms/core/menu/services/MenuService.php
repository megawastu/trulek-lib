<?php
/**
 * @deprecated Since Version 1.0 because CRUD already could be handled by CRUDService
 */
class MenuService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new Menu();
        $modelInstance->setAttributes($params, false);
        
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = Menu::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            Menu::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $modelInstance = Menu::model()->findByPk($id);
        $modelInstance->published = 0x01;
        try {            
            //save
            $modelInstance->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function unpublish($id) {
        $modelInstance = Menu::model()->findByPk($id);
        $modelInstance->published = 0x00;
        try {            
            //save
            $modelInstance->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
