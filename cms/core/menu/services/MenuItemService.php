<?php

class MenuItemService extends CApplicationComponent {
    
    public function create($params=array())
    {
        $modelInstance = new MenuItem();
        $modelInstance->setAttributes($params, false);
        if ($modelInstance->parent_id == "") {
            $modelInstance->parent_id = constant("NULL");
        }

        if ($modelInstance->urlkey == "") {
            $modelInstance->urlkey = constant("NULL");
        }
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {

            //if set to homepage
            if ($modelInstance->homepage == 1) {
                //de-homepage other menu
                $items = MenuItem::model()->findAllByAttributes(array('homepage' => 1));
                if ($items != null) {
                    foreach ($items as $item) {
                        $item->homepage = 0x00;
                        $item->update();
                    }
                }
                
                $modelInstance->homepage = 0x01;
            }
            
            //if widgets are selected
            if (count($params['widgets']) > 0) {
                foreach ($params['widgets'] as $widget_id) {
                    $xref = new MenuItemWidget();
                    $xref->menu_item_id = $modelInstance->id;
                    $xref->widget_id = $widget_id;
                    $xref->save();
                }
            }

            $modelInstance->save();
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = MenuItem::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        if ($modelInstance->parent_id == "") {
            $modelInstance->parent_id = constant("NULL");
        }
        
        if ($modelInstance->urlkey == "") {
            $modelInstance->urlkey = constant("NULL");
        }
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //if set to homepage
            if ($modelInstance->homepage == chr(0x01)) {
                //de-homepage other menu
                $items = MenuItem::model()->findAllByAttributes(array('homepage' => 1));
                if ($items != null) {
                    foreach ($items as $item) {
                        $item->homepage = 0x00;
                        $item->update();
                    }
                }
                $modelInstance->homepage = 0x01;
            }

            //drop all xref
            MenuItemWidget::model()->deleteAllByAttributes(array('menu_item_id' => $modelInstance->id));
            //if widgets are selected
            if (count($params['widgets']) > 0) {
                foreach ($params['widgets'] as $widget_id) {
                    $xref = new MenuItemWidget();
                    $xref->menu_item_id = $modelInstance->id;
                    $xref->widget_id = $widget_id;
                    $xref->save();
                }
            }
            
            $modelInstance->save();
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop all xref
            MenuItemWidget::model()->deleteAllByAttributes(array('menu_item_id' => $id));
            MenuItem::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $modelInstance = MenuItem::model()->findByPk($id);
        $modelInstance->published = 0x01;
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            $modelInstance->save();
            
            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $modelInstance = MenuItem::model()->findByPk($id);
        $modelInstance->published = 0x00;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            $modelInstance->save();

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function getNextOrdering($menu_id)
    {
        $sql = "SELECT MAX(`ordering`) FROM `menu_item` WHERE `menu_id` = '".$menu_id."'";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }

    public function setMenuType(Menu $menuType) {
        Yii::app()->user->setState('menuType', $menuType);
    }

    public function getMenuType()
    {
        return Yii::app()->user->menuType;
    }
}
