<?php

class AddressTypeEnum {
    
    const BILLING = '1';
    const SHIPPING = '2';
    
    public static function getList()
    {
        return array(
            self::BILLING => Yii::t('system', 'Billing'),
            self::SHIPPING => Yii::t('system', 'Shipping'),
        );
    }
}