<?php

class AccountService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new Account();
        $modelInstance->setAttributes($params, false);
        $modelInstance->account_type_id = $params['type'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            $encrypted_password = md5($modelInstance->password);
            $modelInstance->password = $encrypted_password;
            
            $modelInstance->save();
            
            if (isset($params['groups']) && is_array($params['groups']) && count($params['groups']) > 0) {
                foreach ($params['groups'] as $group_id) {
                    $xref = new AccountGroupXref();
                    $xref->account_group_id = $group_id;
                    $xref->account_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = Account::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        $modelInstance->account_type_id = $params['type'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            AccountGroupXref::model()->deleteAllByAttributes(array('account_id' => $modelInstance->id));
            
            $encrypted_password = md5($modelInstance->password);
            $modelInstance->password = $encrypted_password;
            $modelInstance->save();

            if (isset($params['groups']) && is_array($params['groups']) && count($params['groups']) > 0) {
                foreach ($params['groups'] as $id) {
                    $xref = new AccountGroupXref();
                    $xref->account_group_id = $id;
                    $xref->account_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop all xref
            AccountGroupXref::model()->deleteAllByAttributes(array('account_id' => $id));
            $object = Account::model()->findByPk($id);
            $object->delete();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function activate($id) {
        $categories = array();
        $modelInstance = Account::model()->findByPk($id);
        $modelInstance->published = 1;

        $groups = $modelInstance->groups;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            AccountGroupXref::model()->deleteAllByAttributes(array('account_id' => $id));

            $modelInstance->save();
            
            foreach ($groups as $group) {
                $xref = new ArticleCategoryXref();
                $xref->account_group_id = $group->id;
                $xref->account_id = $modelInstance->id;
                $xref->save();
            }
            
            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function deactivate($id) {
        $categories = array();
        $modelInstance = Account::model()->findByPk($id);
        $modelInstance->published = 0;

        $groups = $modelInstance->groups;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            AccountGroupXref::model()->deleteAllByAttributes(array('account_id' => $id));

            //save
            $modelInstance->save();

            //add xref
            foreach ($groups as $group) {
                $xref = new AccountGroupXref();
                $xref->account_group_id = $group->id;
                $xref->account_id = $modelInstance->id;
                $xref->save();
            }

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }
}
