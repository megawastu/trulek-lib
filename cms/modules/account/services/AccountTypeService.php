<?php
/**
 * @deprecated Since Version 1.0 because CRUD already could be handled by CRUDService
 */
class AccountTypeService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new AccountType();
        $modelInstance->setAttributes($params, false);
        
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = AccountType::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            AccountType::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
