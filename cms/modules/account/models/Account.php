<?php

Yii::import('trulek.extensions.petrel.models.Album');
Yii::import('trulek.extensions.petrel.models.Photo');

class Account extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'account';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
           'type'=>array(self::BELONGS_TO, 'AccountType', 'account_type_id'),
           'groups'=>array(self::MANY_MANY, 'AccountGroup', 'account_group_accounts(account_id, account_group_id)'),
           'addresses'=>array(self::HAS_MANY, 'AccountAddressBook', 'account_id'),
           'addressCount'=>array(self::STAT, 'AccountAddressBook', 'account_id'),
           'albums'=>array(self::HAS_MANY, 'Album', 'account_id'),
           'albumCount'=>array(self::STAT, 'Album', 'account_id'),
        );
    }
    
    public function scopes()
    {
        return array(
            
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}