<?php

class AccountGroupXref extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'account_group_accounts';
    }
    
    public function rules()
    {
        return array(
            array('account_id, account_group_id', 'required')
        );
    }
}

