<?php

class AccountGroupForm extends CFormModel
{
    public $id;
    public $name;

    public function rules()
    {
        return array(
           array('name', 'required'),
           array('name', 'validateUniqueName'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueName()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`name` = '".$this->name."' AND `id` <> '".$this->id."'";

       $total = AccountGroup::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('name', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName' => 'Account Type Name')));
       }
   }
   
}
