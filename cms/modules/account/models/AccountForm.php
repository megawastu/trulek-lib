<?php

class AccountForm extends CFormModel
{
    public $id;
    public $type;
    public $groups=array();
    public $email;
    public $password;
    public $repeat_password;
    public $first_name;
    public $last_name;
    public $company;
    public $phone;
    public $fax;
    public $newsletter;
    public $inactive;

    public function rules()
    {
        return array(
           array('type, email, password, first_name, last_name, newsletter, inactive', 'required'),
           array('email', 'validateUniqueEmail'),
           array('password', 'compare', 'compareAttribute'=>'repeat_password'),
        );
    }
    
    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'type' => Yii::t('system', 'Account Type'),
           'groups' => Yii::t('system', 'Account Group'),
           'email' => Yii::t('system', 'Email'),
           'password' => Yii::t('system', 'Password'),
           'repeat_password' => Yii::t('system', 'Repeat Password'),
           'first_name' => Yii::t('system', 'First Name'),
           'last_name' => Yii::t('system', 'Last Name'),
           'company' => Yii::t('system', 'Company'),
           'phone' => Yii::t('system', 'Phone'),
           'fax' => Yii::t('system', 'Fax'),
           'newsletter' => Yii::t('system', 'Newsletter'),
           'inactive' => Yii::t('system', 'Inactive'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'type' => 'type',
           'groups' => 'groups',
           'email' => 'email',
           'password' => 'password',
           'repeat_password' => 'repeat_password',
           'first_name' => 'first_name',
           'last_name' => 'last_name',
           'company' => 'company',
           'phone' => 'phone',
           'fax' => 'fax',
           'newsletter' => 'newsletter',
           'inactive' => 'inactive'
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueEmail()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`email` = '".$this->email."' AND `id` <> '".$this->id."'";

       $total = Account::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('email', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName' => 'Email')));
       }
   }
   
}
