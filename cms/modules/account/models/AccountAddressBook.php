<?php

class AccountAddressBook extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'account_address_book';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
           'account'=>array(self::BELONGS_TO, 'Account', 'account_id'),
        );
    }
    
    public function scopes()
    {
        return array(
            'billing' => array("condition" => "`t`.`address_type` = '1'"),
            'shipping' => array("condition" => "`t`.`address_type` = '2'"),
            'default' => array("condition" => "`t`.`main` = '1'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}