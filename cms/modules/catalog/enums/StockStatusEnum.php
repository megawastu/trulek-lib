<?php

class StockStatusEnum {
    const TWO_THREE_DAYS = "1";
    const IN_STOCK = "2";
    const SOLD_OUT = "3";
    const PREORDER = "4";

    static function getList()
    {
        return array(self::TWO_THREE_DAYS => Yii::t('system', '2-3 Days'),
                     self::IN_STOCK => Yii::t('system', 'In Stock'),
                     self::SOLD_OUT => Yii::t('system', 'Sold Out'),
                     self::PREORDER => Yii::t('system', 'Preorder'));
    }
    
    static function getStatus($status)
    {
        switch ($status)
        {
            case "1":
                return Yii::t('system', '2-3 Days');
                break;
            case "2":
                return Yii::t('system', 'In Stock');
                break;            
            case "3":
                return Yii::t('system', 'Sold Out');
                break;
            case "4":
                return Yii::t('system', 'Pre Order');
                break;
            default:
                break;
        }
    }
}