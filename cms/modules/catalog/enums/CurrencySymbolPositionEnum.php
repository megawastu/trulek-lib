<?php

class CurrencySymbolPositionEnum 
{
    const LEFT = "1";
    const RIGHT = "2";

    static function getList()
    {
        return array(self::LEFT => Yii::t('system', 'Left'),
                     self::RIGHT => Yii::t('system', 'Right'),
                    );
    }
}