<?php

class DayEnum 
{
    const MONDAY = "1";
    const TUESDAY = "2";
    const WEDNESDAY = "3";
    const THURSDAY = "4";
    const FRIDAY = "5";
    const SATURDAY = "6";
    const SUNDAY = "7";
    const HOLIDAY = "8";

    static function getList()
    {
        return array(self::MONDAY => Yii::t('system', 'Monday'),
                     self::TUESDAY => Yii::t('system', 'Tuesday'),
                     self::WEDNESDAY => Yii::t('system', 'Wednesday'),
                     self::THURSDAY => Yii::t('system', 'Thursday'),
                     self::FRIDAY => Yii::t('system', 'Friday'),
                     self::SATURDAY => Yii::t('system', 'Saturday'),
                     self::SUNDAY => Yii::t('system', 'Sunday'),
                     self::HOLIDAY => Yii::t('system', 'Holiday'),
                    );
    }
    
    static function generateTime($mode=24)
    {
        $hourStart = 0;        
        $hourEnd = $mode;
        $minuteStart = 0;
        $minuteEnd = 60;
        
        $generatedTimes = array();
        
        $hours = array();
        for ($i = 0; $i < $hourEnd; $i++)
        {
            $hour = strval($i);
            $hours[$hour] = $hour;
        }
        
        $minutes = array();
        for ($i = 0; $i < $minuteEnd; $i++)
        {
            $minute = strval($i);
            $minutes[$minute] = $minute;
        }
        
        foreach ($hours as $hour)
        {
            $h = (strlen($hour) == 1) ? strval("0" . $hour) : strval($hour);
            foreach ($minutes as $minute)
            {
                $m = (strlen($minute) == 1) ? strval("0" . $minute) : strval($minute);
                $strTime = $h.":".$m;
                $generatedTimes[$strTime] = $strTime;
            }
        }
        
        return $generatedTimes;
    }
}