<?php

class PeriodUnitEnum 
{
    const HOURLY = "1";
    const DAILY = "2";
    const WEEKLY = "3";
    const MONTHLY = "4";
    const YEARLY = "5";

    static function getList()
    {
        return array(self::HOURLY => Yii::t('system', 'Hour(s)'),
                     self::DAILY => Yii::t('system', 'Day(s)'),
                     self::WEEKLY => Yii::t('system', 'Week(s)'),
                     self::MONTHLY => Yii::t('system', 'Month(s)'),
                     self::YEARLY => Yii::t('system', 'Year(s)'),
                    );
    }
    
    static function getPeriodUnit($key)
    {
        $period_unit = '';
        
        switch ($key)
        {
            case 1:
                $period_unit = Yii::t('system', 'Hour');
                break;
            case 2:
                $period_unit = Yii::t('system', 'Day');
                break;
            case 3:
                $period_unit = Yii::t('system', 'Week');
                break;
            case 4:
                $period_unit = Yii::t('system', 'Month');
                break;
            case 5:
                $period_unit = Yii::t('system', 'Year');
                break;
        }
        
        return $period_unit;
    }
}