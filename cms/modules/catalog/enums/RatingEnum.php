<?php

class RatingEnum 
{
    const VERY_BAD = "1";
    const BAD = "2";
    const GOOD = "3";
    const VERY_GOOD = "4";
    const EXCELLENT = "5";

    static function getList()
    {
        return array(self::VERY_BAD => Yii::t('system', 'Very Bad'),
                     self::BAD => Yii::t('system', 'Bad'),
                     self::GOOD => Yii::t('system', 'Good'),
                     self::VERY_GOOD => Yii::t('system', 'Very Good'),
                     self::EXCELLENT => Yii::t('system', 'Excellent'),
                    );
    }
}