<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogPaymentMethodService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new CatalogPaymentMethod();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();       
            
            if (isset($params['stores']) && count($params['stores']) > 0) {
                foreach ($params['stores'] as $store_id) {
                $xref = new CatalogStorePaymentMethod();
                    $xref->store_id = $store_id;
                    $xref->payment_method_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = CatalogPaymentMethod::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {            
            //drop all xref
            CatalogStorePaymentMethod::model()->deleteAllByAttributes(array('payment_method_id' => $modelInstance->id));
            
            $modelInstance->save();        
            
            if (isset($params['stores']) && count($params['stores']) > 0) {
                foreach ($params['stores'] as $store_id) {
                $xref = new CatalogStorePaymentMethod();
                    $xref->store_id = $store_id;
                    $xref->payment_method_id = $modelInstance->id;
                    $xref->save();
                }
            }
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop all xref
            CatalogStorePaymentMethod::model()->deleteAllByAttributes(array('payment_method_id' => $id));
            
            $object = CatalogPaymentMethod::model()->findByPk($id);
            $old_file = $object->file;
            $object->delete();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $objects = array();
        $modelInstance = CatalogPaymentMethod::model()->findByPk($id);
        $modelInstance->published = 1;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $objects = array();
        $modelInstance = CatalogPaymentMethod::model()->findByPk($id);
        $modelInstance->published = 0;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();
//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
}
