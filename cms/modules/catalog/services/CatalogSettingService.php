<?php

class CatalogSettingService extends CApplicationComponent {

    public function save($params=array())
    {
        $modelInstance = null;
        $modelInstance = CatalogSetting::model()->last()->find();
        if ($modelInstance == null)
        {
            $modelInstance = new CatalogSetting();
        }
        
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }
}
