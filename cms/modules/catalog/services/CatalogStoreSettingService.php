<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogStoreSettingService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new CatalogStoreSetting();
        $modelInstance->setAttributes($params, false);
        
        $modelInstance->country_id = $params['country'];
        $modelInstance->currency_id = $params['currency'];
        $modelInstance->weight_class_id = $params['weightClass'];
        $modelInstance->length_class_id = $params['lengthClass'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = CatalogStoreSetting::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $modelInstance->country_id = $params['country'];
        $modelInstance->currency_id = $params['currency'];
        $modelInstance->weight_class_id = $params['weightClass'];
        $modelInstance->length_class_id = $params['lengthClass'];

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {            
            $modelInstance->save();            
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            CatalogStoreSetting::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
