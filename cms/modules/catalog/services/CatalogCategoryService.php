<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogCategoryService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new CatalogCategory();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();       
            
            if (isset($params['stores']) && count($params['stores']) > 0) {
                foreach ($params['stores'] as $store_id) {
                $xref = new CatalogStoreCategory();
                    $xref->store_id = $store_id;
                    $xref->category_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = CatalogCategory::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {            
            //drop all xref
            CatalogStoreCategory::model()->deleteAllByAttributes(array('category_id' => $modelInstance->id));
            
            $modelInstance->save();        
            
            if (isset($params['stores']) && count($params['stores']) > 0) {
                foreach ($params['stores'] as $store_id) {
                $xref = new CatalogStoreCategory();
                    $xref->store_id = $store_id;
                    $xref->category_id = $modelInstance->id;
                    $xref->save();
                }
            }
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop all xref
            CatalogStoreCategory::model()->deleteAllByAttributes(array('category_id' => $id));
            
            $object = CatalogCategory::model()->findByPk($id);
            $old_file = $object->file;
            $object->delete();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $objects = array();
        $modelInstance = CatalogCategory::model()->findByPk($id);
        $modelInstance->published = 1;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $objects = array();
        $modelInstance = CatalogCategory::model()->findByPk($id);
        $modelInstance->published = 0;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();
//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
    
    public function getNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `catalog_category`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
