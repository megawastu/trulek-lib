<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogFileService extends CApplicationComponent {
    
    public function create($params=array())
    {
        $modelInstance = new CatalogProductFile();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = CatalogProductFile::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }
    
    public function delete($id) {
        try {
            $object = CatalogProductFile::model()->findByPk($id);
            $old_file = $object->file_name;
            $object->delete();

            if ($old_file != null) {
               $uploadPath = $_SERVER['DOCUMENT_ROOT'].Yii::app()->assetManager->baseUrl.'/'.Yii::app()->params['baseMediaFolder'].Yii::app()->controller->module->name.'/file';
               Yii::app()->fileUploadService->deleteFile($old_file, $uploadPath);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $objects = array();
        $modelInstance = CatalogProductFile::model()->findByPk($id);
        $modelInstance->published = 0x01;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $objects = array();
        $modelInstance = CatalogProductFile::model()->findByPk($id);
        $modelInstance->published = 0x00;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();

//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
    
    public function getNextOrdering(CatalogProduct $product)
    {
        $sql = "SELECT MAX(`ordering`) FROM `catalog_product_file` WHERE `catalog_product_file`.`product_id` = '".$product->id."'";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
