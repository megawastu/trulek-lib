<?php
/**
 * @deprecated Since Version 1.0 because CRUD already could be handled by CRUDService
 */
Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogWeightClassService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new CatalogWeightClass();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save(); 
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = CatalogWeightClass::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {    
            $modelInstance->save();
            
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            
            //delete object
            CatalogWeightClass::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
