<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogOptionService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new CatalogOption();
        $modelInstance->setAttributes($params, false);
        $modelInstance->option_type_id = $params['type'];

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save(); 
            
            //save option values
            if (isset($params['option_value_names']) && count($params['option_value_names']) > 0)
            {
                foreach ($params['option_value_names'] as $i => $option_value_name)
                {
                    if ($option_value_name != '') {
                        $obj = new CatalogOptionValue();
                        $obj->option_id = $modelInstance->id;
                        $obj->name = $option_value_name;
                        $obj->ordering = $params['option_value_orderings'][$i];
                        $obj->save();
                    }
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = CatalogOption::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        $modelInstance->option_type_id = $params['type'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {    
            //drop xref
            CatalogOptionValue::model()->deleteAllByAttributes(array('option_id' => $modelInstance->id));
            
            $modelInstance->save();    
            //save option values
            if (isset($params['option_value_names']) && count($params['option_value_names']) > 0)
            {
                foreach ($params['option_value_names'] as $i => $option_value_name)
                {
                    if ($option_value_name != '') {
                        $obj = new CatalogOptionValue();
                        $obj->option_id = $modelInstance->id;
                        $obj->name = $option_value_name;
                        $obj->ordering = $params['option_value_orderings'][$i];
                        $obj->save();
                    }
                }
            }
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop xref
            CatalogOptionValue::model()->deleteAllByAttributes(array('option_id' => $id));
            
            //delete object
            CatalogOption::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
    
    public function getNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `catalog_option`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
