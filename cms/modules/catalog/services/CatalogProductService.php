<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogProductService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new CatalogProduct();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();  
            
            foreach ($params['categories'] as $category_id) {
                $xref = new CatalogCategoryXref();
                $xref->category_id = $category_id;
                $xref->product_id = $modelInstance->id;
                $xref->save();
            }
            
            if (isset($params['stores']) && count($params['stores']) > 0) {
                foreach ($params['stores'] as $store_id) {
                $xref = new CatalogStoreProduct();
                    $xref->store_id = $store_id;
                    $xref->product_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            //handles the related products
            if (isset($params['related_products']) && count($params['related_products']) > 0)
            {
                foreach ($params['related_products'] as $i => $id2) {
                    $xref = new CatalogRelatedProduct();
                    $xref->id2 = $id2;
                    $xref->id1 = $modelInstance->id;
                    $xref->save();
                }
            }
            
            //handles the tier price
            if ( isset($params['tier_quantities']) && isset($params['tier_prices']) && 
                 count($params['tier_quantities']) > 0 && count($params['tier_prices']) > 0 &&
                 count($params['tier_quantities']) == count($params['tier_prices']) )
            {
                foreach ($params['tier_quantities'] as $i => $tier_quantity)
                {
                    $tp = new CatalogProductTierPrice();
                    $tp->product_id = $modelInstance->id;
                    $tp->quantity = $tier_quantity;
                    $tp->price = $params['tier_prices'][$i];
                    $tp->save();
                }
            }
            
            //handles the rental prices
            if (isset($params['rentalPriceForms']) && count($params['rentalPriceForms']) > 0)
            {
                foreach ($params['rentalPriceForms'] as $i => $rentalPriceForm)
                {
                    $rp = new CatalogProductRentalPrice();
                    $rp->product_id = $modelInstance->id;
                    $rp->period = $rentalPriceForm->period;
                    $rp->period_unit = $rentalPriceForm->period_unit;
                    $rp->price = $rentalPriceForm->price;
                    $rp->save();
                }
            }
            
            //handles the rental special prices
            if (isset($params['rentalSpecialPriceForms']) && count($params['rentalSpecialPriceForms']) > 0)
            {
                foreach ($params['rentalSpecialPriceForms'] as $i => $rentalSpecialPriceForm)
                {
                    $rsp = new CatalogProductRentalSpecialPrice();
                    $rsp->product_id = $modelInstance->id;
                    $rsp->day = $rentalSpecialPriceForm->day;
                    $rsp->time_start = $rentalSpecialPriceForm->time_start;
                    $rsp->time_end = $rentalSpecialPriceForm->time_end;
                    $rsp->price_prefix = $rentalSpecialPriceForm->price_prefix;
                    $rsp->price = $rentalSpecialPriceForm->price;
                    $rsp->save();
                }
            }
            
            //handles the product attributes
            if (isset($params['productAttributeForms']) && count($params['productAttributeForms']) > 0)
            {
                foreach ($params['productAttributeForms'] as $i => $productAttributeForm)
                {
                    $prodAttr = new CatalogProductAttribute();
                    $prodAttr->product_id = $modelInstance->id;
                    $prodAttr->attribute_id = $productAttributeForm->attribute_id;
                    $prodAttr->value = $productAttributeForm->value;
                    $prodAttr->save();
                }
            }
            
            //handles product options
            if (isset($params['productOptionForms']) && count($params['productOptionForms']) > 0)
            {
                foreach ($params['productOptionForms'] as $productOptionForm)
                {
                    $productOptionModel = new CatalogProductOption();
                    $productOptionModel->option_id = $productOptionForm->option_id;
                    $productOptionModel->product_id = $modelInstance->id;
                    $productOptionModel->required = $productOptionForm->required;
                    $productOptionModel->note = $productOptionForm->note;
                    $productOptionModel->element_id = $productOptionForm->element_id;
                    $productOptionModel->save();
                    
                    if (count($productOptionForm->optionValueForms) > 0)
                    {
                        foreach ($productOptionForm->optionValueForms as $productOptionValueForm)
                        {
                            $productOptionValueModel = new CatalogProductOptionValue();
                            $productOptionValueModel->product_option_id = $productOptionModel->id;
                            $productOptionValueModel->option_value_id = $productOptionValueForm->option_value_id;
                            $productOptionValueModel->substract_stock = $productOptionValueForm->substract_stock;
                            $productOptionValueModel->stock_on_hand = $productOptionValueForm->stock_on_hand;
                            $productOptionValueModel->minimum_order_quantity = $productOptionValueForm->minimum_order_quantity;
                            $productOptionValueModel->price_prefix = $productOptionValueForm->price_prefix;
                            $productOptionValueModel->price = $productOptionValueForm->price;
                            $productOptionValueModel->point_prefix = $productOptionValueForm->point_prefix;
                            $productOptionValueModel->point = $productOptionValueForm->point;
                            $productOptionValueModel->weight_prefix = $productOptionValueForm->weight_prefix;
                            $productOptionValueModel->weight = $productOptionValueForm->weight;
                            $productOptionValueModel->note = $productOptionValueForm->note;
                            $productOptionValueModel->save();
                        }
                    }
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = CatalogProduct::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            //drop all xref
            CatalogCategoryXref::model()->deleteAllByAttributes(array('product_id' => $modelInstance->id));
            CatalogStoreProduct::model()->deleteAllByAttributes(array('product_id' => $modelInstance->id));
            CatalogRelatedProduct::model()->deleteAllByAttributes(array('id1' => $modelInstance->id));
            CatalogProductTierPrice::model()->deleteAllByAttributes(array('product_id' => $modelInstance->id));
            CatalogProductRentalPrice::model()->deleteAllByAttributes(array('product_id' => $modelInstance->id));
            CatalogProductRentalSpecialPrice::model()->deleteAllByAttributes(array('product_id' => $modelInstance->id));
            CatalogProductAttribute::model()->deleteAllByAttributes(array('product_id' => $modelInstance->id));
            CatalogProductOption::model()->deleteAllByAttributes(array('product_id' => $modelInstance->id));
            
            $modelInstance->save();       
            
            foreach ($params['categories'] as $category_id) {
                $xref = new CatalogCategoryXref();
                $xref->category_id = $category_id;
                $xref->product_id = $modelInstance->id;
                $xref->save();
            }
            
            if (isset($params['stores']) && count($params['stores']) > 0) {
                foreach ($params['stores'] as $store_id) {
                $xref = new CatalogStoreProduct();
                    $xref->store_id = $store_id;
                    $xref->product_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            if (isset($params['related_products']) && count($params['related_products']) > 0)
            {                
                foreach ($params['related_products'] as $i => $id2) {                    
                    $xref = new CatalogRelatedProduct();
                    $xref->id2 = $id2;
                    $xref->id1 = $modelInstance->id;
                    $xref->save();
                }
            }
            
            //handles the tier price
            if ( isset($params['tier_quantities']) && isset($params['tier_prices']) && 
                 count($params['tier_quantities']) > 0 && count($params['tier_prices']) > 0 &&
                 count($params['tier_quantities']) == count($params['tier_prices']) )
            {
                foreach ($params['tier_quantities'] as $i => $qty)
                {
                    $price = $params['tier_prices'][$i];
                    if ($qty != null && $price != null) {
                        $tp = new CatalogProductTierPrice();
                        $tp->product_id = $modelInstance->id;
                        $tp->quantity = $qty;
                        $tp->price = $params['tier_prices'][$i];
                        $tp->save();
                    }
                }
            }
            
            //handles the rental prices
            if (isset($params['rentalPriceForms']) && count($params['rentalPriceForms']) > 0)
            {
                foreach ($params['rentalPriceForms'] as $i => $rentalPriceForm)
                {
                    $rp = new CatalogProductRentalPrice();
                    $rp->product_id = $modelInstance->id;
                    $rp->period = $rentalPriceForm->period;
                    $rp->period_unit = $rentalPriceForm->period_unit;
                    $rp->price = $rentalPriceForm->price;
                    $rp->save();
                }
            }
            
            //handles the rental special prices
            if (isset($params['rentalSpecialPriceForms']) && count($params['rentalSpecialPriceForms']) > 0)
            {
                foreach ($params['rentalSpecialPriceForms'] as $i => $rentalSpecialPriceForm)
                {
                    $rsp = new CatalogProductRentalSpecialPrice();
                    $rsp->product_id = $modelInstance->id;
                    $rsp->day = $rentalSpecialPriceForm->day;
                    $rsp->time_start = $rentalSpecialPriceForm->time_start;
                    $rsp->time_end = $rentalSpecialPriceForm->time_end;
                    $rsp->price_prefix = $rentalSpecialPriceForm->price_prefix;
                    $rsp->price = $rentalSpecialPriceForm->price;
                    $rsp->save();
                }
            }
            
            //handles the product attributes
            if (isset($params['productAttributeForms']) && count($params['productAttributeForms']) > 0)
            {
                foreach ($params['productAttributeForms'] as $i => $productAttributeForm)
                {
                    $prodAttr = new CatalogProductAttribute();
                    $prodAttr->product_id = $modelInstance->id;
                    $prodAttr->attribute_id = $productAttributeForm->attribute_id;
                    $prodAttr->value = $productAttributeForm->value;
                    $prodAttr->save();
                }
            }
            
            //handles product options
            if (isset($params['productOptionForms']) && count($params['productOptionForms']) > 0)
            {
                foreach ($params['productOptionForms'] as $productOptionForm)
                {
                    $productOptionModel = new CatalogProductOption();
                    $productOptionModel->option_id = $productOptionForm->option_id;
                    $productOptionModel->product_id = $modelInstance->id;
                    $productOptionModel->required = $productOptionForm->required;
                    $productOptionModel->note = $productOptionForm->note;
                    $productOptionModel->element_id = $productOptionForm->element_id;
                    $productOptionModel->save();
                    
                    if (count($productOptionForm->optionValueForms) > 0)
                    {
                        foreach ($productOptionForm->optionValueForms as $productOptionValueForm)
                        {
                            $productOptionValueModel = new CatalogProductOptionValue();
                            $productOptionValueModel->product_option_id = $productOptionModel->id;
                            $productOptionValueModel->option_value_id = $productOptionValueForm->option_value_id;
                            $productOptionValueModel->substract_stock = $productOptionValueForm->substract_stock;
                            $productOptionValueModel->stock_on_hand = $productOptionValueForm->stock_on_hand;
                            $productOptionValueModel->minimum_order_quantity = $productOptionValueForm->minimum_order_quantity;
                            $productOptionValueModel->price_prefix = $productOptionValueForm->price_prefix;
                            $productOptionValueModel->price = $productOptionValueForm->price;
                            $productOptionValueModel->point_prefix = $productOptionValueForm->point_prefix;
                            $productOptionValueModel->point = $productOptionValueForm->point;
                            $productOptionValueModel->weight_prefix = $productOptionValueForm->weight_prefix;
                            $productOptionValueModel->weight = $productOptionValueForm->weight;
                            $productOptionValueModel->note = $productOptionValueForm->note;
                            $productOptionValueModel->save();
                        }
                    }
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        $transaction = Yii::app()->db->beginTransaction();
        try {
            //drop all xref
            CatalogCategoryXref::model()->deleteAllByAttributes(array('product_id' => $id));
            CatalogStoreProduct::model()->deleteAllByAttributes(array('product_id' => $id));
            CatalogRelatedProduct::model()->deleteAllByAttributes(array('id1' => $id));
            CatalogProductTierPrice::model()->deleteAllByAttributes(array('product_id' => $id));
            CatalogProductRentalPrice::model()->deleteAllByAttributes(array('product_id' => $id));
            CatalogProductRentalSpecialPrice::model()->deleteAllByAttributes(array('product_id' => $id));
            CatalogProductAttribute::model()->deleteAllByAttributes(array('product_id' => $id));
            CatalogProductOption::model()->deleteAllByAttributes(array('product_id' => $id));
            
            $object = CatalogProduct::model()->findByPk($id);
            $object->delete();
            
            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function publish($id) {
        $objects = array();
        $modelInstance = CatalogProduct::model()->findByPk($id);
        $modelInstance->published = 1;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $objects = array();
        $modelInstance = CatalogProduct::model()->findByPk($id);
        $modelInstance->published = 0;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();

//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
    
    public function getNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `catalog_product`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
    
    public function setProduct(CatalogProduct $product) {
        Yii::app()->user->setState('product', $product);
    }
    
    public function getProduct()
    {
        return Yii::app()->user->product;
    }
    
    public function updateCatalogFiles($pfids=array())
    {
       if (count($pfids))
       {
           foreach ($pfids as $pfid)
           {
               $objFile = CatalogProductFile::model()->findByPk($pfid);
               if ($objFile != null)
               {
                   $objFile->label = $_POST['file_labels'][$objFile->id];
                   $objFile->ordering = $_POST['file_orderings'][$objFile->id];

                   $published = (isset($_POST['file_published'][$objFile->id])) ? $_POST['file_published'][$objFile->id] : 0;
                   $objFile->published = $published;

                   if (isset($_POST['file_main']) && $_POST['file_main'] == $objFile->id) {
                       $objFile->main = 1;
                   } else {
                       $objFile->main = 0;
                   }

               }
               $objFile->save();
           }
       }
    }
    
    public function deleteCatalogFiles($rpfids=array())
    {
        if (is_array($rpfids) && count($rpfids) > 0)
        {
            foreach ($rpfids as $rpfid)
            {
                $objFile = UploadedFile::model()->findByPk($rpfid);
                if ($objFile != null) {
                    $file_type = null;
                    $arr = explode("/", $objFile->file_type);
                    switch ($arr[0])
                    {
                        case "image":
                        default:
                            $file_type = "images";
                            break;
                    }
                    $uploadPath = '../'.Yii::app()->params['uploadFolder']."/".Yii::app()->user->saasClient->site_address."/".$file_type;
                    Yii::app()->fileUploadService->deleteFile($objFile->file_name, $uploadPath);
                    $objFile->delete();
                }
            }
        }
    }
    
}
