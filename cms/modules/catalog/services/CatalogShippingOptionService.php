<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogShippingOptionService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new CatalogShippingOption();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();       
            
            if (isset($params['stores']) && count($params['stores']) > 0) {
                foreach ($params['stores'] as $store_id) {
                $xref = new CatalogStoreShippingOption();
                    $xref->store_id = $store_id;
                    $xref->shipping_option_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = CatalogShippingOption::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {            
            //drop all xref
            CatalogStoreShippingOption::model()->deleteAllByAttributes(array('shipping_option_id' => $modelInstance->id));
            
            $modelInstance->save();        
            
            if (isset($params['stores']) && count($params['stores']) > 0) {
                foreach ($params['stores'] as $store_id) {
                $xref = new CatalogStoreShippingOption();
                    $xref->store_id = $store_id;
                    $xref->shipping_option_id = $modelInstance->id;
                    $xref->save();
                }
            }
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop all xref
            CatalogStoreShippingOption::model()->deleteAllByAttributes(array('shipping_option_id' => $id));
            
            $object = CatalogShippingOption::model()->findByPk($id);
            $old_file = $object->file;
            $object->delete();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $objects = array();
        $modelInstance = CatalogShippingOption::model()->findByPk($id);
        $modelInstance->published = 1;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $objects = array();
        $modelInstance = CatalogShippingOption::model()->findByPk($id);
        $modelInstance->published = 0;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();
//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
}
