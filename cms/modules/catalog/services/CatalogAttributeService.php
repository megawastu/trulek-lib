<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogAttributeService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new CatalogAttribute();
        $modelInstance->setAttributes($params, false);
        $modelInstance->attribute_group_id = $params['group'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save(); 
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = CatalogAttribute::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        $modelInstance->attribute_group_id = $params['group'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {    
            $modelInstance->save();
            
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            
            //delete object
            CatalogAttribute::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
    
    public function getNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `catalog_attribute`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
