<?php

class CatalogProductImageUploadForm extends CFormModel {

   public $image;
   
   /**
    *
    * @return <Array>
    */
   public function rules()
   {
       return array(
          array('image', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
       );
   }

   /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(           
           'image' => Yii::t('catalogs', 'Image'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'image' => 'image',
       );
   }
   
}
