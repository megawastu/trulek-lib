<?php

class CatalogCategoryForm extends CFormModel
{
    public $id;
    public $parent_id;
    public $name;
    public $urlkey;
    public $published;
    public $ordering;
    public $description;
    public $meta_desc;
    public $meta_keyword;
    public $file;
    
    public $delete_file;
    
    public $stores = array();

    public function rules()
    {
        return array(
           array('name, urlkey, published, ordering, stores', 'required'),
           array('name', 'validateUniqueName'),
           array('urlkey', 'validateUniqueUrlkey'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'parent_id' => Yii::t('system', 'Parent'),
           'name' => Yii::t('system', 'Name'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
           'published' => Yii::t('system', 'Published'),
           'ordering' => Yii::t('system', 'Sort Order'), 
           'description' => Yii::t('system', 'Description'),                     
           'meta_desc' => Yii::t('system', 'Meta Description'),
           'meta_keyword' => Yii::t('system', 'Meta Keyword'),
           'file' => Yii::t('system', 'File Name'),
           'delete_file' => Yii::t('system', 'Delete File'),
           'stores' => Yii::t('system', 'Stores'), 
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'parent_id' => 'parent_id',
           'name' => 'name',
           'urlkey' => 'urlkey',
           'published' => 'published',
           'ordering' => 'ordering', 
           'description' => 'description',                     
           'meta_desc' => 'meta_desc',
           'meta_keyword' => 'meta_keyword',
           'file' => 'file',
           'delete_file' => 'delete_file',
           'stores' => 'stores', 
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueName()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`name` = '".$this->name."' AND `id` <> '".$this->id."'";

       $total = CatalogCategory::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('name', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Category name')));
       }
   }

   /**
    * @return void
    */
   public function validateUniqueUrlkey()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`urlkey` = '".$this->urlkey."' AND `id` <> '".$this->id."'";

       $total = CatalogCategory::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('urlkey', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Urlkey')));
       }
   }   
   
   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
   
}
