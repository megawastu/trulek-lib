<?php

class CatalogProductReview extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }
    
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_product_review';
    }
    
    public function relations()
    {
        return array(
            'product' => array(self::BELONGS_TO, 'CatalogProduct', 'product_id'),            
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function scopes()
    {
       return array(
           'published' => array("conditions" => "`published` = '1'"),
           'unpublished' => array("conditions" => "`published` = '0'"),
       );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}