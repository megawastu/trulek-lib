<?php

class CatalogProductFileForm extends CFormModel
{
    public $id;
    public $product_id;
    public $caption;
    public $description;
    public $meta_desc;
    public $meta_keywords;
    public $file_name;
    public $urlkey; 
    public $published;
    public $ordering;
    public $main;
    
    public $delete_file;

    public function rules()
    {
        return array(
           array('product_id, caption, urlkey, published, ordering', 'required'),
           array('urlkey', 'validateUniqueUrlkey'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'product_id' => Yii::t('system', 'Product'),
           'caption' => Yii::t('system', 'Caption'),
           'description' => Yii::t('system', 'Description'),
           'meta_desc' => Yii::t('system', 'Meta Description'),
           'meta_keywords' => Yii::t('system', 'Meta Keywords'),
           'file_name' => Yii::t('system', 'File Name'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
           'published' => Yii::t('system', 'Published'),
           'ordering' => Yii::t('system', 'Sort Order'),           
           'main' => Yii::t('system', 'Main'),
           'delete_file' => Yii::t('system', 'Delete File'), 
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'product_id' => 'product_id',
           'caption' => 'caption',
           'description' => 'description',
           'meta_desc' => 'meta_desc',
           'meta_keywords' => 'meta_keywords',
           'file_name' => 'file_name',
           'urlkey' => 'urlkey',
           'published' => 'published',
           'ordering' => 'ordering',           
           'main' => 'main',
           'delete_file' => 'delete_file',  
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueUrlkey()
   {
       $criteria = new CDbCriteria();
       $criteria->condition = "`urlkey` = '".$this->urlkey."' AND `id` <> '".$this->id."'";

       $total = CatalogProduct::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('urlkey', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName' => 'Urlkey')));
       }
   }
}
