<?php

class CatalogOptionForm extends CFormModel
{
    public $id;
    public $type;
    public $name;
    public $ordering;
    public $description;

    public $option_value_names = array();
    public $option_value_orderings = array();
    
    public function rules()
    {
        return array(
           array('type, name, ordering, option_value_names, option_value_orderings', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'type' => Yii::t('system', 'Type'),
           'name' => Yii::t('system', 'Name'),
           'ordering' => Yii::t('system', 'Ordering'),
           'option_value_names' => Yii::t('system', 'Option Value Names'),
           'option_value_orderings' => Yii::t('system', 'Option Value Orderings'),
        );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'type' => 'type',
           'name' => 'name',
           'ordering' => 'ordering',
           'option_value_names' => 'option_value_names',
           'option_value_orderings' => 'option_value_orderings',
       );
   }
   
}
