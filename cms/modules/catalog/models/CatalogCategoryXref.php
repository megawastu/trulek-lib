<?php

class CatalogCategoryXref extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_product_categories';
    }
    
    public function rules()
    {
        return array(
            array('product_id, category_id', 'required')
        );
    }
}

