<?php

class CatalogCurrencyForm extends CFormModel
{
    public $id;
    public $name;
    public $code;
    public $symbol;
    public $symbol_position;
    public $decimal_place;
    public $value;
    public $enabled;
    
    public function rules()
    {
        return array(
           array('name, code, symbol, symbol_position, decimal_place, value, enabled', 'required'),
           array('code', 'validateUniqueCode'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
           'code' => Yii::t('system', 'Code'),
           'symbol' => Yii::t('system', 'Symbol'),
           'symbol_position' => Yii::t('system', 'Symbol Position'),
           'decimal_place' => Yii::t('system', 'Decimal Place'),
           'value' => Yii::t('system', 'Value'),
           'enabled' => Yii::t('system', 'Enabled'),
        );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'code' => 'code',
           'symbol' => 'symbol',
           'symbol_position' => 'symbol_position',
           'decimal_place' => 'decimal_place',
           'value' => 'value',
           'enabled' => 'enabled',
       );
   }   
   
   /**
    * @return void
    */
   public function validateUniqueCode()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`code` = '".$this->code."' AND `id` <> '".$this->id."'";

       $total = CatalogCurrency::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('code', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Code')));
       }
   } 
}
