<?php

class CatalogRelatedProduct extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_related_products';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'products' =>array(self::MANY_MANY, 'CatalogProduct', 'catalog_related_products(id1, id2)'),
        );
    }
    
    public function scopes()
    {
        return array(
        );
    }

    public function behaviors()
    {
        return array(
            //'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}