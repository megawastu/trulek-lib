<?php

class CatalogProductOptionForm extends CFormModel
{
    public $id;
    public $option_id;
    public $required;
    public $note;
    public $element_id;
    public $optionValueForms = array();
    
    public function rules()
    {
        return array(
           array('option_id, required', 'required'),
        );
    }
   
}
