<?php

class CatalogShippingOptionForm extends CFormModel {

   public $id;
   public $name;
   public $unique_name;
   public $description;
   public $url;
   public $file;
   public $published;
   
   public $stores = array();
   
   public $delete_file;
   
   
   /**
    *
    * @return <Array>
    */
   public function rules()
   {
       return array(
          array('name, unique_name, published, stores', 'required'),
          array('unique_name', 'validateUniqueName'),
		  array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
       );
   }
   
   /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('catalog', 'Id'),
           'name' => Yii::t('catalog', 'Name'),
           'unique_name' => Yii::t('catalog', 'Unique Name'),
           'description' => Yii::t('catalog', 'Description'),
           'url' => Yii::t('catalog', 'Url'),
		   'file' => Yii::t('catalog', 'File'),
		   'published' => Yii::t('catalog', 'Published'),
           'delete_file' => Yii::t('system', 'Delete File'),
		   'stores' => Yii::t('system', 'Stores'), 
        );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'unique_name' => 'unique_name',
           'description' => 'description',
           'url' => 'url',
		   'file' => 'file',
		   'published' => 'published',
           'delete_file' => 'delete_file',
		   'stores' => 'stores', 
       );
   }   
   
   /**
    * @return void
    */
   public function validateUniqueName()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`unique_name` = '".$this->unique_name."' AND `id` <> '".$this->id."'";

       $total = CatalogShippingOption::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('unique_name', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Unique Name')));
       }
   } 
   
   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
   
}
