<?php

class CatalogProductVideoUploadForm extends CFormModel {

   public $video;

   /**
    * 
    * @return <Array>
    */
   public function rules()
   {
       return array(
          array('video', 'file', 'types'=> 'avi, mp4, swf, mpg, flv, mov',
                                'maxSize'=>1024 * 1024 * 80, // 80MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .avi, .mp4, .swf, .mpg, .flv, .mov'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 80MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
       );
   }

   /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(           
           'video' => Yii::t('catalogs', 'Video'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'video' => 'video',
       );
   }
   
}
