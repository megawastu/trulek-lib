<?php

class CatalogProductForm extends CFormModel
{
    public $id;
    public $categories=array();
    public $sku;
    public $name;
    public $weight;
    public $width;
    public $height;
    public $length;    
    public $short_desc;
    public $long_desc;
    public $urlkey;
    public $meta_desc;
    public $meta_keyword;
    public $published;
    public $ordering;
    public $price;
    public $special_price;
    public $special_price_from;
    public $special_price_until;
    public $new_from;
    public $new_until;
    
    public $substract_stock;
    public $stock_on_hand;
    public $stock_status;
    public $minimum_order_quantity;
    public $manufacturer_id;
    public $date_available;
    public $related_products = array();
    public $require_shipping;
    
    public $tier_quantities = array();
    public $tier_prices = array();
    
    public $rentalPriceForms = array();    
    public $rentalSpecialPriceForms = array();
    public $productOptionForms = array();
    public $productAttributeForms = array();
    
    public $stores = array();
    
    public function rules()
    {
        return array(
           array('name, sku, published, ordering, urlkey, categories, substract_stock, stock_status, require_shipping, stores', 'required'),
           array('sku', 'validateUniqueSku'),
           array('urlkey', 'validateUniqueUrlkey'),
           array(
              'special_price_until',
              'compare',
              'compareAttribute'=>'special_price_from',
              'operator'=>'>',
              'allowEmpty'=>true ,
              'message'=>'{attribute} must be greater than "{compareValue}".'
            ),
           array(
              'new_until',
              'compare',
              'compareAttribute'=>'new_from',
              'operator'=>'>',
              'allowEmpty'=>true ,
              'message'=>'{attribute} must be greater than "{compareValue}".'
            ),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('catalogs', 'Id'),
           'categories' => Yii::t('catalogs', 'Categories'),
           'sku' => Yii::t('catalogs', 'SKU'),
           'name' => Yii::t('catalogs', 'Name'),
           'weight' => Yii::t('catalogs', 'Weight'),
           'width' => Yii::t('catalogs', 'Width'),
           'height' => Yii::t('catalogs', 'Height'),
           'length' => Yii::t('catalogs', 'Length'),
           'short_desc' => Yii::t('catalogs', 'Short Description'),
           'long_desc' => Yii::t('catalogs', 'Long Description'),
           'urlkey' => Yii::t('catalogs', 'SEO Keyword'),
           'meta_desc' => Yii::t('catalogs', 'Meta Description'),
           'meta_keyword' => Yii::t('catalogs', 'Meta Keyword'),
           'published' => Yii::t('catalogs', 'Published'),
           'ordering' => Yii::t('catalogs', 'Sort Order'),
           'price' => Yii::t('catalogs', 'Price'),
           'special_price' => Yii::t('catalogs', 'Special Price'),
           'special_price_from' => Yii::t('catalogs', 'Special Price From'),
           'special_price_until' => Yii::t('catalogs', 'Special Price Until'),
           'new_from' => Yii::t('catalogs', 'New From'),
           'new_until' => Yii::t('catalogs', 'New Until'),
           'tier_quantities' => Yii::t('catalogs', 'Tier Quantity'),
           'substract_stock' => Yii::t('catalogs', 'Substract Stock'),
           'stock_on_hand' => Yii::t('catalogs', 'Stock on Hand'),
           'stock_status' => Yii::t('catalogs', 'Stock Status'),
           'minimum_order_quantity' => Yii::t('catalogs', 'Minimum Order Quantity'),
           'manufacturer_id' => Yii::t('catalogs', 'Manufacturer'),
           'date_available' => Yii::t('catalogs', 'Date Available'),
           'related_products' => Yii::t('catalogs', 'Related Products'),
           'require_shipping' => Yii::t('catalogs', 'Requires Shipping'),
           'tier_quantites' => Yii::t('catalogs', 'Tier Quantities'),
           'tier_prices' => Yii::t('catalogs', 'Tier Prices'),
           'stores' => Yii::t('system', 'Stores'), 
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'categories' => 'categories',
           'sku' => 'sku',
           'name' => 'name',
           'weight' =>'weight',
           'width' => 'width',
           'height' => 'height',
           'length' => 'length',
           'short_desc' => 'short_desc',
           'long_desc' => 'long_desc',
           'urlkey' => 'urlkey',
           'meta_desc' => 'meta_desc',
           'meta_keyword' => 'meta_keyword',
           'published' => 'published',
           'ordering' => 'ordering',
           'price' => 'price',
           'special_price' => 'special_price',
           'special_price_from' => 'special_price_from',
           'special_price_until' => 'special_price_until',
           'new_from' => 'new_from',
           'new_until' => 'new_until',
           'substract_stock' => 'substract_stock',
           'stock_on_hand' => 'stock_on_hand',
           'stock_status' => 'stock_status',
           'minimum_order_quantity' => 'minimum_order_quantity',
           'manufacturer_id' => 'manufacturer_id',
           'date_available' => 'date_available',
           'related_products' => 'related_products',
           'require_shipping' => 'require_shipping',
           'tier_quantities' => 'tier_quantities',
           'tier_prices' => 'tier_prices',           
           'rentalPriceForms' => 'rentalPriceForms',  
           'rentalSpecialPriceForms' => 'rentalSpecialPriceForms',
           'productOptionForms' => 'productOptionForms',
           'productAttributeForms' => 'productAttributeForms',
           'stores' => 'stores', 
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueSku()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`sku` = '".$this->sku."' AND `id` <> '".$this->id."'";

       $total = CatalogProduct::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('sku', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName' => 'Product SKU')));
       }
   }

   /**
    * @return void
    */
   public function validateUniqueUrlkey()
   {
       $criteria = new CDbCriteria();
       $criteria->condition = "`urlkey` = '".$this->urlkey."' AND `id` <> '".$this->id."'";

       $total = CatalogProduct::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('urlkey', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName' => 'Urlkey')));
       }
   }
}
