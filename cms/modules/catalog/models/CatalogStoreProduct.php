<?php

class CatalogStoreProduct extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_store_products';
    }
    
    public function rules()
    {
        return array(
            array('store_id, product_id', 'required')
        );
    }
}

