<?php

class CatalogStoreSetting extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_store_setting';
    }

    public function relations()
    {
        return array(
            'weightClass' => array(self::BELONGS_TO, 'CatalogWeightClass', 'weight_class_id'),
            'lengthClass' => array(self::BELONGS_TO, 'CatalogLengthClass', 'length_class_id'),
            'currency' => array(self::BELONGS_TO, 'CatalogCurrency', 'currency_id'),
            'country' => array(self::BELONGS_TO, 'CatalogCountry', 'country_id'),
        );
    }
    
    public function scopes()
    {
        return array (
            'default' => array("condition" => "`default_store`='1'"),
            'enabled' => array("condition" => "`enabled` = '1'"),
            'disabled' => array("condition" => "`enabled` = '0'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
            'SaveUploadedFileBehavior'=> 'trulek.extensions.fileuploader.behaviors.SaveUploadedFileBehavior',
        );
    }
}