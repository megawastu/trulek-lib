<?php

class CatalogProductTierPrice extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_product_tiering_price';
    }
    
    public function relations()
    {
        return array(
            'product' => array(self::BELONGS_TO, 'CatalogProduct', 'product_id'),            
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function scopes()
    {
       return array(
       );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}