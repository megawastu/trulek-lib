<?php

class CatalogStoreSettingForm extends CFormModel {

   public $id;
   public $store_name;
   public $store_owner;
   public $store_url;
   public $enabled;
   public $address;
   public $email;
   public $phone;
   public $fax;
   public $city;
   public $country;
   public $show_google_map;
   public $google_map;
   public $file;
   public $theme;   
   public $weightClass;
   public $lengthClass;
   public $currency;
   public $auto_update_currency;
   public $decimal_places;
   public $catalog_only;
   public $invoice_prefix;
   public $only_show_price_to_logged_in_account;
   public $account_approval_required;
   public $enable_guest_checkout;
   public $enable_sharing;
   public $display_stock;
   public $show_out_of_stock_warning;
   public $stock_checkout;
   public $allow_product_review;
   public $show_related_product;
   public $total_related_product_shown;
   public $default_items_per_page;
   public $default_store;
   
   public $delete_file;
   
   public $operational_time;
   
   
   /**
    *
    * @return <Array>
    */
   public function rules()
   {
       return array(
          array('store_name, store_owner, store_url, address, email, phone, enabled
                 , country, city, theme, weightClass, lengthClass, currency
                 , auto_update_currency, decimal_places, catalog_only, invoice_prefix
                 , only_show_price_to_logged_in_account, account_approval_required
                 , enable_guest_checkout, enable_sharing, display_stock
                 , show_out_of_stock_warning, stock_checkout, allow_product_review
                 , show_related_product, total_related_product_shown, default_items_per_page, default_store
                 , show_google_map', 'required'),
          array('store_name', 'validateUniqueStoreName'),
          array('store_url', 'validateUniqueStoreUrl'),
		  array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
       );
   }
   
   /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('catalog', 'Id'),
           'store_name' => Yii::t('catalog', 'Store Name'),
           'store_owner' => Yii::t('catalog', 'Store Owner'),
           'store_url' => Yii::t('catalog', 'Store Url'),
           'enabled' => Yii::t('catalog', 'Enabled'),
           'address' => Yii::t('catalog', 'Address'),
           'email' => Yii::t('catalog', 'Email'),
           'phone' => Yii::t('catalog', 'Phone'),
           'fax' => Yii::t('catalog', 'Fax'),
           'city' => Yii::t('catalog', 'City'),
           'country' => Yii::t('catalog', 'Country'),
           'show_google_map' => Yii::t('catalog', 'Show Google Map'),
           'google_map' => Yii::t('system', 'Google Map'),
           'file' => Yii::t('catalog', 'Store Logo'),
           'theme' => Yii::t('catalog', 'Theme'),
           'weightClass' => Yii::t('catalog', 'Weight Class'),
           'lengthClass' => Yii::t('catalog', 'Length Class'),
           'currency' => Yii::t('catalog', 'Currency'),
           'auto_update_currency' => Yii::t('catalog', 'Auto Update Currency'),
           'decimal_places' => Yii::t('catalog', 'Decimal Places'),
           'catalog_only' => Yii::t('catalog', 'Use as Catalog Only'),
           'invoice_prefix' => Yii::t('catalog', 'Invoice Prefix'),
           'only_show_price_to_logged_in_account' => Yii::t('catalog', 'Only Show Price to Logged-in Account'),
           'account_approval_required' => Yii::t('catalog', 'Account Approval Required'),
           'enable_guest_checkout' => Yii::t('catalog', 'Enable Guest Checkout'),
           'enable_sharing' => Yii::t('catalog', 'Enable Sharing'),
           'display_stock' => Yii::t('catalog', 'Display Stock'),
           'show_out_of_stock_warning' => Yii::t('catalog', 'Show Out of Stock Warning'),
           'stock_checkout' => Yii::t('catalog', 'Stock Checkout'),
           'allow_product_review' => Yii::t('catalog', 'Allow Product Review'),
           'show_related_product' => Yii::t('catalog', 'Show Related Product'),
           'total_related_product_shown' => Yii::t('catalog', 'Total Related Product Shown'),
           'default_items_per_page' => Yii::t('catalog', 'Default Item per Page'),
           'default_store' => Yii::t('catalog', 'Default Store'),
           'delete_file' => Yii::t('system', 'Delete File'),
		   'operational_time' => Yii::t('catalog', 'Operating Hours'),
        );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'store_name' => 'store_name',
           'store_owner' => 'store_owner',
           'store_url' => 'store_url',
           'enabled' => 'enabled',
           'address' => 'address',
           'email' => 'email',
           'phone' => 'phone',
           'fax' => 'fax',
           'city' => 'city',
           'country' => 'country',
           'show_google_map' => 'show_google_map',
           'google_map' => 'google_map',
           'file' => 'file',
           'theme' => 'theme',
           'weightClass' => 'weightClass',
           'lengthClass' => 'lengthClass',
           'currency' => 'currency',
           'auto_update_currency' => 'auto_update_currency',
           'decimal_places' => 'decimal_places',
           'catalog_only' => 'catalog_only',
           'invoice_prefix' => 'invoice_prefix',
           'only_show_price_to_logged_in_account' => 'only_show_price_to_logged_in_account',
           'account_approval_required' => 'account_approval_required',
           'enable_guest_checkout' => 'enable_guest_checkout',
           'enable_sharing' => 'enable_sharing',
           'display_stock' => 'display_stock',
           'show_out_of_stock_warning' => 'show_out_of_stock_warning',
           'stock_checkout' => 'stock_checkout',
           'allow_product_review' => 'allow_product_review',
           'show_related_product' => 'show_related_product',
           'total_related_product_shown' => 'total_related_product_shown',
           'default_items_per_page' => 'default_items_per_page',
           'default_store' => 'default_store',
		   'delete_file' => 'delete_file',
		   'operational_time' => 'operational_time',
       );
   }   
   
   /**
    * @return void
    */
   public function validateUniqueStoreName()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`store_name` = '".$this->store_name."' AND `id` <> '".$this->id."'";

       $total = CatalogStoreSetting::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('store_name', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Store Name')));
       }
   } 
   
   /**
    * @return void
    */
   public function validateUniqueStoreUrl()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`store_url` = '".$this->store_url."' AND `id` <> '".$this->id."'";

       $total = CatalogStoreSetting::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('store_url', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Store Url')));
       }
   } 
   
   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
   
}
