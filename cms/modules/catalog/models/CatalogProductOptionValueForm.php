<?php

class CatalogProductOptionValueForm extends CFormModel
{
    public $id;
    public $option_value_id;
    public $substract_stock;
    public $stock_on_hand;
    public $minimum_order_quantity;
    public $price_prefix;
    public $price;
    public $point_prefix;
    public $point;
    public $weight_prefix;
    public $weight;
    public $note;
    
    public function rules()
    {
        return array(
           array('option_value_id, substract_stock 
                  , stock_on_hand, minimum_order_quantity, price_prefix
                  , minimum_order_quantity, price_prefix, price, point_prefix, point
                  , weight_prefix, weight, note'
                  , 'required'),
        );
    }
   
}
