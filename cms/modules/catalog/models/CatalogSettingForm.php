<?php

class CatalogSettingForm extends CFormModel {

   public $id;
   public $weight_measurement_symbol;
   public $length_measurement_symbol;
   public $currency_symbol;
   public $decimals;
   public $decimal_symbol;
   public $thousands_separator;
   public $show_price;
   public $enable_sharing;
   public $enable_facebook_like;
   public $enable_comments;   
   public $show_related_products;
   public $number_of_related_products;
   
   /**
    *
    * @return <Array>
    */
   public function rules()
   {
       return array(
          array('weight_measurement_symbol, length_measurement_symbol, currency_symbol, decimals, decimal_symbol, thousands_separator, show_price, enable_sharing, enable_facebook_like, enable_comments, show_related_products, number_of_related_products', 'required'),
       );
   }

   /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('catalog', 'Id'),
           'weight_measurement_symbol' => Yii::t('catalog', 'Weight Measurement Symbol'),
           'length_measurement_symbol' => Yii::t('catalog', 'Length Measurement Symbol'),
           'currency_symbol' => Yii::t('catalog', 'Currency Symbol'),
           'decimals' => Yii::t('catalog', 'Number of Decimals'),
           'decimal_symbol' => Yii::t('catalog', 'Decimal Symbol'),
           'thousands_separator' => Yii::t('catalog', 'Thousands Separator'),
           'show_price' => Yii::t('catalog', 'Show Price'),
           'enable_sharing' => Yii::t('catalog', 'Enable Sharing'),
           'enable_facebook_like' => Yii::t('catalog', 'Enable Facebook Like'),
           'enable_comments' => Yii::t('catalog', 'Enable Comments'),
           'show_related_products' => Yii::t('catalog', 'Show Related Products'),
           'number_of_related_products' => Yii::t('catalog', 'Number of Related Products to Show'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'weight_measurement_symbol' => 'weight_measurement_symbol',
           'length_measurement_symbol' => 'length_measurement_symbol',
           'currency_symbol' => 'currency_symbol',
           'decimals' => 'decimals',
           'decimal_symbol' => 'decimal_symbol',
           'thousands_separator' => 'thousands_separator',
           'show_price' => 'show_price',
           'enable_sharing' => 'enable_sharing',
           'enable_facebook_like' => 'enable_facebook_like',
           'enable_comments' => 'enable_comments',
           'show_related_products' => 'show_related_products',
           'number_of_related_products' => 'number_of_related_products',
       );
   }   
}
