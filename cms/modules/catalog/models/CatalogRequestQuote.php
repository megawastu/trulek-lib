<?php

Yii::import('trulek.extensions.fileuploader.enums.UploadTypeEnum');

class CatalogRequestQuote extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_quote_request';
    }
    
    public function relations()
    {
        return array(
            
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function scopes()
    {
       return array(
           'status' => array(),
           'priority' => array(),
       );
    }
	
	public function status($status=1)
	{
		$this->getDbCriteria()->mergeWith(array(
			"condition" => "`status` = '".$status."'",
		));
		return $this;
	}
	
	public function priority($priority=4)
	{
		$this->getDbCriteria()->mergeWith(array(
			"condition" => "`priority` = '".$priority."'",
		));
		return $this;
	}

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}