<?php

class CatalogOption extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_option';
    }
    
    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
           'type'=>array(self::BELONGS_TO, 'CatalogOptionType', 'option_type_id'),
           'values'=>array(self::HAS_MANY, 'CatalogOptionValue', 'option_id'),
           'valueCount'=>array(self::STAT, 'CatalogOptionValue', 'option_id'),
        );
    }
    
    public function scopes()
    {
        return array(
            
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}