<?php

class CatalogCategory extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_category';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
           'products'=>array(self::MANY_MANY, 'CatalogProduct', 'catalog_product_categories(product_id, category_id)'),
           'productCount'=>array(self::STAT, 'CatalogProduct', 'catalog_product_categories(product_id, category_id)'),
           'stores'=>array(self::MANY_MANY, 'CatalogStoreSetting', 'catalog_store_categories(store_id, category_id)'),
           'storeCount'=>array(self::STAT, 'CatalogStoreSetting', 'catalog_store_categories(store_id, category_id)'),
        );
    }
    
    public function scopes()
    {
        return array(
            "published" => array("condition" => "`t`.`published`='1'"),
            "unpublished" => array("condition" => "`t`.`published`='0'"),
            'topparents'=>array(
                    'condition' => 'parent_id is NULL',
                    'order' => 'position ASC',
           ),
           'childs'=>array(
                    'condition' => 'parent_id is not NULL',
                    'order' => 'position ASC',
           )
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
            'SaveUploadedFileBehavior'=> 'trulek.extensions.fileuploader.behaviors.SaveUploadedFileBehavior',
        );
    }
}