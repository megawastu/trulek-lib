<?php

class CatalogOptionTypeForm extends CFormModel
{
    public $id;
    public $type;

    public function rules()
    {
        return array(
           array('type', 'required'),
           array('type', 'validateUniqueType'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'type' => Yii::t('system', 'Type'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'type' => 'type',
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueType()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`type` = '".$this->type."' AND `id` <> '".$this->id."'";

       $total = CatalogOptionType::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('type', Yii::t('system', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Type')));
       }
   }
   
}
