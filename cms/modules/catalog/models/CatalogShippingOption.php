<?php

Yii::import('trulek.extensions.fileuploader.enums.UploadTypeEnum');

class CatalogShippingOption extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_shipping_option';
    }
    
    public function relations()
    {
        return array(
            'stores' => array(self::MANY_MANY, 'CatalogStoreSetting', 'catalog_store_shipping_options(shipping_option_id, store_id)'),
            'storeCount' => array(self::STAT, 'CatalogStoreSetting', 'catalog_store_shipping_options(shipping_option_id, store_id)'),
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function scopes()
    {
       return array(
           'published' => array("condition" => "published = '1'"),
           'unpublished' => array("condition" => "published = '0'"),
       );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}