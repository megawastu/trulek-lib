<?php

class CatalogAttributeGroupForm extends CFormModel
{
    public $id;
    public $name;
    public $ordering;
    
    public function rules()
    {
        return array(
           array('name, ordering', 'required'),
           array('name', 'validateUniqueName'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
           'ordering' => Yii::t('system', 'Sort Order'),
        );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'ordering' => 'ordering',
       );
   }   
   
   /**
    * @return void
    */
   public function validateUniqueName()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`name` = '".$this->name."' AND `id` <> '".$this->id."'";

       $total = CatalogAttributeGroup::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('name', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Name')));
       }
   }
}
