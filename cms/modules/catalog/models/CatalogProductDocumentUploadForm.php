<?php

class CatalogProductDocumentUploadForm extends CFormModel {

   public $document;

   /**
    *
    * @return <Array>
    */
   public function rules()
   {
       return array(
          array('document', 'file', 'types'=> 'doc, xls, pdf',
                                'maxSize'=>1024 * 1024 * 5, // 5MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .doc, .xls, .pdf'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 25MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
       );
   }

   /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(           
           'document' => Yii::t('catalogs', 'Document'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'document' => 'document',
       );
   }
   
}
