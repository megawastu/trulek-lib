<?php

class CatalogLengthClassForm extends CFormModel
{
    public $id;
    public $title;
    public $unit;
    public $value;
    
    public function rules()
    {
        return array(
           array('title, unit, value', 'required'),
           array('title', 'validateUniqueTitle'),
           array('unit', 'validateUniqueUnit'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'title' => Yii::t('system', 'Title'),
           'unit' => Yii::t('system', 'Unit'),
           'value' => Yii::t('system', 'Value'),
        );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'title' => 'title',
           'unit' => 'unit',
           'value' => 'value',
       );
   }   
   
   /**
    * @return void
    */
   public function validateUniqueTitle()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`title` = '".$this->title."' AND `id` <> '".$this->id."'";

       $total = CatalogLengthClass::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('title', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Title')));
       }
   } 
   
   /**
    * @return void
    */
   public function validateUniqueUnit()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`unit` = '".$this->unit."' AND `id` <> '".$this->id."'";

       $total = CatalogLengthClass::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('unit', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Unit')));
       }
   } 
}
