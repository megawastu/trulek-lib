<?php

class CatalogProductFile extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_product_file';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'products' =>array(self::MANY_MANY, 'CatalogProduct', 'catalog_product_files(product_id, file_id)'),
        );
    }
    
    public function scopes()
    {
        return array(
            'images' => array("join" => "INNER JOIN `core_uploaded_file` ON `core_uploaded_file`.`id` = `t`.`id`",
                              "condition" => " `t`.`product_id`='".$this->product_id."' AND `core_uploaded_file`.`file_type`='image/jpeg' OR `core_uploaded_file`.`file_type`='image/jpg' OR `core_uploaded_file`.`file_type`='image/png' OR `core_uploaded_file`.`file_type`='image/gif'",
                             ),
        );
    }

    public function behaviors()
    {
        return array(
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            //'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}