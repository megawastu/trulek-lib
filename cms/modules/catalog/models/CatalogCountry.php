<?php

class CatalogCountry extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_country';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            
        );
    }
    
    public function scopes()
    {
        return array(
            'enabled' => array("condition" => "`enabled` = '1'"),
            'disabled' => array("condition" => "`enabled` = '0'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}