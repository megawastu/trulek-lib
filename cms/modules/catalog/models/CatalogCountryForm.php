<?php

class CatalogCountryForm extends CFormModel
{
    public $id;
    public $name;
    public $iso_code_2;
    public $iso_code_3;
    public $postcode_required;
    public $enabled;
    
    public function rules()
    {
        return array(
           array('name, postcode_required, enabled', 'required'),
           array('iso_code_2', 'validateUniqueIsoCode2'),
           array('iso_code_3', 'validateUniqueIsoCode3'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
           'iso_code_2' => Yii::t('system', 'Iso Code 2'),
           'iso_code_3' => Yii::t('system', 'Iso Code 3'),
           'postcode_required' => Yii::t('system', 'Postcode Required'),
           'enabled' => Yii::t('system', 'Enabled'),
        );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'iso_code_2' => 'iso_code_2',
           'iso_code_3' => 'iso_code_3',
           'postcode_required' => 'postcode_required',
           'enabled' => 'enabled',
       );
   }   
   
   /**
    * @return void
    */
   public function validateUniqueIsoCode2()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`iso_code_2` = '".$this->iso_code_2."' AND `id` <> '".$this->id."'";

       $total = CatalogCountry::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('iso_code_2', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Iso Code 2')));
       }
   } 
   
   /**
    * @return void
    */
   public function validateUniqueIsoCode3()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`iso_code_3` = '".$this->iso_code_3."' AND `id` <> '".$this->id."'";

       $total = CatalogCountry::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('iso_code_3', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Iso Code 3')));
       }
   } 
}
