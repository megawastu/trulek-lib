<?php

Yii::import('trulek.extensions.fileuploader.enums.UploadTypeEnum');

class CatalogProduct extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_product';
    }
    
    public function relations()
    {
        return array(
            'categories' => array(self::MANY_MANY, 'CatalogCategory', 'catalog_product_categories(product_id, category_id)'),
            'categoriesCount' => array(self::STAT, 'CatalogCategory', 'catalog_product_categories(product_id, category_id)'),
            'relatedProducts' => array(self::MANY_MANY, 'CatalogProduct', 'catalog_related_products(id1, id2)', "condition" => "`relatedProducts`.`published` = '1'" ),
            'relatedProductCount' => array(self::STAT, 'CatalogProduct', 'catalog_related_products(id1, id2)', "condition" => "`relatedProducts`.`published` = '1'" ),
            'files' => array(self::HAS_MANY, 'CatalogProductFile', 'product_id'),
            'fileCount' => array(self::STAT, 'CatalogProductFile', 'product_id'),
            'images' => array(self::HAS_MANY, 'CatalogProductFile', 'product_id',  "join" => "INNER JOIN `core_uploaded_file` ON `core_uploaded_file`.`id` = `images`.`id`" 
//                                                                                   ,"condition" => "`core_uploaded_file`.`file_type`='image/jpeg' OR `core_uploaded_file`.`file_type`='image/png' OR `core_uploaded_file`.`file_type`='image/gif'"
                                                                                        
                             ),
//            'imageCount' => array(self::STAT, 'CatalogProductFile', 'product_id', array("join" => "INNER JOIN `core_uploaded_file` ON `core_uploaded_file`.`id` = `catalog_product_file`.`id`", "condition" => "`catalog_product_file`.`product_id` = `catalog_product`.`id` AND `core_uploaded_file`.`file_type`='image/jpeg' OR file_type='image/jpg' OR file_type='image/png' file_type='image/gif'")),
//            'videos' => array(self::HAS_MANY, 'CatalogProductFile', 'product_id', array("condition" => "file_type='".UploadTypeEnum::VIDEO."'")),
//            'videoCount' => array(self::STAT, 'CatalogProductFile', 'product_id', array("condition" => "file_type='".UploadTypeEnum::VIDEO."'")),
//            'docs' => array(self::HAS_MANY, 'CatalogProductFile', 'product_id', array("condition" => "file_type='".UploadTypeEnum::DOCUMENT."'")),
//            'docCount' => array(self::STAT, 'CatalogProductFile', 'product_id', array("condition" => "file_type='".UploadTypeEnum::DOCUMENT."'")),
            'comments' => array(self::HAS_MANY, 'CatalogComment', 'product_id'),
            'commentCount' => array(self::STAT, 'CatalogComment', 'product_id'),
            'views' => array(self::HAS_MANY, 'CatalogProductView', 'product_id'),
            'viewCount' => array(self::STAT, 'CatalogProductView', 'product_id'),
            
            'tierPrices' => array(self::HAS_MANY, 'CatalogProductTierPrice', 'product_id'),
            'tierPriceCount' => array(self::STAT, 'CatalogProductTierPrice', 'product_id'),
            
            'rentalPrices' => array(self::HAS_MANY, 'CatalogProductRentalPrice', 'product_id'),
            'rentalPriceCount' => array(self::STAT, 'CatalogProductRentalPrice', 'product_id'),
            
            'rentalSpecialPrices' => array(self::HAS_MANY, 'CatalogProductRentalSpecialPrice', 'product_id'),
            'rentalSpecialPriceCount' => array(self::STAT, 'CatalogProductRentalSpecialPrice', 'product_id'),
            
            'productAttributes' => array(self::HAS_MANY, 'CatalogProductAttribute', 'product_id'),   
            
            'options' => array(self::HAS_MANY, 'CatalogProductOption', 'product_id'),
            'optionCount' => array(self::STAT, 'CatalogProductOption', 'product_id'),
            
            'stores' => array(self::MANY_MANY, 'CatalogStoreSetting', 'catalog_store_products(store_id, product_id)'),
            'storeCount' => array(self::STAT, 'CatalogStoreSetting', 'catalog_store_products(store_id, product_id)'),
            
            'reviews' => array(self::HAS_MANY, 'CatalogProductReview', 'product_id'),
            'reviewCount' => array(self::STAT, 'CatalogProductReview', 'product_id'),
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function scopes()
    {
       return array(
           'published' => array("condition" => "published = '1'"),
           'unpublished' => array("condition" => "published = '0'"),
           'featured' => array("join" => "INNER JOIN `catalog_product_categories` ON `catalog_product_categories`.`product_id` = `t`.`id`",
                               "condition" => "`catalog_product_categories`.`category_id` = 'a5928cc8339c4fb9c5d72950940f301e'",
                               "order" => "`t`.`date_created` DESC"
                              ),
           'new' => array("condition" => "(`t`.`new_from` <> '' AND `t`.`new_from` <> '0000-00-00') AND (`t`.`new_until` <> '' AND `t`.`new_until` <> '0000-00-00') AND (new_from >= DATE(NOW()) OR new_until >= DATE(NOW()))",
                          "order" => "`t`.`date_created` DESC",
                         ),
       );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
            'CatalogPriceDisplayBehavior'=> 'trulek.cms.modules.catalog.behaviors.CatalogPriceDisplayBehavior',
        );
    }
}