<?php

class CatalogProductOption extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'catalog_product_option';
    }
    
    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
           'options'=>array(self::BELONGS_TO, 'CatalogOption', 'option_id'),
           'products'=>array(self::BELONGS_TO, 'CatalogProductOption', 'product_id'),
           'values' => array(self::HAS_MANY, 'CatalogProductOptionValue', 'product_option_id'),
        );
    }
    
    public function scopes()
    {
        return array(
            
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}