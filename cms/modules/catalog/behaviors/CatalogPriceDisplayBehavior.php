<?php

Yii::import('trulek.cms.modules.catalog.enums.PeriodUnitEnum');

class CatalogPriceDisplayBehavior extends CActiveRecordBehavior {

    private function _getActiveStore()
    {
        $modelInstance = CatalogStoreSetting::model()->default()->find();
        if (Yii::app()->user->hasState('activeStore'))
        {
            $modelInstance = Yii::app()->user->activeStore;
        }
        
        return $modelInstance;
    }
    
    public function displayPrice()
    {
        $store = $this->_getActiveStore();

        //set price
        $price = $this->owner->price;
        $displayPrice = $price;
        $special_price = $this->getSpecialPrice();
        
        $displayPrice = ($special_price > 0) ? '<span class="normal-price on-special">' : '<span class="normal-price">';
           $displayPrice .= '<span class="original-price">'.$this->formatPrice($price).'</span>';
           if ($special_price > 0) {
              $displayPrice .= '&nbsp;<span class="special-price">'.$this->formatPrice($special_price).'</span>';
           }
        $displayPrice .= '</span>';

        if ($store != null)
        {
           if ($store->only_show_price_to_logged_in_account <= 0)
           {
              if (Yii::app()->user->isGuest === true) {
                  return Yii::t('catalogs', 'Call us for pricing');
              }
              return $displayPrice;
           }
           else
           {
               return Yii::t('catalogs', 'Call us for pricing');
           }
        }
        else //if no config found, will display the price after all
        {
            return $displayPrice;
        }
    }

    public function getSpecialPrice()
    {
        $today = date('Y-m-d');
        //has special price ?
        if ($this->owner->special_price != "" && $this->owner->special_price != null && $this->owner->special_price != 0)
        {
            //yes ?
            //check for the validity
            if (($this->owner->special_price_from != null && $this->owner->special_price_until != null) &&
                 ($this->owner->special_price_from != "" && $this->owner->special_price_until != "") &&
                 ($this->owner->special_price_from != "0000-00-00" && $this->owner->special_price_from != "0000-00-00")
               )
            {
                //get special price that are already started but not yet ends
                $happening = $this->happening($this->owner->special_price_from, $this->owner->special_price_until);

                if ($happening === true)
                {
                    return $this->owner->special_price;
                }
                else
                {
                    return 0;
                }
            }
            else //if the from and until date not specified, means no special price
            {
                return 0;
            }
        }
        else
        {
            return 0;
        }
    }

    public function formatPrice($price)
    {
        $store = $this->_getActiveStore();
        $currency = CatalogCurrency::model()->findByPk($store->currency_id);
        $currency_symbol = $currency->symbol;
        $decimal_places = $store->decimal_places;
        $formatted_price = $currency_symbol.' '.number_format($price, $decimal_places);

        return $formatted_price;
    }
   
    public function displayRentalPrices()
    {
        $output = '';
        $rentalPrices = array();
        $rps = $this->owner->rentalPrices(array("order" => "`period_unit` ASC"));
        if ($rps != null)
        {
            $rentalPrices = $rps;
        }
        
        if (count($rentalPrices) > 0) {
            foreach ($rentalPrices as $rentalPrice)
            {
                $output .= '<div class="row">';
                    $output .= '<ul class="rental-pricing">';
                        $output .= '<li class="col1">'.$rentalPrice->period.'&nbsp;'.PeriodUnitEnum::getPeriodUnit($rentalPrice->period_unit)."</li>";
                        $output .= '<li class="col2">'.$this->formatPrice($rentalPrice->price)."</li>";
                    $output .= '</ul>';
                $output .= '</div>';
                $output .= '<br class="clr">';
            }
        }
        
        return $output;
    }

    /**
     * Function to check if an event has started and still on going
     * returns true when it is started and ongoing
     * returns false when it is not yet started
     * returns false when it is started and ended
     *
     * @param <String> $fromDate
     * @param <String> $untilDate
     * @return <Boolean>
     */
    public function happening($fromDate, $untilDate)
    {
        $today = date('Y-m-d');
        $sql = "SELECT TIMESTAMPDIFF(DAY, '".$today."', '".$fromDate."')";
        $started = Yii::app()->db->createCommand($sql)->queryScalar();

        $sql = "SELECT TIMESTAMPDIFF(DAY, '".$today."', '".$untilDate."')";
        $ended = Yii::app()->db->createCommand($sql)->queryScalar();

        if ($started > 0) { //not yet started
            return false;
        }

        if ($started <= 0 && $ended >= 0) { //it is started and still ongoing
            return true;
        }

        if ($started <= 0 && $ended < 0) // it has been started and it was ended
        {
            return false;
        }
    }

    public function daysLeft($date)
    {
        $today = date('Y-m-d');
        $sql = "SELECT TIMESTAMPDIFF(DAY, '".$today."', '".$date."')";
        $daysLeft = Yii::app()->db->createCommand($sql)->queryScalar();

        return $daysLeft;
    }
}

