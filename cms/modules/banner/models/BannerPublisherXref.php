<?php

class BannerPublisherXref extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'banner_publisher_banners';
    }
    
    public function rules()
    {
        return array(
            array('banner_id, contact_id', 'required')
        );
    }
}

