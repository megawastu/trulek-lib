<?php

class Banner extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'banner';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'category' =>array(self::BELONGS_TO, 'BannerCategory', 'category_id'),
            'publishers' => array(self::MANY_MANY, 'BannerContact', 'banner_publisher_banners(banner_id, contact_id)'),
            'publisherCount' => array(self::MANY_MANY, 'BannerContact', 'banner_publisher_banners(banner_id, contact_id)'),
            'clients' => array(self::MANY_MANY, 'BannerContact', 'banner_client_banners(banner_id, contact_id)'),
            'clientCount' => array(self::MANY_MANY, 'BannerContact', 'banner_client_banners(banner_id, contact_id)'),
        );
    }
    
    public function scopes()
    {
        return array(
            "published" => array("condition" => "`t`.`published`='1'"),
            "unpublished" => array("condition" => "`t`.`published`='0'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
            'SaveUploadedFileBehavior'=> 'trulek.extensions.fileuploader.behaviors.SaveUploadedFileBehavior',
        );
    }
}