<?php

class BannerCategoryForm extends CFormModel
{
    public $id;
    public $name;
    public $published;
    public $ordering;
    public $urlkey;

    public function rules()
    {
        return array(
           array('name, published, ordering, urlkey', 'required'),
           array('urlkey', 'validateUniqueUrlkey'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
           'published' => Yii::t('system', 'Published'),
           'ordering' => Yii::t('system', 'Sort Order'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'published' => 'published',
           'ordering' => 'ordering',
           'urlkey' => 'urlkey',
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueUrlkey()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`urlkey` = '".$this->urlkey."' AND `id` <> '".$this->id."'";

       $total = BannerCategory::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('urlkey', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Urlkey')));
       }
   }   
}
