<?php

class BannerContact extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'banner_contact';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
           'banners'=>array(self::MANY_MANY, 'Banner', 'banner_contact_banners(banner_id, contact_id)'),
           'bannerCount'=>array(self::STAT, 'Banner', 'banner_contact_banners(banner_id, contact_id)'),
        );
    }
    
    public function scopes()
    {
        return array(
            "publishers" => array("condition" => "`t`.`type`='0'"),
            "clients" => array("condition" => "`t`.`type`='1'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}