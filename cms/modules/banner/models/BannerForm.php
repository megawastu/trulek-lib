<?php

class BannerForm extends CFormModel
{
    public $id;
    public $category;
    public $name;
    public $urlkey;
    public $impression_total;
    public $impression_made;
    public $clicks;
    public $clickurl;
    public $published;
    public $custom_banner_code;
    public $description;
    public $ordering;
    
    public $file;
    public $delete_file;
    
    public $publishers;
    public $clients;

    public function rules()
    {
        return array(
           array('category, name, urlkey, clickurl, published, ordering, publishers, clients', 'required'),
           array('urlkey', 'validateUniqueUrlkey'),
           array('clickurl', 'url'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'category' => Yii::t('system', 'Category'),
           'name' => Yii::t('system', 'Name'),
           'urlkey' => Yii::t('system', 'Urlkey'),
           'impression_total' => Yii::t('system', 'Impression Total'),
           'impression_made' => Yii::t('system', 'Impression Made'),
           'clicks' => Yii::t('system', 'Clicks'),
           'clickurl' => Yii::t('system', 'Click Url'),
           'published' => Yii::t('system', 'Published'),
           'custom_banner_code' => Yii::t('system', 'Custom Banner Code'),
           'description' => Yii::t('system', 'Description'),
           'ordering' => Yii::t('system', 'Ordering'),
           'publishers' => Yii::t('system', 'Publishers'),
           'clients' => Yii::t('system', 'Clients'),
           'file' => Yii::t('system', 'File'),
           'delete_file' => Yii::t('system', 'Delete File'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'category' => 'category',
           'name' => 'name',
           'urlkey' => 'urlkey',
           'impression_total' => 'impression_total',
           'impression_made' => 'impression_made',
           'clicks' => 'clicks',
           'clickurl' => 'clickurl',
           'published' => 'published',
           'custom_banner_code' => 'custom_banner_code',
           'description' => 'description',
           'ordering' => 'ordering',
           'publishers' => 'publishers',
           'clients' => 'clients',
           'file' => 'file',
           'delete_file' => 'delete_file',
       );
   }
   
   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueUrlkey()
   {
       $criteria = new CDbCriteria();
       $criteria->condition = "`urlkey` = '".$this->urlkey."' AND `id` <> '".$this->id."'";

       $total = Banner::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('urlkey', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Urlkey')));
       }
   }   
}
