<?php

class BannerContactForm extends CFormModel
{
    public $id;
    public $name;
    public $contact;
    public $website;
    public $email;
    public $extra_info;
    public $type;

    public function rules()
    {
        return array(
           array('name, contact, email, type', 'required'),
           array('email', 'email'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
           'contact' => Yii::t('system', 'Contact'),
           'website' => Yii::t('system', 'Website'),
           'email' => Yii::t('system', 'Email'),
           'extra_info' => Yii::t('system', 'Extra Info'),
           'type' => Yii::t('system', 'Type'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'contact' => 'contact',
           'website' => 'website',
           'email' => 'email',
           'extra_info' => 'extra_info',
           'type' => 'type',
       );
   }
}
