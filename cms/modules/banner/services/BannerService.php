<?php

class BannerService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new Banner();
        $modelInstance->setAttributes($params, false);
        $modelInstance->category_id = $params['category'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {

            $modelInstance->save();
            
            if (isset($params['publishers']) && $params['publishers'] != null)
            {
                if (is_array($params['publishers'])) {
                    foreach ($params['publishers'] as $publisher_id) {
                        $xref = new BannerPublisherXref();
                        $xref->banner_id = $modelInstance->id;
                        $xref->contact_id = $publisher_id;
                        $xref->save();
                    }
                } else {
                    $xref = new BannerPublisherXref();
                    $xref->banner_id = $modelInstance->id;
                    $xref->contact_id = $params['publishers'];
                    $xref->save();
                }
            }
            
            if (isset($params['clients']) && $params['clients'] != null)
            {

                if (is_array($params['clients'])) {
                    foreach ($params['clients'] as $client_id) {
                        $xref = new BannerClientXref();
                        $xref->banner_id = $modelInstance->id;
                        $xref->contact_id = $client_id;
                        $xref->save();
                    }
                } else {
                    $xref = new BannerClientXref();
                    $xref->banner_id = $modelInstance->id;
                    $xref->contact_id = $params['clients'];
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = Banner::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        $modelInstance->category_id = $params['category'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            //drop all xref
            BannerPublisherXref::model()->deleteAllByAttributes(array('banner_id' => $modelInstance->id));
            BannerClientXref::model()->deleteAllByAttributes(array('banner_id' => $modelInstance->id));
            
            $modelInstance->save();
            
            if (isset($params['publishers']) && $params['publishers'] != null)
            {
                if (is_array($params['publishers'])) {
                    foreach ($params['publishers'] as $publisher_id) {
                        $xref = new BannerPublisherXref();
                        $xref->banner_id = $modelInstance->id;
                        $xref->contact_id = $publisher_id;
                        $xref->save();
                    }
                } else {
                    $xref = new BannerPublisherXref();
                    $xref->banner_id = $modelInstance->id;
                    $xref->contact_id = $params['publishers'];
                    $xref->save();
                }
            }
            
            if (isset($params['clients']) && $params['clients'] != null)
            {
                if (is_array($params['clients'])) {
                    foreach ($params['clients'] as $client_id) {
                        $xref = new BannerClientXref();
                        $xref->banner_id = $modelInstance->id;
                        $xref->contact_id = $client_id;
                        $xref->save();
                    }
                } else {
                    $xref = new BannerClientXref();
                    $xref->banner_id = $modelInstance->id;
                    $xref->contact_id = $params['clients'];
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop all xref
            BannerPublisherXref::model()->deleteAllByAttributes(array('banner_id' => $id));
            BannerClientXref::model()->deleteAllByAttributes(array('banner_id' => $id));
            
            $object = Banner::model()->findByPk($id);
            $object->delete();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    /**
     * @deprecated since version 1.2: replaced by api.extensions.core.actions.PublishAction
     * @param <String> $id
     */
    public function publish($id) {
        $categories = array();
        $modelInstance = Banner::model()->findByPk($id);
        $modelInstance->published = 1;

        $publishers = $modelInstance->contacts(array("condition" => "`type`='0'"));
        $clients = $modelInstance->contacts(array("condition" => "`type`='1'"));
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            BannerPublisherXref::model()->deleteAllByAttributes(array('banner_id' => $id));
            BannerClientXref::model()->deleteAllByAttributes(array('banner_id' => $id));
            
            $modelInstance->save();
            
            foreach ($publishers as $publisher) {
                $xref = new BannerPublisherXref();
                $xref->banner_id = $modelInstance->id;
                $xref->contact_id = $publisher->id;
                $xref->save();
            }
            
            foreach ($clients as $client) {
                $xref = new BannerClientXref();
                $xref->banner_id = $modelInstance->id;
                $xref->contact_id = $client->id;
                $xref->save();
            }
            
            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    /**
     * @deprecated since version 1.2: replaced by api.extensions.core.actions.UnpublishAction
     * @param <String> $id
     */
    public function unpublish($id) {
        $categories = array();
        $modelInstance = Banner::model()->findByPk($id);
        $modelInstance->published = 0;

        $publishers = $modelInstance->contacts(array("condition" => "`type`='0'"));
        $clients = $modelInstance->contacts(array("condition" => "`type`='1'"));

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            BannerPublisherXref::model()->deleteAllByAttributes(array('banner_id' => $id));
            BannerClientXref::model()->deleteAllByAttributes(array('banner_id' => $id));

            //save
            $modelInstance->save();

            foreach ($publishers as $publisher) {
                $xref = new BannerPublisherXref();
                $xref->banner_id = $modelInstance->id;
                $xref->contact_id = $publisher->id;
                $xref->save();
            }
            
            foreach ($clients as $client) {
                $xref = new BannerClientXref();
                $xref->banner_id = $modelInstance->id;
                $xref->contact_id = $client->id;
                $xref->save();
            }

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function getNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `banner`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
