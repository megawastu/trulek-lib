<?php
/**
 * @deprecated Since Version 1.0 because CRUD already could be handled by CRUDService
 */
class BannerCategoryService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new BannerCategory();
        $modelInstance->setAttributes($params, false);
        $modelInstance->ordering = $this->getNextOrdering();
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = BannerCategory::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            BannerCategory::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $articles = array();
        $modelInstance = BannerCategory::model()->findByPk($id);
        $modelInstance->published = 1;
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //save
            $modelInstance->save();

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $articles = array();
        $modelInstance = BannerCategory::model()->findByPk($id);
        $modelInstance->published = 0;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //save
            $modelInstance->save();

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function getNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `banner_category`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
