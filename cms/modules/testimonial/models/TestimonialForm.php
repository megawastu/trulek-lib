<?php

class TestimonialForm extends CFormModel
{
    public $id;
    public $category_id;
    public $title;
    public $main;
    public $full_name;
    public $email;
    public $website;
    public $ordering;
    public $published;
    public $approved;

    public function rules()
    {
        return array(
           array('category_id, title, main, full_name, email, ordering, published, approved', 'required'),
           array('website', 'url'),
           array('email', 'email'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'category_id' => Yii::t('system', 'Category'),
           'title' => Yii::t('system', 'Title'),
           'main' => Yii::t('system', 'Main'),
           'full_name' => Yii::t('system', 'Full Name'),
           'email' => Yii::t('system', 'Email'),
           'website' => Yii::t('system', 'Website'),
           'ordering' => Yii::t('system', 'Ordering'),
           'published' => Yii::t('system', 'Published'),
           'approved' => Yii::t('system', 'Approved'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'category_id' => 'category_id',
           'title' => 'title',
           'main' => 'main',
           'full_name' => 'full_name',
           'email' => 'email',
           'website' => 'website',
           'ordering' => 'ordering',
           'published' => 'published',
           'approved' => 'approved',
       );
   }
}
