<?php

class RequestQuoteForm extends CFormModel
{
    public $id;
    public $name;
    public $email;
	public $phone;
	public $mobile;
	public $fax;
	public $street;
	public $city;
	public $state;
	public $postalcode;
	public $country;
	public $message;
	public $notes;
	public $account_id;
	public $folder;
	public $priority; //1,2,3,4
	public $status; //1=New, 2=Closed

    public function rules()
    {
        return array(
           array('name, email, mobile, street, city, country, message, priority, status', 'required'),
           array('email', 'email'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
           'email' => Yii::t('system', 'Email'),
		   'phone' => Yii::t('system', 'Phone'),
           'mobile' => Yii::t('system', 'Mobile'),
           'fax' => Yii::t('system', 'Fax'),
		   'street' => Yii::t('system', 'Street'),
           'city' => Yii::t('system', 'City'),
           'state' => Yii::t('system', 'State'),
		   'postalcode' => Yii::t('system', 'Postal code'),
           'country' => Yii::t('system', 'Country'),
           'message' => Yii::t('system', 'Message'),
		   'notes' => Yii::t('system', 'Any additional notes?'),
		   'account_id' => Yii::t('system', 'Related to Account'),
		   'folder' => Yii::t('system', 'Please upload your photos'),
		   'priority' => Yii::t('system', 'Urgency'),
		   'status' => Yii::t('system', 'Status'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'email' => 'email',
		   'phone' => 'phone',
           'mobile' => 'mobile',
           'fax' => 'fax',
		   'street' => 'street',
           'city' => 'city',
           'state' => 'state',
		   'postalcode' => 'postalcode',
           'country' => 'country',
           'message' => 'message',
		   'notes' => 'notes',
		   'account_id' => 'account_id',
		   'folder' => 'folder',
		   'priority' => 'priority',
		   'status' => 'status',
       );
   }
}
