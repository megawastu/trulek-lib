<?php

class VoucherThemeForm extends CFormModel
{
    public $id;
    public $name;
    
    public $file;
    public $delete_file;
    
    public function rules()
    {
        return array(
           array('name', 'required'),
           array('name', 'validateUniqueName'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
           'file' => Yii::t('system', 'File Name'),
           'delete_file' => Yii::t('system', 'Delete File'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'file' => 'file',
           'delete_file' => 'delete_file',
       );
   }
   
   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueName()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`name` = '".$this->name."' AND `id` <> '".$this->id."'";

       $total = VoucherTheme::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('name', Yii::t('catalogs', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName' => 'Name')));
       }
   }
}
