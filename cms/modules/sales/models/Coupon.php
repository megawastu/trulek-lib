<?php

class Coupon extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'sales_coupon';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
           'products'=>array(self::MANY_MANY, 'CatalogProduct', 'sales_coupon_products(coupon_id, product_id)'),
           'productCount'=>array(self::STAT, 'CatalogProduct', 'sales_coupon_products(coupon_id, product_id)'),
        );
    }
    
    public function scopes()
    {
        return array(
            'percentage' => array("condition" => "`t`.`type` = '1'"),
            'fixed_amount' => array("condition" => "`t`.`type` = '2'"),
            'valid' => array("condition" => "`t`.`invalid` = '0'"),
            'invalid' => array("condition" => "`t`.`invalid` = '1'"),
            'enabled' => array("condition" => "`t`.`enabled` = '1'"),
            'disabled' => array("condition" => "`t`.`enabled` = '0'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}