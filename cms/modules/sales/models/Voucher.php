<?php

class Voucher extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'sales_voucher';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
           'theme'=>array(self::BELONGS_TO, 'VoucherTheme', 'voucher_theme_id'),
        );
    }
    
    public function scopes()
    {
        return array(
            'active' => array('condition' => "`t`.`inactive` = '0'"),
            'inactive' => array('condition' => "`t`.`inactive` = '1'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}