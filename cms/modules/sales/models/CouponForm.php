<?php

class CouponForm extends CFormModel
{
    public $id;
    public $name;
    public $code;
    public $type;
    public $discount;
    public $min_total_amount;
    public $free_shipping;
    public $date_start;
    public $date_end;
    public $uses_total;
    public $uses_customer;
    public $invalid;
    public $invalid_note;
    public $enabled;   
    
    public $products = array();

    public function rules()
    {
        return array(
           array('name, code, type, discount, min_total_amount, free_shipping, uses_total, uses_customer, invalid, enabled', 'required'),
           array('code', 'validateUniqueCode'),
           array(
              'date_end',
              'compare',
              'compareAttribute'=>'date_start',
              'operator'=>'>',
              'allowEmpty'=>true ,
              'message'=>'{attribute} must be greater than "{compareValue}".'
            ),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
           'code' => Yii::t('system', 'Code'),
           'type' => Yii::t('system', 'Type'),
           'discount' => Yii::t('system', 'Discount'),
           'min_total_amount' => Yii::t('system', 'Min. Total Purchase'),
           'free_shipping' => Yii::t('system', 'Free Shipping'),
           'date_start' => Yii::t('system', 'Start Date'),
           'date_end' => Yii::t('system', 'End Date'),
           'uses_total' => Yii::t('system', 'Uses Total'),
           'uses_customer' => Yii::t('system', 'Uses Customer'),
           'invalid' => Yii::t('system', 'Invalid'),
           'invalid_note' => Yii::t('system', 'Invalid Note'),
           'enabled' => Yii::t('system', 'Enabled'),
           'products' => Yii::t('system', 'Products'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'code' => 'code',
           'type' => 'type',
           'discount' => 'discount',
           'min_total_amount' => 'min_total_amount',
           'free_shipping' => 'free_shipping',
           'date_start' => 'date_start',
           'date_end' => 'date_end',
           'uses_total' => 'uses_total',
           'uses_customer' => 'uses_customer',
           'invalid' => 'invalid',
           'invalid_note' => 'invalid_note',
           'enabled' => 'enabled',
           'products' => 'products',
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueCode()
   {
       $criteria = new CDbCriteria();
       $criteria->condition = "`code` = '".$this->code."' AND `id` <> '".$this->id."'";

       $total = Coupon::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('code', Yii::t('system', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName' => 'Code')));
       }
   }
}
