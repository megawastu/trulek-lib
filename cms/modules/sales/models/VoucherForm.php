<?php

class VoucherForm extends CFormModel
{
    public $id;
    public $theme;
    public $code;
    public $from_name;
    public $from_email;
    public $to_name;
    public $to_email;
    public $amount;
    public $message;
    public $inactive;
    
    public function rules()
    {
        return array(
           array('theme, code, from_name, from_email, to_name, to_email, amount, message, inactive', 'required'),
           array('code', 'validateUniqueCode'),
           array('from_email, to_email', 'email'),
           
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'theme' => Yii::t('system', 'Voucher Theme'),
           'code' => Yii::t('system', 'Code'),
           'from_name' => Yii::t('system', 'From Name'),
           'from_email' => Yii::t('system', 'From Email'),
           'to_name' => Yii::t('system', 'To Name'),
           'to_email' => Yii::t('system', 'To Email'),
           'amount' => Yii::t('system', 'Amount'),
           'message' => Yii::t('system', 'Message'),
           'inactive' => Yii::t('system', 'Inactive'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'theme' => 'theme',
           'code' => 'code',
           'from_name' => 'from_name',
           'from_email' => 'from_email',
           'to_name' => 'to_name',
           'to_email' => 'to_email',
           'amount' => 'amount',
           'message' => 'message',
           'inactive' => 'inactive',
       );
   }
   
   /**
    * @return void
    */
   public function validateUniqueCode()
    {
       $criteria = new CDbCriteria();
       $criteria->condition = "`code` = '".$this->code."' AND `id` <> '".$this->id."'";

       $total = Voucher::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('code', Yii::t('system', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName}' => 'Code')));
       }
   }
}
