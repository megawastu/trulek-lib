<?php

Yii::import('trulek.cms.modules.sales.models.*');

class CouponService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new Coupon();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();    
            
            if (isset($params['products']) && count($params['products']) > 0)
            {
                foreach ($params['products'] as $i => $product_id) {
                    $xref = new CouponProductXref();
                    $xref->product_id = $product_id;
                    $xref->coupon_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = Coupon::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {            
            //drop all xref
            CouponProductXref::model()->deleteAllByAttributes(array('coupon_id' => $modelInstance->id));
            
            $modelInstance->save();   
            if (isset($params['products']) && count($params['products']) > 0)
            {
                foreach ($params['products'] as $i => $product_id) {
                    $xref = new CouponProductXref();
                    $xref->product_id = $product_id;
                    $xref->coupon_id = $modelInstance->id;
                    $xref->save();
                }
            }
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            $object = Coupon::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function enable($id) {
        $objects = array();
        $modelInstance = Coupon::model()->findByPk($id);
        $modelInstance->published = 1;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function disable($id) {
        $objects = array();
        $modelInstance = Coupon::model()->findByPk($id);
        $modelInstance->published = 0;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();
//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
}
