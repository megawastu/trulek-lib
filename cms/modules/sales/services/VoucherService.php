<?php

Yii::import('trulek.cms.modules.sales.models.*');

class VoucherService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new Voucher();
        $modelInstance->setAttributes($params, false);
        $modelInstance->voucher_theme_id = $params['theme'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = Voucher::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        $modelInstance->voucher_theme_id = $params['theme'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {            
            $modelInstance->save();            
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            $object = Voucher::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function activate($id) {
        $objects = array();
        $modelInstance = Voucher::model()->findByPk($id);
        $modelInstance->published = 1;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function deactivate($id) {
        $objects = array();
        $modelInstance = Voucher::model()->findByPk($id);
        $modelInstance->published = 0;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();
//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
}
