<?php

Yii::import('trulek.cms.modules.sales.models.*');

class VoucherThemeService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new VoucherTheme();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = VoucherTheme::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {            
            $modelInstance->save();            
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            $object = VoucherTheme::model()->findByPk($id);
            $old_file = $object->file_name;
            $object->delete();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function activate($id) {
        $objects = array();
        $modelInstance = VoucherTheme::model()->findByPk($id);
        $modelInstance->published = 1;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function inactivate($id) {
        $objects = array();
        $modelInstance = VoucherTheme::model()->findByPk($id);
        $modelInstance->published = 0;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();
//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
}
