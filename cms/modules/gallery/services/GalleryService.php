<?php

Yii::import('trulek.cms.modules.gallery.models.Album');
Yii::import('trulek.cms.modules.gallery.models.Photo');

class GalleryService extends CApplicationComponent {

    public function createAlbum($params=array())
    {
        $modelInstance = new Album();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function updateAlbum($params=array())
    {
        $modelInstance = Album::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function deleteAlbum($id) {
        try {
            //drop all photos
            Photo::model()->deleteAllByAttributes(array('album_id' => $id));

            Album::model()->deleteByPk($id);
            
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    /**
     * @deprecated since version 1.2: replaced by api.extensions.core.actions.PublishAction
     * @param <String> $id
     */
    public function publishAlbum($id) {
        $objects = array();
        $modelInstance = Album::model()->findByPk($id);
        $modelInstance->published = 1;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    /**
     * @deprecated since version 1.2: replaced by api.extensions.core.actions.UnpublishAction
     * @param <String> $id
     */
    public function unpublishAlbum($id) {
        $objects = array();
        $modelInstance = Album::model()->findByPk($id);
        $modelInstance->published = 0;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();

//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
    
    public function getAlbumNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `album`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
    
    public function setAlbum(Album $album) {
        Yii::app()->user->setState('album', $album);
    }

    public function getAlbum()
    {
        return Yii::app()->user->album;
    }
    
    public function createPhoto($params=array())
    {
        $modelInstance = new Photo();
        $modelInstance->setAttributes($params, false);
        $modelInstance->album_id = $params['album'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function updatePhoto($params=array())
    {
        $modelInstance = Photo::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        $modelInstance->album_id = $params['album'];
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }
    
    public function deletePhoto($id) {
        try {
            $object = Photo::model()->findByPk($id);
            $old_file = $object->file;
            $object->delete();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    /**
     * @deprecated since version 1.2: replaced by api.extensions.core.actions.PublishAction
     * @param <String> $id
     */
    public function publishPhoto($id) {
        $objects = array();
        $modelInstance = Photo::model()->findByPk($id);
        $modelInstance->published = 1;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    /**
     * @deprecated since version 1.2: replaced by api.extensions.core.actions.UnpublishAction
     * @param <String> $id
     */
    public function unpublishPhoto($id) {
        $objects = array();
        $modelInstance = Photo::model()->findByPk($id);
        $modelInstance->published = 0;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();

//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
    
    public function getPhotoNextOrdering(Album $album)
    {
        $sql = "SELECT MAX(`ordering`) FROM `photo` WHERE `photo`.`album_id` = '".$album->id."'";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
