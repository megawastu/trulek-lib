<?php

class AlbumForm extends CFormModel
{
    public $id;
    public $description;
    public $from_date;
    public $ordering;
    public $published;
    public $title;
    public $to_date;
    public $urlkey;

    public function rules()
    {
        return array(
           array('title, urlkey, published, ordering', 'required'),
           array(
              'to_date',
              'compare',
              'compareAttribute'=>'from_date',
              'operator'=>'>',
              'allowEmpty'=>true ,
              'message'=>'{attribute} must be greater than "{compareValue}".'
            ),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'description' => Yii::t('system', 'Description'),
           'from_date' => Yii::t('system', 'From Date'),
           'ordering' => Yii::t('system', 'Sort Order'),
           'published' => Yii::t('system', 'Published'),
           'title' => Yii::t('system', 'Title'),
           'to_date' => Yii::t('system', 'Until Date'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),           
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'description' => 'description',
           'from_date' => 'from_date',
           'ordering' => 'ordering',
           'published' => 'published',
           'title' => 'title',
           'to_date' => 'to_date',
           'urlkey' => 'urlkey',
       );
   }
}
