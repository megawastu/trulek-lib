<?php

class PhotoForm extends CFormModel
{
    public $id;
    public $album;
    public $caption;
    public $captured_date;
    public $description;
    public $file;
    public $main_photo;
    public $ordering;
    public $published;
    public $urlkey;
    
    public $delete_file;

    public function rules()
    {
        return array(
           array('caption, published, main_photo, ordering, urlkey', 'required'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'album' => Yii::t('system', 'Album'),
           'caption' => Yii::t('system', 'Caption'),
           'captured_date' => Yii::t('system', 'Captured Date'),
           'description' => Yii::t('system', 'Description'),
           'file' => Yii::t('system', 'File'),
           'main_photo' => Yii::t('system', 'Main'),
           'ordering' => Yii::t('system', 'Sort Order'),
           'published' => Yii::t('system', 'Published'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
           'delete_file' => Yii::t('system', 'Delete'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'album' => 'album',
           'caption' => 'caption',
           'captured_date' => 'captured_date',
           'description' => 'description',
           'file' => 'file',
           'main_photo' => 'main_photo',
           'ordering' => 'ordering',
           'published' => 'published',
           'urlkey' => 'urlkey',
           'delete_file' => 'delete_file',
       );
   }
   
   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
}
