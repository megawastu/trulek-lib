<?php

class Album extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'album';
    }

    /**
     * Method to defined rules for validation
     * @return Array
     */
    public function rules()
    {
        return array(
            array('title, urlkey, published, ordering', 'required'),
            array('urlkey', 'unique'),
            array(
              'to_date',
              'compare',
              'compareAttribute'=>'from_date',
              'operator'=>'>',
              'allowEmpty'=>true ,
              'message'=>'{attribute} must be greater than "{compareValue}".'
            ),
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'photos' =>array(self::HAS_MANY, 'Photo', 'album_id'),
            'photoCount' =>array(self::STAT, 'Photo', 'album_id'),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
        );
    }
}