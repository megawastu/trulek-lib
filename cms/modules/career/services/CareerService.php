<?php

class CareerService extends CApplicationComponent {

    public function createVacancy($params=array())
    {
        $modelInstance = new Vacancy();
        $modelInstance->setAttributes($params, false);
        
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function updateVacancy($params=array())
    {
        $modelInstance = Vacancy::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function deleteVacancy($id) {
        try {
            Vacancy::model()->deleteByPk($id);
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publishVacancy($id) {
        $modelInstance = Vacancy::model()->findByPk($id);
        $modelInstance->published = 0x01;
        try {            
            //save
            $modelInstance->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function unpublishVacancy($id) {
        $modelInstance = Vacancy::model()->findByPk($id);
        $modelInstance->published = 0x00;
        try {            
            //save
            $modelInstance->save();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
    
    public function createApplicant($params=array())
    {
        $modelInstance = new Applicant();
        $modelInstance->setAttributes($params, false);
        
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function updateApplicant($params=array())
    {
        $modelInstance = Applicant::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function deleteApplicant($id) {
        try {
            $object = Applicant::model()->findByPk($id);
            $object->delete();
            
            $old_file = $object->resume_file;
            if ($old_file != null) {
               $uploadPath = $_SERVER['DOCUMENT_ROOT'].Yii::app()->assetManager->baseUrl.'/'.Yii::app()->params['baseMediaFolder'].Yii::app()->controller->module->name.'/'.Yii::app()->controller->id;
               Yii::app()->fileUploadService->deleteFile($old_file, $uploadPath);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
    
    public function getVacancyNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `career_vacancy`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
