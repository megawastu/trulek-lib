<?php

class VacancyForm extends CFormModel
{
    public $id;
    public $title;
    public $job_code;
    public $urlkey;
    public $meta_desc;
    public $meta_keyword;  
    public $url;
    public $ordering;
    public $published;
    public $valid_until;
    public $responsibilities;
    public $requirements;
    public $description;
    public $job_type;    

    public function rules()
    {
        return array(
           array('title, job_code, urlkey, ordering, published, job_type', 'required'),
           array('url', 'url'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'title' => Yii::t('system', 'Title'),
           'job_code' => Yii::t('system', 'Job Code'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
           'meta_desc' => Yii::t('system', 'Meta Description'),
           'meta_keyword' => Yii::t('system', 'Meta Keyword'),
           'url' => Yii::t('system', 'Url'),
           'ordering' => Yii::t('system', 'Sort Order'),
           'published' => Yii::t('system', 'Published'),
           'valid_until' => Yii::t('system', 'Valid Until'),
           'responsibilities' => Yii::t('system', 'Responsibilities'),
           'requirements' => Yii::t('system', 'Requirements'),
           'description' => Yii::t('system', 'Description'),
           'job_type' => Yii::t('system', 'Job Type'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'title' => 'title',
           'job_code' => 'job_code',
           'urlkey' => 'urlkey',
           'meta_desc' => 'meta_desc',
           'meta_keyword' => 'meta_keyword',
           'url' => 'url',
           'ordering' => 'ordering',
           'published' => 'published',
           'valid_until' => 'valid_until',
           'responsibilities' => 'responsibilities',
           'requirements' => 'requirements',
           'description' => 'description',
           'job_type' => 'job_type',
       );
   }
}
