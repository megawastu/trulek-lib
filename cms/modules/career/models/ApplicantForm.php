<?php

class ApplicantForm extends CFormModel
{
    public $id;
    public $id_number;
    public $title;
    public $first_name;
    public $last_name;
    public $email;  
    public $mailing_address;
    public $residential_address;
    public $phone;
    public $mobile;
    public $desired_position;
    public $date_of_birth;
    public $nationality;
    public $religion;
    public $gender;
    public $marital_status;
    public $blood_type;
    public $resume_file;
    public $resume_url;    
    
    public $delete_file;

    public function rules()
    {
        return array(
           array('title, id_number, title, first_name, last_name, email, date_of_birth, religion, gender, nationality, marital_status', 'required'),
           array('email', 'email'),
           array('resume_file', 'file', 'types'=> 'doc, pdf',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .doc, .pdf'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'id_number' => Yii::t('system', 'Id Number'),
           'title' => Yii::t('system', 'Title'),
           'first_name' => Yii::t('system', 'First Name'),
           'last_name' => Yii::t('system', 'Last Name'),
           'email' => Yii::t('system', 'Email'),
           'mailing_address' => Yii::t('system', 'Mailing Address'),
           'residential_address' => Yii::t('system', 'Residential Address'),
           'phone' => Yii::t('system', 'Phone'),
           'mobile' => Yii::t('system', 'Mobile'),
           'desired_position' => Yii::t('system', 'Desired Position'),
           'date_of_birth' => Yii::t('system', 'Date of Birth'),
           'nationality' => Yii::t('system', 'Nationality'),
           'religion' => Yii::t('system', 'Religion'),
           'gender' => Yii::t('system', 'Gender'),
           'marital_status' => Yii::t('system', 'Marital Status'),
           'blood_type' => Yii::t('system', 'Blood Type'),
           'resume_file' => Yii::t('system', 'Resume File'),
           'resume_url' => Yii::t('system', 'Resume Url'),
           'delete_file' => Yii::t('system', 'Delete'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'id_number' => 'id_number',
           'title' => 'title',
           'first_name' => 'first_name',
           'last_name' => 'last_name',
           'email' => 'email',
           'mailing_address' => 'mailing_address',
           'residential_address' => 'residential_address',
           'phone' => 'phone',
           'mobile' => 'mobile',
           'desired_position' => 'desired_position',
           'date_of_birth' => 'date_of_birth',
           'nationality' => 'nationality',
           'religion' => 'religion',
           'gender' => 'gender',
           'marital_status' => 'marital_status',
           'blood_type' => 'blood_type',
           'resume_file' => 'resume_file',
           'resume_url' => 'resume_url',
           'delete_file' => 'delete_file',
       );
   }
}
