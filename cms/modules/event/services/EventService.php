<?php

Yii::import('trulek.cms.modules.event.models.EventCategory');
Yii::import('trulek.cms.modules.event.models.EventCategoryXref');
Yii::import('trulek.cms.modules.event.models.Event');

class EventService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new Event();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();

            if (isset($params['categories']) && count($params['categories']) > 0) {
                foreach ($params['categories'] as $category_id) {
                    $xref = new EventCategoryXref();
                    $xref->event_category_id = $category_id;
                    $xref->event_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = Event::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            if (isset($params['categories']) && count($params['categories']) > 0) {
               EventCategoryXref::model()->deleteAllByAttributes(array('event_id' => $modelInstance->id));
            }
            
            $modelInstance->save();

            if (isset($params['categories']) && count($params['categories']) > 0) {
                foreach ($params['categories'] as $category_id) {
                    $xref = new EventCategoryXref();
                    $xref->event_category_id = $category_id;
                    $xref->event_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop all xref
            EventCategoryXref::model()->deleteAllByAttributes(array('event_id' => $id));

            $object = Event::model()->findByPk($id);
            $old_file = $object->file;
            $object->delete();

            if ($old_file != null) {
               $uploadPath = $_SERVER['DOCUMENT_ROOT'].Yii::app()->assetManager->baseUrl.'/'.Yii::app()->params['baseMediaFolder'].Yii::app()->controller->module->name.'/'.Yii::app()->controller->id;
               Yii::app()->fileUploadService->deleteFile($old_file, $uploadPath);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $objects = array();
        $modelInstance = Event::model()->findByPk($id);
        $modelInstance->published = 0x01;

        $categories = $modelInstance->categories;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            if ($categories != null && count($categories) > 0) {
                EventCategoryXref::model()->deleteAllByAttributes(array('event_id' => $id));
            }
            
            $modelInstance->save();
            
            if ($categories != null && count($categories) > 0) {
                foreach ($categories as $category) {
                    $xref = new EventCategoryXref();
                    $xref->event_category_id = $category->id;
                    $xref->event_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $objects = array();
        $modelInstance = Event::model()->findByPk($id);
        $modelInstance->published = 0x00;

        $categories = $modelInstance->categories;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            if ($categories != null && count($categories) > 0) {
                EventCategoryXref::model()->deleteAllByAttributes(array('event_id' => $id));
            }
            
            //save
            $modelInstance->save();

            //add xref
            if ($categories != null && count($categories) > 0) {
                foreach ($categories as $category) {
                    $xref = new EventCategoryXref();
                    $xref->event_category_id = $category->id;
                    $xref->event_id = $modelInstance->id;
                    $xref->save();
                }
            }

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }
}
