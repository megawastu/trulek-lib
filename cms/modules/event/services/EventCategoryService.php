<?php

Yii::import('trulek.cms.modules.event.models.EventCategory');
Yii::import('trulek.cms.modules.event.models.EventCategoryXref');
Yii::import('trulek.cms.modules.event.models.Event');

class EventCategoryService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new EventCategory();
        $modelInstance->setAttributes($params, false);
        
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = EventCategory::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            EventCategoryXref::model()->deleteAllByAttributes(array('event_category_id' => $id));

            $object = EventCategory::model()->findByPk($id);
            $old_file = $object->file;
            $object->delete();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $objects = array();
        $modelInstance = EventCategory::model()->findByPk($id);
        $modelInstance->published = 0x01;

        $events = $modelInstance->events;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            EventCategoryXref::model()->deleteAllByAttributes(array('event_category_id' => $id));

            //save
            $modelInstance->save();

            //add xref
            foreach ($events as $event) {
                $xref = new EventCategoryXref();
                $xref->event_id = $event->id;
                $xref->event_category_id = $modelInstance->id;
                $xref->save();
            }

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $objects = array();
        $modelInstance = EventCategory::model()->findByPk($id);
        $modelInstance->published = 0x00;

        $events = $modelInstance->events;

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            //drop all xref
            EventCategoryXref::model()->deleteAllByAttributes(array('event_category_id' => $id));

            //save
            $modelInstance->save();

            //add xref
            foreach ($events as $event) {
                $xref = new EventCategoryXref();
                $xref->event_id = $event->id;
                $xref->event_category_id = $modelInstance->id;
                $xref->save();
            }

            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
            $transaction->rollback();
            exit();
        }
    }
}
