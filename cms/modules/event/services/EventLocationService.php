<?php

Yii::import('trulek.cms.modules.event.models.Location');

class EventLocationService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new Location();
        $modelInstance->setAttributes($params, false);
        
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = Location::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);
        try {
            $modelInstance->save();
            return $modelInstance;
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            Event::model()->deleteAllByAttributes(array('location_id' => $id));
            $object = Location::model()->findByPk($id);
            $old_file = $object->file;
            $object->delete();

            if ($old_file != null) {
               $uploadPath = $_SERVER['DOCUMENT_ROOT'].Yii::app()->assetManager->baseUrl.'/'.Yii::app()->params['baseMediaFolder'].Yii::app()->controller->module->name.'/'.Yii::app()->controller->id;
               Yii::app()->fileUploadService->deleteFile($old_file, $uploadPath);
            }
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }
}
