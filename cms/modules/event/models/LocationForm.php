<?php

class LocationForm extends CFormModel
{
    public $id;
    public $city;
    public $country;
    public $description;
    public $file;
    public $name;
    public $postal_code;
    public $region;
    public $state;
    public $street;
    public $urlkey;
    public $google_map;

    public $delete_file;
    
    public function rules()
    {
        return array(
           array('name, urlkey, street', 'required'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'city' => Yii::t('system', 'City'),
           'country' => Yii::t('system', 'Country'),
           'description' => Yii::t('system', 'Description'),
           'file' => Yii::t('system', 'File'),
           'name' => Yii::t('system', 'Name'),
           'postal_code' => Yii::t('system', 'Postal Code'),
           'region' => Yii::t('system', 'Region'),
           'state' => Yii::t('system', 'State'),
           'street' => Yii::t('system', 'Street'),
           'urlkey' => Yii::t('system', 'Urlkey'),
           'google_map' => Yii::t('system', 'Google Map'),
           'delete_file' => Yii::t('system', 'Delete File'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'city' => 'city',
           'country' => 'country',
           'description' => 'description',
           'file' => 'file',
           'name' => 'name',
           'postal_code' => 'postal_code',
           'region' => 'region',
           'state' => 'state',
           'street' => 'street',
           'urlkey' => 'urlkey',
           'google_map' => 'google_map',
           'delete_file' => 'delete_file',
       );
   }

   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
}
