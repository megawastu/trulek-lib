<?php

class Event extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'event';
    }

    /**
     * Method to defined rules for validation
     * @return Array
     */
    public function rules()
    {
        return array(
            array('name, urlkey, published, location_id, enable_registration, description', 'required'),
            array('urlkey', 'unique'),
            array(
              'end_date',
              'compare',
              'compareAttribute'=>'start_date',
              'operator'=>'>',
              'allowEmpty'=>true ,
              'message'=>'{attribute} must be greater than "{compareValue}".'
            ),
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'categories' =>array(self::MANY_MANY, 'EventCategory', 'event_category_events(event_category_id, event_id)'),
            'categoryCount' =>array(self::STAT, 'EventCategory', 'event_category_events(event_category_id, event_id)'),
            'location' =>array(self::BELONGS_TO, 'Location', 'location_id'),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
            'SaveUploadedFileBehavior'=> 'trulek.extensions.fileuploader.behaviors.SaveUploadedFileBehavior',
        );
    }
}