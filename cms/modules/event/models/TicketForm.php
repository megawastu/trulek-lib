<?php

class TicketForm extends CFormModel
{
    public $id;
    public $available_spaces;
    public $description;
    public $end_date_availability;
    public $event_id;
    public $max_required_tickets_per_booking;
    public $min_required_tickets_per_booking;
    public $name;
    public $price;
    public $start_date_availability;
    
    public function rules()
    {
        return array(
           array('available_spaces, event_id, max_required_tickets_per_booking, min_required_tickets_per_booking, name, price', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'available_spaces' => Yii::t('system', 'Space Available'),           
           'description' => Yii::t('system', 'Description'),
           'end_date_availability' => Yii::t('system', 'End Date of Ticket Availability'),
           'event_id' => Yii::t('system', 'Event'),
           'max_required_tickets_per_booking' => Yii::t('system', 'Maximum Tickets Required per Booking'),
           'min_required_tickets_per_booking' => Yii::t('system', 'Minimum Tickets Required per Booking'),
           'name' => Yii::t('system', 'Name'),
           'price' => Yii::t('system', 'Price'),
           'start_date_availability' => Yii::t('system', 'Start Date of Ticket Availability'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'available_spaces' => 'available_spaces',
           'description' => 'description',
           'end_date_availability' => 'end_date_availability',
           'event_id' => 'event_id',
           'max_required_tickets_per_booking' => 'max_required_tickets_per_booking',
           'min_required_tickets_per_booking' => 'min_required_tickets_per_booking',
           'name' => 'name',
           'price' => 'price',
           'start_date_availability' => 'start_date_availability',
       );
   }
}
