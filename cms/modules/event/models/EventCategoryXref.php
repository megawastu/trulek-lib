<?php

class EventCategoryXref extends CActiveRecord
{
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'event_category_events';
    }
    
    public function rules()
    {
        return array(
            array('event_category_id, event_id', 'required')
        );
    }
}

