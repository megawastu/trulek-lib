<?php

class Ticket extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'event_ticket';
    }

    /**
     * Method to defined rules for validation
     * @return Array
     */
    public function rules()
    {
        return array(
            array('available_spaces, event_id, max_required_tickets_per_booking, min_required_tickets_per_booking, name, price', 'required'),
        );
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'event' =>array(self::BELONGS_TO, 'Event', 'event_id'),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
        );
    }
}