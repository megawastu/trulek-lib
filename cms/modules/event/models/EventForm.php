<?php

class EventForm extends CFormModel
{
    public $id;
    public $description;
    public $enable_registration;
    public $end_date;
    public $file;
    public $google_map;
    public $location_id;
    public $name;
    public $published;
    public $start_date;
    public $urlkey;

    public $delete_file;

    public $categories;
    
    public function rules()
    {
        return array(
           array('name, urlkey, location_id, published, enable_registration, description', 'required'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
           array(
              'end_date',
              'compare',
              'compareAttribute'=>'start_date',
              'operator'=>'>',
              'allowEmpty'=>true ,
              'message'=>'{attribute} must be greater than "{compareValue}".'
           ),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'description' => Yii::t('system', 'Description'),
           'enable_registration' => Yii::t('system', 'Enable Registration'),
           'end_date' => Yii::t('system', 'End Date'),
           'file' => Yii::t('system', 'File'),
           'google_map' => Yii::t('system', 'Google Map'),
           'location_id' => Yii::t('system', 'Location'),
           'name' => Yii::t('system', 'Name'),
           'published' => Yii::t('system', 'Published'),
           'start_date' => Yii::t('system', 'Start Date'),
           'urlkey' => Yii::t('system', 'Urlkey'),
           'delete_file' => Yii::t('system', 'Delete File'),
           'categories' => Yii::t('system', 'Categories'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'description' => 'description',
           'enable_registration' => 'enable_registration',
           'end_date' => 'end_date',
           'file' => 'file',
           'google_map' => 'google_map',
           'location_id' => 'location_id',
           'name' => 'name',
           'published' => 'published',
           'start_date' => 'start_date',
           'urlkey' => 'urlkey',
           'delete_file' => 'delete_file',
           'categories' => 'categories',
       );
   }

   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
}
