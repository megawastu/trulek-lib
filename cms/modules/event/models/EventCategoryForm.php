<?php

class EventCategoryForm extends CFormModel
{
    public $id;
    public $description;
    public $file;
    public $name;
    public $urlkey;
    public $published;

    public $delete_file;
    
    public function rules()
    {
        return array(
           array('name, urlkey, published', 'required'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'description' => Yii::t('system', 'Description'),
           'file' => Yii::t('system', 'File'),
           'name' => Yii::t('system', 'Name'),
           'urlkey' => Yii::t('system', 'Urlkey'),
           'published' => Yii::t('system', 'Published'),
           'dekete_file' => Yii::t('system', 'Delete File'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'description' => 'description',
           'file' => 'file',
           'name' => 'name',
           'urlkey' => 'urlkey',
           'published' => 'published',
           'delete_file' => 'delete_file',
       );
   }

   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
}
