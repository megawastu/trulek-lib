<?php

class PortfolioStatusEnum {
    
    const INCOMING = '1';
    const ONGOING = '2';
    const LIVED = '3';
    
    public static function getList()
    {
        return array(
            self::INCOMING => Yii::t('system', 'Incoming'),
            self::ONGOING  => Yii::t('system', 'Ongoing'),
            self::LIVED    => Yii::t('system', 'Lived'),
        );
    }

    public static function getText($dbStatus)
    {
        $text = '';
        switch ($dbStatus)
        {
            case self::INCOMING:
                $text = Yii::t('system', 'Incoming');
                break;
            case self::ONGOING:
                $text = Yii::t('system', 'Ongoing');
                break;
            case self::LIVED:
                $text = Yii::t('system', 'Lived');
                break;
        }

        return $text;
    }
}