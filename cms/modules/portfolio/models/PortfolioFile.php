<?php

class PortfolioFile extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'portfolio_file';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'portfolio' =>array(self::BELONGS_TO, 'Portfolio', 'id'),
        );
    }
    
    public function scopes()
    {
        return array(
            "published" => array("condition" => "`t`.`published`=TRUE"),
            "unpublished" => array("condition" => "`t`.`published`=FALSE"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
            'SaveUploadedFileBehavior'=> 'trulek.extensions.fileuploader.behaviors.SaveUploadedFileBehavior',
        );
    }
}