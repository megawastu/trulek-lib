<?php

class PortfolioFileForm extends CFormModel
{
    public $id;
    public $portfolio_id;
    public $caption;
    public $description;
    public $meta_desc;
    public $meta_keywords;
    public $file;
    public $urlkey; 
    public $published;
    public $ordering;
    public $main;
    
    public $delete_file;

    public function rules()
    {
        return array(
           array('portfolio_id, caption, urlkey, published, ordering, main', 'required'),
           array('urlkey', 'validateUniqueUrlkey'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => false),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'portfolio_id' => Yii::t('system', 'Portfolio'),
           'caption' => Yii::t('system', 'Caption'),
           'description' => Yii::t('system', 'Description'),
           'meta_desc' => Yii::t('system', 'Meta Description'),
           'meta_keywords' => Yii::t('system', 'Meta Keywords'),
           'file' => Yii::t('system', 'File Name'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
           'published' => Yii::t('system', 'Published'),
           'ordering' => Yii::t('system', 'Sort Order'),           
           'main' => Yii::t('system', 'Main'),
           'delete_file' => Yii::t('system', 'Delete File'), 
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'portfolio_id' => 'portfolio_id',
           'caption' => 'caption',
           'description' => 'description',
           'meta_desc' => 'meta_desc',
           'meta_keywords' => 'meta_keywords',
           'file' => 'file',
           'urlkey' => 'urlkey',
           'published' => 'published',
           'ordering' => 'ordering',           
           'main' => 'main',
           'delete_file' => 'delete_file',  
       );
   }

   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }

   /**
    * @return void
    */
   public function validateUniqueUrlkey()
   {
       $criteria = new CDbCriteria();
       $criteria->condition = "`urlkey` = '".$this->urlkey."' AND `id` <> '".$this->id."'";

       $total = PortfolioFile::model()->count($criteria);
       if ($total > 0)
       {
         $this->addError('urlkey', Yii::t('system', 'The {fieldName} was already registered. Please enter another {fieldName}.', array('{fieldName' => 'Urlkey')));
       }
   }
}
