<?php

class PortfolioForm extends CFormModel
{
    public $id;
    public $title;
    public $description;
    public $published;
    public $ordering;
    public $meta_desc;
    public $meta_keywords;    
    public $urlkey;
    public $start_date;
    public $end_date;
    public $status;
    public $url;
    public $categories=array();
    public $clients=array();

    public function rules()
    {
        return array(
           array('title, published, ordering, urlkey, categories', 'required'),
           array('url', 'url'),
           array(
              'end_date',
              'compare',
              'compareAttribute'=>'start_date',
              'operator'=>'>',
              'allowEmpty'=>true ,
              'message'=>'{attribute} must be greater than "{compareValue}".'
            ),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'categories' => Yii::t('system', 'Categories'),
           'title' => Yii::t('system', 'Title'),
           'description' => Yii::t('system', 'Description'),
           'published' => Yii::t('system', 'Published'),
           'ordering' => Yii::t('system', 'Sort Order'),
           'meta_desc' => Yii::t('system', 'Meta Description'),
           'meta_keywords' => Yii::t('system', 'Meta Keywords'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
           'start_date' => Yii::t('system', 'Start Date'),
           'end_date' => Yii::t('system', 'End Date'),
           'status' => Yii::t('system', 'Status'),
           'url' => Yii::t('system', 'Url'),
           'clients' => Yii::t('system', 'Clients'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'categories' => 'categories',
           'title' => 'title',
           'description' => 'description',
           'published' => 'published',
           'ordering' => 'ordering',
           'meta_desc' => 'meta_desc',
           'meta_keywords' => 'meta_keywords',
           'urlkey' => 'urlkey',
           'start_date' => 'start_date',
           'end_date' => 'end_date',
           'status' => 'status',
           'url' => 'url',
           'clients' => 'clients',
       );
   }
}
