<?php

class Portfolio extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'portfolio';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
            'categories' =>array(self::MANY_MANY, 'PortfolioCategory', 'portfolio_category_portfolios(portfolio_id, category_id)'),
            'categoryCount' =>array(self::STAT, 'PortfolioCategory', 'portfolio_category_portfolios(portfolio_id, category_id)'),
            'clients' =>array(self::MANY_MANY, 'PortfolioClient', 'portfolio_client_portfolios(portfolio_id, client_id)'),
            'clientCount' =>array(self::STAT, 'PortfolioClient', 'portfolio_client_portfolios(portfolio_id, client_id)'),
            'files' => array(self::HAS_MANY, 'PortfolioFile', 'portfolio_id'),
            'fileCount' => array(self::STAT, 'PortfolioFile', 'portfolio_id'),
        );
    }
    
    public function scopes()
    {
        return array(
            "published" => array("condition" => "`t`.`published`='1'"),
            "unpublished" => array("condition" => "`t`.`published`='0'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
        );
    }
}