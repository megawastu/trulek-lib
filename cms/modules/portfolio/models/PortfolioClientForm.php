<?php

class PortfolioClientForm extends CFormModel
{
    public $id;
    public $name;
    public $urlkey;
    public $description;
    public $published;
    public $ordering;
    public $meta_desc;
    public $meta_keywords;
    public $file;
    public $url;
    
    public $delete_file;

    public function rules()
    {
        return array(
           array('name, urlkey, published, ordering', 'required'),
           array('url', 'url'),
           array('file', 'file', 'types'=> 'jpg, png, gif',
                                'maxSize'=>1024 * 1024 * 2, // 2MB,
                                'wrongType'=> Yii::t('system', 'Unallowed file type was uploaded. Allowed types: .jpg, .png, .gif'),
                                'tooLarge'=> Yii::t('system', 'The file was larger than 2MB. Please upload a smaller file.'),
                                'allowEmpty' => true),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'name' => Yii::t('system', 'Name'),
           'urlkey' => Yii::t('system', 'SEO Keyword'),
           'description' => Yii::t('system', 'Description'),
           'published' => Yii::t('system', 'Published'),
           'ordering' => Yii::t('system', 'Sort Order'),           
           'meta_desc' => Yii::t('system', 'Meta Description'),
           'meta_keywords' => Yii::t('system', 'Meta Keywords'),
           'url' => Yii::t('system', 'URL'),
           'file' => Yii::t('system', 'File Name'),
           'delete_file' => Yii::t('system', 'Delete File'), 
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'name' => 'name',
           'urlkey' => 'urlkey',
           'url' => 'url',
           'description' => 'description',
           'published' => 'published',
           'ordering' => 'ordering',           
           'meta_desc' => 'meta_desc',
           'meta_keywords' => 'meta_keywords',
           'file' => 'file',
           'delete_file' => 'delete_file', 
       );
   }

   public function behaviors()
   {
       return array(
           'UploadableFormBehavior' => 'trulek.extensions.fileuploader.behaviors.UploadableFormBehavior'
       );
   }
}
