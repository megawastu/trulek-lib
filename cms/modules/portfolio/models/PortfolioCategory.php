<?php

class PortfolioCategory extends CActiveRecord
{
    
    /**
     * Returns the static model of the specified AR class.
     * @return CActiveRecord the static model class
     */
    public static function model($className=__CLASS__)
    {
            return parent::model($className);
    }

    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
            return 'portfolio_category';
    }

    /**
     * @name relations
     * @return Array
     */
    public function relations()
    {
        return array(
           'portfolios'=>array(self::MANY_MANY, 'Portfolio', 'portfolio_category_portfolios(portfolio_id, category_id)'),
           'portfolioCount'=>array(self::STAT, 'Portfolio', 'portfolio_category_portfolios(portfolio_id, category_id)'),
        );
    }
    
    public function scopes()
    {
        return array(
            "published" => array("condition" => "`t`.`published`='1'"),
            "unpublished" => array("condition" => "`t`.`published`='0'"),
        );
    }

    public function behaviors()
    {
        return array(
            'MappingGeneratorBehavior' => 'trulek.extensions.core.behaviors.MappingGeneratorBehavior',
            'AutoTimestampBehavior' => 'trulek.extensions.core.behaviors.AutoTimestampBehavior',
            'AuditTrailBehavior'=> 'trulek.cms.core.audit.behaviors.AuditTrailBehavior',
            'SaveUploadedFileBehavior'=> 'trulek.extensions.fileuploader.behaviors.SaveUploadedFileBehavior',
        );
    }
}