<?php

Yii::import('trulek.cms.modules.portfolio.models.*');

class PortfolioFileService extends CApplicationComponent {
    
    public function create($params=array())
    {
        $modelInstance = new PortfolioFile();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = PortfolioFile::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }
    
    public function delete($id) {
        try {
            $object = PortfolioFile::model()->findByPk($id);
            $old_file = $object->file;
            $object->delete();
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $objects = array();
        $modelInstance = PortfolioFile::model()->findByPk($id);
        $modelInstance->published = 1;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $objects = array();
        $modelInstance = PortfolioFile::model()->findByPk($id);
        $modelInstance->published = 0;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();

//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
    
    public function getNextOrdering(Portfolio $portfolio)
    {
        $sql = "SELECT MAX(`ordering`) FROM `portfolio_file` WHERE `portfolio_file`.`portfolio_id` = '".$portfolio->id."'";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
    
    public function setPortfolio(Portfolio $portfolio) {
        Yii::app()->user->setState('portfolio', $portfolio);
    }

    public function getPortfolio()
    {
        return Yii::app()->user->portfolio;
    }
}
