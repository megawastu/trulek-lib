<?php

Yii::import('trulek.cms.modules.portfolio.models.*');

class PortfolioCategoryService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new PortfolioCategory();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();            
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = PortfolioCategory::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try 
        {            
            $modelInstance->save();            
            $transaction->commit();           
            return $modelInstance;
        } 
        catch (Exception $e) 
        {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            $object = PortfolioCategory::model()->findByPk($id);
            $old_file = $object->file;
            $object->delete();
            
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $objects = array();
        $modelInstance = PortfolioCategory::model()->findByPk($id);
        $modelInstance->published = 0x01;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $objects = array();
        $modelInstance = PortfolioCategory::model()->findByPk($id);
        $modelInstance->published = 0x00;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();
//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
    
    public function getNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `portfolio_category`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
}
