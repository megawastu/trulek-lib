<?php

Yii::import('trulek.cms.modules.portfolio.models.*');

class PortfolioService extends CApplicationComponent {

    public function create($params=array())
    {
        $modelInstance = new Portfolio();
        $modelInstance->setAttributes($params, false);
        
        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            $modelInstance->save();  
            
            foreach ($params['categories'] as $category_id) {
                $xref = new PortfolioCategoryXref();
                $xref->category_id = $category_id;
                $xref->portfolio_id = $modelInstance->id;
                $xref->save();
            }
            
            if (isset($params['clients']) && $params['clients'] != null && count($params['clients']) > 0) {
                foreach ($params['clients'] as $client_id) {
                    $xref = new PortfolioClientXref();
                    $xref->client_id = $client_id;
                    $xref->portfolio_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function update($params=array())
    {
        $modelInstance = Portfolio::model()->findByPk($params['id']);
        $modelInstance->setAttributes($params, false);

        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {
            
            //drop all xref
            PortfolioCategoryXref::model()->deleteAllByAttributes(array('portfolio_id' => $modelInstance->id));
            PortfolioClientXref::model()->deleteAllByAttributes(array('portfolio_id' => $modelInstance->id));
            
            $modelInstance->save();       
            
            foreach ($params['categories'] as $category_id) {
                $xref = new PortfolioCategoryXref();
                $xref->category_id = $category_id;
                $xref->portfolio_id = $modelInstance->id;
                $xref->save();
            }
            
            if (isset($params['clients']) && $params['clients'] != null && count($params['clients']) > 0) {
                foreach ($params['clients'] as $client_id) {
                    $xref = new PortfolioClientXref();
                    $xref->client_id = $client_id;
                    $xref->portfolio_id = $modelInstance->id;
                    $xref->save();
                }
            }
            
            $transaction->commit();
            
            return $modelInstance;
        } catch (Exception $e) {
            $transaction->rollback();
            echo $e->getMessage();
            exit();
        }
    }

    public function delete($id) {
        try {
            //drop all xref
            PortfolioCategoryXref::model()->deleteAllByAttributes(array('portfolio_id' => $id));
            PortfolioClientXref::model()->deleteAllByAttributes(array('portfolio_id' => $id));
            $object = Portfolio::model()->findByPk($id);
            $object->delete();
            
            $old_file = $object->file_name;
            if ($old_file != null) {
               $uploadPath = $_SERVER['DOCUMENT_ROOT'].Yii::app()->assetManager->baseUrl.'/'.Yii::app()->params['baseMediaFolder'].Yii::app()->controller->module->name.'/index';
               Yii::app()->fileUploadService->deleteFile($old_file, $uploadPath);
            }
            
        } catch (Exception $e) {
            echo $e->getMessage();
            exit();
        }
    }

    public function publish($id) {
        $objects = array();
        $modelInstance = Portfolio::model()->findByPk($id);
        $modelInstance->published = 0x01;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            $modelInstance->save();
            
//            $transaction->commit();
            
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }

    public function unpublish($id) {
        $objects = array();
        $modelInstance = Portfolio::model()->findByPk($id);
        $modelInstance->published = 0x00;

//        $transaction = $modelInstance->dbConnection->beginTransaction();
        try {            
            //save
            $modelInstance->save();

//            $transaction->commit();
        } catch (Exception $e) {
            echo $e->getMessage();
//            $transaction->rollback();
            exit();
        }
    }
    
    public function getNextOrdering()
    {
        $sql = "SELECT MAX(`ordering`) FROM `portfolio`";
        $max = Yii::app()->db->createCommand($sql)->queryScalar();
        return $max + 1;
    }
    
    public function setPortfolio(Portfolio $portfolio) {
        Yii::app()->user->setState('portfolio', $portfolio);
    }

    public function getPortfolio()
    {
        return Yii::app()->user->portfolio;
    }
}
