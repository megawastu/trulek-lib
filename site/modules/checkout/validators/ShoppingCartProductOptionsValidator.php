<?php
Yii::import('trulek.cms.modules.catalog.models.CatalogOption');
Yii::import('trulek.cms.modules.catalog.enums.StockStatusEnum');

class ShoppingCartProductOptionsValidator extends CValidator
{    
    /**
     * @var string
     */
    public $itemId;
	
    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object,$attribute)
    {
            $values=$object->$attribute;
            
            $itemId = $this->itemId;
            $item_id = $object->$itemId;
            
            //get product
            $modelInstance = CatalogProduct::model()->findByPk($item_id);
            //get product options
            $options = $modelInstance->options;
            
            foreach ($options as $option)
            {
                $catalogOption = CatalogOption::model()->findByPk($option->option_id);
                if ($option->required > 0)
                {                    
                    if (!isset($values[$option->option_id]) || $values[$option->option_id] == "" || $values[$option->option_id] == null)
                    {
                        $message=($this->message!==null) ? $this->message : Yii::t('system','The {optionName} may not be left blank.', array('{optionName}' => $catalogOption->name));
                        $this->addError($object,$attribute,$message);
                    }
                }
            }
    }
}
