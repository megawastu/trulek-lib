<?php

Yii::import('trulek.cms.modules.catalog.enums.StockStatusEnum');

class ShoppingCartCheckStockValidator extends CValidator
{    
	/**
	 * @var string
	 */
	public $itemId;
	
    /**
     * Validates the attribute of the object.
     * If there is any error, the error message is added to the object.
     * @param CModel $object the object being validated
     * @param string $attribute the attribute being validated
     */
    protected function validateAttribute($object,$attribute)
    {
            $value=$object->$attribute;
			
			$itemId = $this->itemId;
			$item_id = $object->$itemId;
			
			//check stock on hand, and then check the active store setting to see stock_checkout setting
			
			//get product
			$modelInstance = CatalogProduct::model()->findByPk($item_id);
			//get active store
			$activeStore = Yii::app()->user->activeStore;			
			
			//check if the stock is not zero			
			if ( ($modelInstance->stock_on_hand <= 0 || $modelInstance->stock_status==StockStatusEnum::SOLD_OUT) && $activeStore->stock_checkout <= 0)
			{
			   $message=($this->message!==null) ? $this->message : Yii::t('system','{attribute} is out of stock.', array('{value}'=>$modelInstance->stock_on_hand));
               $this->addError($object,$attribute,$message);
			}
			
			//check if the quantity is more than the available stock
			if ( ($value > $modelInstance->stock_on_hand) && $activeStore->stock_checkout <= 0 )
			{
				$message=($this->message!==null) ? $this->message : Yii::t('system','The {attribute} you have entered is more than the stock available.', array('{value}'=>$object->quantity));
                $this->addError($object,$attribute,$message);
			}
    }
}
