<?php

Yii::import('trulek.site.modules.checkout.components.ShoppingCart');
Yii::import('trulek.site.modules.checkout.components.ShoppingCartItem');
Yii::import('trulek.site.modules.checkout.validators.ShoppingCartCheckStockStatusValidator');

class CheckoutMethodForm extends CFormModel
{
    public $checkout_method;

    public function rules()
    {
        return array(
           array('checkout_method', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'checkout_method' => Yii::t('system', 'Checkout Method'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'checkout_method' => 'checkout_method',
       );
   }
}
