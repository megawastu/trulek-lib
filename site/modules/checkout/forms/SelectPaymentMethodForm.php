<?php

class SelectPaymentMethodForm extends CFormModel
{
	public $id;
	
    public function rules()
    {
        return array(
           array('id', 'required'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Payment Method'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
       );
   }
}
