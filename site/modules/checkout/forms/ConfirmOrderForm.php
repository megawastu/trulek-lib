<?php

class ConfirmOrderForm extends CFormModel
{
	public $confirmed;
	
    public function rules()
    {
        return array(
           array('confirmed', 'required'),
		   array('confirmed', 'validateConfirmed'),
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'confirmed' => Yii::t('system', 'I have checked and confirmed that this is the right order specification I have selected.'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'confirmed' => 'confirmed',
       );
   }
   
   /**
    * @return void
    */
   public function validateConfirmed()
   {
       if ($this->confirmed <= 0) {
	      $this->addError('confirmed', Yii::t('system', 'You have to confirm your product selection before continue.'));
	   }
   }
}
