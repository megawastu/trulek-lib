<?php

Yii::import('trulek.site.modules.checkout.components.ShoppingCart');
Yii::import('trulek.site.modules.checkout.components.ShoppingCartItem');
Yii::import('trulek.site.modules.checkout.validators.ShoppingCartCheckStockValidator');

class ShoppingCartForm extends CFormModel
{
    public $item_id;
    public $quantity;
    public $options=array();

    public function rules()
    {
        return array(
           array('item_id, quantity', 'required'),
           array('quantity', 'numerical'),           
           array('quantity', 'trulek.site.modules.checkout.validators.ShoppingCartCheckStockValidator', 'itemId'=>'item_id'),
           array('options', 'trulek.site.modules.checkout.validators.ShoppingCartProductOptionsValidator', 'itemId'=>'item_id'),
			
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'item_id' => Yii::t('system', 'Item'),
           'quantity' => Yii::t('system', 'Quantity'),
           'options' => Yii::t('system', 'Options'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'item_id' => 'item_id',
           'quantity' => 'quantity',
           'options' => 'options',
       );
   }
   
   public function onBeforeValidate($event)
   {
      if ($this->quantity <= 0) {
        $this->quantity = 1;
      }
   }
}
