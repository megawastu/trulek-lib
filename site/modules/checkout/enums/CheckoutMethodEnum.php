<?php

class CheckoutMethodEnum {
    const GUEST = "guest";
    const REGISTER = "register";
    const MEMBER = "member";
    
    static function getList()
    {
        return array(
                        self::GUEST => self::GUEST,
                        self::REGISTER => self::REGISTER,
                        self::MEMBER => self::MEMBER,
                    );
    }
}
