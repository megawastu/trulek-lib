<?php

class RequestQuoteStatusEnum {
    const OPEN = 1;
    const CLOSED = 2;
    
    static function getList()
    {
        return array(
			self::OPEN => Yii::t('system', 'New'),
			self::CLOSED => Yii::t('system', 'Closed'),
		);
    }
	
	static function get($status)
	{
	   switch ($status)
	   {
	      case 2:
		     $status = Yii::t('system', 'Closed');
		     break;
	      case 1:
		  default:
			$status = Yii::t('system', 'New');
			break;
	   }
	   
	   return $status;
	}
}
