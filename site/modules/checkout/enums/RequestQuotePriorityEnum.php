<?php

class RequestQuotePriorityEnum {
    const VERY_LOW = "Very Low";
    const LOW = "Register";
    const MEDIUM = "Medium";
    const HIGH = "High";
    const VERY_HIGH = "Very High";
    
    static function getList()
    {
        return array(
			self::VERY_LOW => self::VERY_LOW,
			self::LOW => self::LOW,
			self::MEDIUM => self::MEDIUM,
			self::HIGH => self::HIGH,
			self::VERY_HIGH => self::VERY_HIGH,
		);
    }
	
	static function get($status)
	{
	   switch ($status)
	   {
		   case 5:
		     $status = Yii::t('system', 'Very High');
		     break;
		   case 1:
		     $status = Yii::t('system', 'Very Low');
		     break;
		  case 3:
		     $status = Yii::t('system', 'Medium');
		     break;
	      case 2:
		     $status = Yii::t('system', 'Low');
		     break;
		   case 1:
		     $status = Yii::t('system', 'Very Low');
		     break;
	      case 4:
		  default:
			$status = Yii::t('system', 'High');
			break;
	   }
	   
	   return $status;
	}
}
