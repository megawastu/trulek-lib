<?php

Yii::import('trulek.site.modules.checkout.components.payment.interfaces.IPayment');

class BankTransfer implements IPayment {
   
   public function paymentUrl()
   {
      return 'payment/bankTransfer';
   }
}