<?php

Yii::import('trulek.site.modules.checkout.components.shipping.interfaces.IShipper');

class FlatRate implements IShipper {
   
   public function calculateRate()
   {
      return 15000;
   }
}