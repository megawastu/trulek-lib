<?php

class Checkout
{    
    public $checkoutMethod=null;
    public $billingDetail=null; //AccountAddressBook id
	public $shippingDetailSameAsBilling=null;
	
    public $shippingDetail=null; //AccountAddressBook id
    
    public $shippingMethod=null;
    public $paymentMethod=null;
    
    public $orderConfirmed=null;
	    
    public function getCheckoutMethod() {
        return $this->checkoutMethod;
    }

    public function setCheckoutMethod($checkoutMethod) {
        $this->checkoutMethod = $checkoutMethod;
    }

    public function getBillingDetail() {
        return $this->billingDetail;
    }

    public function setBillingDetail($billingDetail) {
		if ($billingDetail->address_type == AddressTypeEnum::BILLING) {
			$this->billingDetail = $billingDetail->id;
		}
    }

    public function getShippingDetail() {
        return $this->shippingDetail;
    }

    public function setShippingDetail($shippingDetail) {
		if ($shippingDetail->address_type == AddressTypeEnum::SHIPPING) {
			$this->shippingDetail = $shippingDetail->id;
		}
    }

    public function getShippingMethod() {
        return $this->shippingMethod;
    }

    public function setShippingMethod($shippingMethod) {
        $this->shippingMethod = $shippingMethod->id;
    }

    public function getPaymentMethod() {
        return $this->paymentMethod;
    }

    public function setPaymentMethod($paymentMethod) {
        $this->paymentMethod = $paymentMethod->id;
    }

    public function getOrderConfirmed() {
        return $this->orderConfirmed;
    }

    public function setOrderConfirmed($orderConfirmed) {
        $this->orderConfirmed = $orderConfirmed;
    }
    
}
