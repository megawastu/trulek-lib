<?php

class ShoppingCartItem
{
    public $id;
    public $quantity;
    public $rentInfo=null;
    public $options=array();
    
    public function getId() {
        return $this->id;
    }

    public function setId($id) {
        $this->id = $id;
    }

    public function getQuantity() {
        return $this->quantity;
    }

    public function setQuantity($quantity) {
        $this->quantity = $quantity;
    }
    
    public function getOptions() {
        return $this->options;
    }

    public function setOptions($options) {
        $this->options = $options;
    }
    
    public function getRentInfo() {
        return $this->rentInfo;
    }

    public function setRentIInfo(ShoppingCartRentalInformation $rentInfo) {
        $this->rentInfo = $rentInfo;
    }

}
