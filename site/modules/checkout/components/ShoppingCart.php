<?php

class ShoppingCart
{    
    public $items = array();
    public $coupons = array();
    public $lastUrl;
    
    private function _itemExists($id)
    {
        return array_key_exists($id, $this->items);
    }

    /**
     * @param CatalogProduct $product
     * @param int $qty
     * @param Array $options 
     * @return void
     */
    public function addItem($id, $qty=1, $options=array())
    {        
        if ($this->_itemExists($id) === false) { // new item
            $cartItem = new ShoppingCartItem();
            $cartItem->quantity = $qty;
        }
        else //update item quantity
        {
            $cartItem = $this->items[$id];
            $cartItem->quantity = $cartItem->quantity + $qty;
        }
        
        $cartItem->id = $id;
        
        if (isset($options) && count($options) > 0) {
           $cartItem->options = $options;
        }
        
        if ($cartItem->id != "" && $cartItem->id != null)
        {
            $this->items[$cartItem->id] = $cartItem;
        }
    }
    
    public function removeItem($id)
    {       
       if (count($this->items) > 0) {
           foreach ($this->items as $item_id => $item) {
              if ($item_id == $id)
              {
                  unset ($this->items[$id]);
              }
           }
           return true;
       }
       else
       {
           return false;
       }
    }

    public function removeItems ($item_ids = array())
    {
        if (count($this->items) > 0) {
            foreach ($item_ids as $id) {
               if (array_key_exists($id, $this->items[$id]))
               {
                  unset($this->items[$id]);
               }
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Get the cart sub total (only sum the product price
     * @return <int>
     */
    public function getCartSubTotal()
    {
       $subtotal = 0;
       if (count($this->items) > 0)
       {
           foreach ($this->items as $item)
           {
               $subtotal += ($item->price * $item->quantity);
           }
       }

       return $subtotal;
    }

    public function getCoupons()
    {
        return $this->coupons;
    }

    public function getCartGrandTotal()
    {
        $subtotal = $this->getCartSubTotal();
        
        //apply coupons
        if (isset(Yii::app()->store->config['enable_coupon']) && Yii::app()->store->config['enable_coupon'] > 0) {
            if (count($this->coupons) > 0)
            {
                foreach ($this->coupons as $coupon)
                {
                    if ($coupon->unit == 1)  //percentage
                    {
                        $couponValue = $subtotal * ($coupon->coupon_value / 100);
                        $subtotal -= $couponValue;
                    }
                    else if ($coupon->unit == 2) //total
                    {
                        $subtotal -= $coupon->coupon_value;
                    }
                }
            }
        }

        //calculate shipping cost
        if (Yii::app()->user->hasState("checkout"))
        {
            if (Yii::app()->user->checkout->shippingMethod != null && Yii::app()->user->checkout->shippingMethod != "")
            {
                $shippingMethod = Yii::app()->user->checkout->shippingMethod;
                $service = new $shippingMethod->serviceName();
                if (count($this->items) > 0) {
                   $totalShippingCost = $service->calculateTotalShippingRates($this->items);
                   $subtotal += $totalShippingCost;
                }
            }
        }

        return $subtotal;
    }

    public function calculateTotalPricePerItem($price, $quantity)
    {
       return $price * $quantity;
    }
	
    public function getItems()
    {
        return $this->items;
    }

    public function getTotalItemsInCart()
    {
        $quantity = 0;
        if (count($this->items) > 0)
        {
            foreach ($this->items as $item)
            {
                $quantity += $item->quantity;
            }
        }
        return $quantity;
    }

    public function getTotalItemWeight()
    {
        $totalWeight = 0;
        if (count($this->items) > 0)
        {
            foreach($this->items as $item)
            {
                $totalWeight += $item->weight;
            }
        }
        return $totalWeight;
    }
    
    public function updateCart($ids=array(), $quantities=array())
    {
        foreach ($ids as $key => $id)
        {
            $cartItem = $this->items[$id];
            $cartItem->quantity = $quantities[$key];
        }
    }

    public function emptyCart()
    {
        foreach ($this->items as $item)
        {
            unset($this->items[$item->id]);
        }

        foreach ($this->coupons as $coupon)
        {
            unset($this->coupons[$coupon->id]);
        }
        
        $this->items = array();
        $this->coupons = array();
    }

    public function hasError()
    {
        if ($this->errorCode > 0)
        {
            return true;
        }
        return false;
    }

    public function addCoupon(Coupon $coupon)
    {
        if (!array_key_exists($coupon->id, $this->coupons))
        {
            $this->coupons[$coupon->id] = $coupon;
        }
    }

    public function removeCoupon($coupon_id)
    {
        if (array_key_exists($coupon_id, $this->coupons))
        {
           unset($this->coupons[$coupon_id]);
        }
    }    
}
