<?php

Yii::import('trulek.cms.modules.catalog.models.*');
Yii::import('trulek.site.modules.checkout.components.ShoppingCart');
Yii::import('trulek.site.modules.checkout.components.ShoppingCartItem');

class ShoppingCartService extends CApplicationComponent 
{       
    public $onlyOneItemAllowed=false;
    
    public function init()
    {
        parent::init();
        $this->createShoppingCart();
    }
    
    public function createShoppingCart()
    {
        $shoppingCart = new ShoppingCart();
        if (Yii::app()->user->hasState("cart"))
        {
            $shoppingCart = Yii::app()->user->cart;
        }
        
        Yii::app()->user->setState("cart", $shoppingCart);
    }
    
    public function getShoppingCart()
    {
        $cart = null;
        if (Yii::app()->user->hasState("cart"))
        {
            $cart = Yii::app()->user->cart;
        }
        
        return $cart;
    }
	
	public function getShoppingCartItems()
	{
	   $list = array();
	   $cart = null;
	   if (Yii::app()->user->hasState("cart"))
       {
            $cart = Yii::app()->user->cart;
       }
	   
	   if ($cart != null)
	   {
	      $list = $cart->items;
	   }
	   return $list;
	}
    
    /**
     * @param CatalogProduct $product
     * @param int $qty
     * @param Array $options 
     * @return void
     */
    public function addToShoppingCart(CatalogProduct $product, $qty=1, $options=array())
    {
        if ($this->onlyOneItemAllowed === true) { //empty cart before add another item
            $this->emptyShoppingCart();
        }
        $id = $product->id;
        Yii::app()->user->cart->addItem($id, $qty, $options);
    }
    
    public function removeFromShoppingCart (CatalogProduct $product)
    {
        $id = $product->id;
        Yii::app()->user->cart->removeItem($id);
    }
    
    public function updateShoppingCart($products=array(), $quantities=array())
    {
        $ids = array();
        foreach ($products as $key => $product)
        {
            $ids[] = $product->id;
        }
        
        Yii::app()->user->cart->updateCart($ids, $quantities);
    }
    
    public function emptyShoppingCart()
    {
        Yii::app()->user->cart->emptyCart();
    }
}
