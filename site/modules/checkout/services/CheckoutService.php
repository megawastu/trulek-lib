<?php

Yii::import('trulek.cms.modules.account.models.*');
Yii::import('trulek.site.modules.checkout.components.Checkout'); 
Yii::import('trulek.site.modules.checkout.enums.CheckoutMethodEnum');
Yii::import('trulek.cms.modules.account.enums.AddressTypeEnum');
Yii::import('application.modules.account.enums.AccountTypeEnum');
Yii::import('application.modules.account.enums.DefaultAccountGroupEnum');
Yii::import('application.modules.security.models.LoginForm');
Yii::import('trulek.site.modules.checkout.components.shipping.shippers.*');

class CheckoutService extends CApplicationComponent
{       
    public function init()
    {
        parent::init();
        
        $checkout = new Checkout();
        
        if (Yii::app()->user->hasState("checkout") === true)
        {
            $checkout = Yii::app()->user->checkout;
        }
        
        Yii::app()->user->setState("checkout", $checkout);
        
        if (Yii::app()->user->isGuest === false)
        {
            Yii::app()->user->checkout->checkoutMethod = CheckoutMethodEnum::MEMBER;
        }
    }
        
    public function getCheckoutMethod() {
        return Yii::app()->user->checkout->checkoutMethod;
    }

    public function setCheckoutMethod($checkoutMethod) {
        Yii::app()->user->checkout->checkoutMethod = $checkoutMethod;
    }    
	
	public function getBillingDetail() {
        $id = Yii::app()->user->checkout->billingDetail;
		$accountAddressBook = AccountAddressBook::model()->findByPk($id);
        return $accountAddressBook;
    }
	
    public function setBillingDetail($accountAddressBook) {
        Yii::app()->user->checkout->setBillingDetail($accountAddressBook);
    }    
	
	public function getShippingDetail() {
		$id = Yii::app()->user->checkout->shippingDetail;
		$accountAddressBook = AccountAddressBook::model()->findByPk($id);
        return $accountAddressBook;
    }

    public function setShippingDetail($accountAddressBook) {
        Yii::app()->user->checkout->setShippingDetail($accountAddressBook);
    }    
	
	public function getCurrentStep()
	{
	   $step = 0;
	   
	   $checkout = Yii::app()->user->checkout;
	   if ($checkout->checkoutMethod == null)
	   {
	      $step = 1;
	   }
	   
	   if ($checkout->billingDetail != null)
	   {
	      $step = 2;
	   }
	   
	   if ($checkout->shippingDetail != null)
	   {
	      $step = 3;
	   }
	   
	   if ($checkout->shippingMethod != null)
	   {
	      $step = 4;
	   }
	   
	   if ($checkout->paymentMethod != null)
	   {
	      $step = 5;
	   }
	   
	   if ($checkout->orderConfirmed != null && $checkout->orderConfirmed === true)
	   {
	      $step = 6;
	   }	   
	   
	   return $step;
	}
	
	public function isCheckoutProcessCompleted()
	{
	   $completed = false;
	   $checkout = Yii::app()->user->checkout;
	   
	   if (
			$checkout->checkoutMethod != null
			&& $checkout->billingDetail != null && $checkout->billingDetail != ""
			&& $checkout->shippingDetail != null && $checkout->shippingDetail != ""
			&& $checkout->shippingMethod != null && $checkout->shippingMethod != ""
			&& $checkout->paymentMethod != null && $checkout->paymentMethod != ""
			&& $checkout->orderConfirmed != null && $checkout->orderConfirmed  === true
		   )
	   {
	      $completed = true;
	   }
		   
	   return $completed;
	}
	
	public function isRequestQuoteCompleted()
	{
	   $completed = false;
	   
	   if (Yii::app()->user->hasState("checkout")) {
		   $checkout = Yii::app()->user->checkout;
		   
		   if (
				$checkout->checkoutMethod != null
				&& $checkout->billingDetail != null && $checkout->billingDetail != ""
				&& $checkout->shippingDetail != null && $checkout->shippingDetail != ""
				&& $checkout->orderConfirmed != null && $checkout->orderConfirmed  === true
			   )
		   {
			  $completed = true;
		   }
	   }
	   
	   return $completed;
	}
    
    public function checkoutMethodValidation()
    {
        if (isset($_POST['CheckoutMethodForm']))
        {
            $checkoutMethodForm = new CheckoutMethodForm();
            $checkoutMethodForm->setAttributes($_POST['CheckoutMethodForm']);
            Yii::app()->user->checkout->checkoutMethod = $checkoutMethodForm->checkout_method;
        }
        
        if (isset($_POST['LoginForm']))
        {
            Yii::app()->user->checkout->checkoutMethod = CheckoutMethodEnum::MEMBER;
        }
    }
	
	public function registerAccount(RegisterForm $form)
	{
		  $transaction = Yii::app()->db->beginTransaction();
		  try {
			  
			  //create account
			  $modelInstance = new Account();
			  $modelInstance->setAttributes($form->attributes, false);
			  $modelInstance->account_type_id = AccountTypeEnum::CUSTOMER;
			  $unencrypted_password = $modelInstance->password; //will be used for auto login.
			  $encrypted_password = md5($modelInstance->password);
			  $modelInstance->password = $encrypted_password;
			  $modelInstance->save();
			  
			  //save xref
			  $xref = new AccountGroupXref();
			  $xref->account_id = $modelInstance->id;
			  $xref->account_group_id = DefaultAccountGroupEnum::DEF;
			  $xref->save();
			  $transaction->commit();

			  //auto login
			  $loginForm = new LoginForm();
			  //set form attributes
			  $loginForm->email = $modelInstance->email;
			  $loginForm->password = $unencrypted_password;
			  
			  if ($loginForm->validate()) {
			     Yii::app()->user->checkout->checkoutMethod = CheckoutMethodEnum::MEMBER;
			  }
		  
		  } catch (Exception $e) {
			  echo $e->getMessage();
			  exit();
			  $transaction->rollback();
		  }
	}
	
	public function addAddress(AddressBookForm $form)
	{
	   try {
		   $modelInstance = new AccountAddressBook();
		   $modelInstance->setAttributes($form->attributes, false);
		   $modelInstance->account_id = Yii::app()->user->id;
		   $modelInstance->save();
		   
		   switch ($modelInstance->address_type)
		   {
		      case AddressTypeEnum::SHIPPING:
			     $this->setShippingDetail($modelInstance);
			     break;
		      case AddressTypeEnum::BILLING:
			  default:
				 $this->setBillingDetail($modelInstance);
			     break;
		   }
		   
	   } catch (Exception $e) {
	      echo $e->getMessage();
		  exit();
	   }
	}
	
	public function useExistingAddress($addressId)
	{
	   try {
	      $modelInstance = AccountAddressBook::model()->findByPk($addressId);
		  switch ($modelInstance->address_type)
		   {
		      case AddressTypeEnum::SHIPPING:
			     $this->setShippingDetail($modelInstance);
			     break;
		      case AddressTypeEnum::BILLING:
			  default:
				 $this->setBillingDetail($modelInstance);
			     break;
		   }
	   } catch (Exception $e) {
	      echo $e->getMessage();
		  exit();
	   }
	}
	
	public function setShippingDetailSameAsBilling($boo)
	{
	   Yii::app()->user->checkout->shippingDetailSameAsBilling = $boo;
	}
	
	public function isShippingDetailSameAsBilling()
	{
	   return Yii::app()->user->checkout->shippingDetailSameAsBilling;
	}
	
	public function selectShippingOption($shipper_id)
	{
	   $modelInstance = CatalogShippingOption::model()->findByPk($shipper_id);
	   Yii::app()->user->checkout->setShippingMethod($modelInstance);
	}
	
	public function selectPaymentMethod($id)
	{
	   $modelInstance = CatalogPaymentMethod::model()->findByPk($id);
	   Yii::app()->user->checkout->setPaymentMethod($modelInstance);
	}
	
	public function calculateShippingRate($shipper_id)
	{
	   $modelInstance = CatalogShippingOption::model()->findByPk($shipper_id);
	   $className = '';
	   $rate = 0;
	   
	   $arrs = explode("_", $modelInstance->unique_name);
	   
	   if ($arrs != null && count($arrs) > 0) {
		   foreach ($arrs as $arr)
		   {
			  if ($arr != "_")
			  {
				 $className .= ucfirst($arr);
			  }
		   }
	   }
	   else
	   {
	      $className = ucfirst($arrs);   
	   }
	   
	   if ($className != "")
	   {
	      $refClass = new ReflectionClass($className);
          $objShipper = $refClass->newInstance();
		  $rate = $objShipper->calculateRate();
	   }
	   
	   return $rate;
	}
	
	public function confirmOrder(ConfirmOrderForm $form)
	{
	   Yii::app()->user->checkout->orderConfirmed= $form->confirmed;
	}
}
