<?php

class ShoppingCartNotEmptyFilter extends CFilter
{    
    public $redirectTo;
    
    protected function preFilter($filterChain) // logic being applied before the action is executed
    {
        $message = Yii::t('system', 'No items found in the cart');
        $redirect = false;
        
        $cart = Yii::app()->shoppingCartService->getShoppingCart();
        
        if ($cart == null)
        {
            Yii::app()->user->setFlash('error', $message);
            $redirect = true;
        }
        
        if (!is_null($cart))
        {
            if (Yii::app()->user->cart->getTotalItemsInCart() <= 0) {
                Yii::app()->user->setFlash('error', $message);
                $redirect = true;
            }
        }
        
        if ($redirect === true)
        {  
            if ($this->redirectTo != null && $this->redirectTo != "") {
               Yii::app()->controller->redirect($this->redirectTo);
            }
            else
            {
                Yii::app()->controller->redirect(Yii::app()->homeUrl.'checkout/cart');
            }
        }
        
        return true; // false if the action should not be executed
    }

    protected function postFilter($filterChain)
    {
        // logic being applied after the action is executed
    }
}
