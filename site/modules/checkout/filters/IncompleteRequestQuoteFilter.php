<?php

class IncompleteRequestQuoteFilter extends CFilter
{    
    public $redirectTo;
    
    protected function preFilter($filterChain) // logic being applied before the action is executed
    {
        $message = Yii::t('system', 'Request quote is not completed');
        $completed = false;
        
        $completed = Yii::app()->checkoutService->isRequestQuoteCompleted();
		
        if ($completed)
        {
            Yii::app()->user->setFlash('error', $message);
			if ($this->redirectTo != null && $this->redirectTo != "") 
			{
               Yii::app()->controller->redirect($this->redirectTo);
            }
            else
            {
                Yii::app()->controller->redirect(Yii::app()->homeUrl.'checkout/requestQuote');
            }
        }
        
        return true; // false if the action should not be executed
    }

    protected function postFilter($filterChain)
    {
        // logic being applied after the action is executed
    }
}
