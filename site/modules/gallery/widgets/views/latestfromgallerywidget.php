<?php if (isset($modelInstanceList) && count($modelInstanceList) > 0) { ?>
<div class="widget bodywidget featured-portfolio-items">
    <h3 class="widgettitle"><?php echo Yii::t('system', 'Latest'); ?> <a href="<?php echo Yii::app()->homeUrl; ?>gallery/photos/<?php echo $album->urlkey; ?>"><?php echo Yii::t('system', $album->title); ?></a></h3>

    <!-- thumbnail scroller markup begin -->
    <div id="tS2" class="jThumbnailScroller">
            <div class="jTscrollerContainer">
                    <div class="jTscroller">
                            <?php 
                               foreach ($modelInstanceList as $modelInstance) { 
                                    $folder = Yii::app()->getAssetManager()->baseUrl.'/'.Yii::app()->params['baseMediaFolder'].'gallery/photos';
                                    $img = Yii::app()->assetManager->baseUrl.'/'.Yii::app()->params['baseMediaFolder'].'images/no_image_'.Yii::app()->language.'.jpg';
                                    if ($modelInstance->file_name != null || $modelInstance->file_name != "") {
                                        $img = $folder.'/'.$modelInstance->file_name;
                                    }
                                    $thumb = Yii::app()->getAssetManager()->baseUrl.'/vendors/image-1.4.1/image.php'.$img.'?height=100&image='.$img;
                             ?>
                            <a href="<?php echo Yii::app()->homeUrl; ?>gallery/photos/<?php echo $album->urlkey; ?>">
                                <img src="<?php echo $thumb; ?>" alt="<?php echo $modelInstance->caption; ?>" />
                            </a>
                            <?php } ?>
                    </div>
            </div>
            <a href="#" class="jTscrollerPrevButton"></a>
            <a href="#" class="jTscrollerNextButton"></a>
    </div>
    <!-- thumbnail scroller markup end -->
    <script type="text/javascript">
    /* <![CDATA[ */
            /* calling thumbnailScroller function with options as parameters */					
            (function($){
                    window.onload=function(){ 						
                            $("#tS2").thumbnailScroller({ 
                                    scrollerType:"clickButtons"
                            });							
                    }
            })(jQuery);					
    /* ]]> */
    </script>				
</div><!--//featured-portfolio-items-->
<?php } ?>