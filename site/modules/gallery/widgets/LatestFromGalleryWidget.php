<?php

Yii::import('trulek.cms.modules.gallery.models.*');

class LatestFromGalleryWidget extends CWidget {
    
    public $album_urlkey;
    public $limit;
    
    public function init() { parent::init(); }

    public function run() { $this->renderContent(); }

    protected function renderContent()
    {
        $album = Album::model()->findByAttributes(array('urlkey' => $this->album_urlkey));
        
        $criteria = new CDbCriteria();
        $criteria->condition="`published` = TRUE";
        $criteria->order="`date_created` DESC";
        $criteria->limit=$this->limit;
        
        $modelInstanceList = array();
        $modelInstanceList = $album->photos(array('condition' => $criteria->condition,
                                                        'order' => $criteria->order,
                                                        'limit' => $criteria->limit
                                                      )
                                                 );
        
        $data['album'] = $album;
        $data['modelInstanceList'] = $modelInstanceList;
        $this->render(strtolower(__CLASS__), (isset($data) && count($data) > 0) ? $data : '');
    }
}
