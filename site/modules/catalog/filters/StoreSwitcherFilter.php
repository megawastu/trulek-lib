<?php

Yii::import('trulek.cms.modules.catalog.models.CatalogStoreSetting');

class StoreSwitcherFilter extends CFilter
{    
    protected function preFilter($filterChain) // logic being applied before the action is executed
    {
        $store = Yii::app()->catalogStoreSettingService->getActiveStore();        
        Yii::app()->user->setState('activeStore', $store);
		
        if (Yii::app()->request->isPostRequest)
        {
            if (isset($_POST['_store_id']) && $_POST['_store_id'] != "")
            {
                $id = $_POST['_store_id'];
                $store = CatalogStoreSetting::model()->findByPk($id);
                Yii::app()->catalogStoreSettingService->switchStore($store);
            }
        }
        else
        {
            if (isset($_GET['store']) && $_GET['store'] != "")
            {
                $strStoreName = strtolower($_GET['store']);
                $store = CatalogStoreSetting::model()->findByAttributes(array('store_name' => $strStoreName));
                if ($store != null) {
                    Yii::app()->catalogStoreSettingService->switchStore($store);
                }
            }
        }
        return true; // false if the action should not be executed
    }

    protected function postFilter($filterChain)
    {
        // logic being applied after the action is executed
    }
}
