<?php

class CatalogProductReviewForm extends CFormModel
{
    public $id;
    public $product_id;
    public $email;
    public $name;
    public $text;
    public $rating;
    public $published;
    public $notify_reviewer;
	public $recaptcha;

    public function rules()
    {
        return array(
           array('product_id, email, name, text, published', 'required'),
           array('email', 'email'),
		   array('recaptcha',
                    'application.extensions.recaptcha.EReCaptchaValidator',
                    'privateKey'=>'6Ld90sQSAAAAAIbZ3hv1DXbA2VRm_H9AkUiV70Cw')
        );
    }

    /**
    * Set Labels for this form
    * @return <Array>
    */
   public function attributeLabels()
   {
       return array(
           'id' => Yii::t('system', 'Id'),
           'product_id' => Yii::t('system', 'Product Id'),
           'email' => Yii::t('system', 'Email'),
           'name' => Yii::t('system', 'Name'),
           'text' => Yii::t('system', 'Your Review'),
           'rating' => Yii::t('system', 'Rating'),
           'published' => Yii::t('system', 'Published'),
           'notify_reviewer' => Yii::t('system', 'Notify Reviewer'),
		   'recaptcha' => Yii::t('system', 'Prove that you are human'),
       );
   }

   /**
    * Set Safe Attributes Name
    * @return <Array>
    */
   public function attributeNames()
   {
       return array(
           'id' => 'id',
           'product_id' => 'product_id',
           'email' => 'email',
           'name' => 'name',
           'text' => 'text',
           'rating' => 'rating',
           'published' => 'published',
           'notify_reviewer' => 'notify_reviewer',
		   'recaptcha' => 'recaptcha',
       );
   }
}
