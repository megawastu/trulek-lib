<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogProductDetailService extends CApplicationComponent {
    
     public function getRentalPrices(CatalogProduct $modelInstance)
     {
        $rentalPrices = $modelInstance->rentalPrices(array("order" => "`period_unit` ASC"));
        if ($rentalPrices == null)
        {
            $rentalPrices = array();
        }

        return $rentalPrices;
     }

     public function getRentalSpecialPrices(CatalogProduct $modelInstance)
     {
         
     }

     public function getRentalPriceOnWeekend(CatalogProduct $modelInstance)
     {
         $rentalPricesOnWeekend = array();
         
         //6 = Saturday, 7 = Sunday on the database
         $arrCriteria = array();
         $arrCriteria = array(
             "condition" => "`product_id` = '".$modelInstance->id."' AND `day` = '6'",
             "order" => "`day` ASC",
             "limit" => 1,
         );

         $rentalPricesForWeekend = $modelInstance->rentalSpecialPrices($arrCriteria);

         if ($rentalPricesForWeekend == null)
         {
             $rentalPricesForWeekend = array();
         }
         
         return $rentalPricesForWeekend;
     }

     public function getTotalReviews(CatalogProduct $product)
     {
         $totalReviews = 0;
         $reviewCriteria = array();
         $reviewCriteria['condition'] = "`published` = '1'";
         $totalReviews = $product->reviewCount($reviewCriteria);
         if ($totalReviews == null)
         {
             $totalReviews = 0;
         }

         return $totalReviews;
     }
     
     public function getRatingSummary(CatalogProduct $product)
     {
        $arrCriteria = array();
        $arrCriteria['condition']= "`published` = '1'";
        
        $reviewList = $product->reviews($arrCriteria);
        if ($reviewList == null)
        {
            $reviewList = array();
        }

        $totalReview = 0;
        $averageRating = 0;
        if (count($reviewList) > 0)
        {
            $totalReview = count($reviewList);
            $sumRating = 0;
            foreach ($reviewList as $review)
            {
                $sumRating += $review->rating;
            }

            $averageRating = $sumRating / $totalReview;
        }

        return number_format($averageRating, 0);
     }
     

}
