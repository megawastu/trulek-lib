<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogStoreSettingService extends CApplicationComponent {
    
    public function getDefaultStore()
    {
        $modelInstance = null;
        $modelInstance = CatalogStoreSetting::model()->default()->find();

        return $modelInstance;
    }

    public function getActiveStore()
    {
        if (Yii::app()->user->hasState('activeStore')) {
            $curActiveStore = Yii::app()->user->activeStore;
            //refresh session
            $activeStore = CatalogStoreSetting::model()->findByPk($curActiveStore->id);
            Yii::app()->user->setState('activeStore', $activeStore);
        } else {
            $activeStore = $this->getDefaultStore();
        }
        return $activeStore;
    }

    public function switchStore(CatalogStoreSetting $store)
    {
        Yii::app()->user->setState('activeStore', $store);
    }

    public function formatCurrency($value)
    {
        //get active store
        $store = Yii::app()->catalogStoreSettingService->getActiveStore();
        $currency = CatalogCurrency::model()->findByPk($store->currency_id);
        
        $currency_symbol = $currency->symbol;
        $decimal_places = $store->decimal_places;
        $formatted_price = $currency_symbol.' '.number_format($value, $decimal_places, ",", ".");

        return $formatted_price;
    }

}
