<?php

Yii::import('trulek.cms.modules.catalog.models.*');

class CatalogService extends CApplicationComponent {

    public function formatPrice($price)
    {
        $activeStore = Yii::app()->catalogStoreSettingService->getActiveStore();
        $currency = CatalogCurrency::model()->findByPk($activeStore->currency_id);
        $currency_symbol = $currency->symbol;
        $decimal_places = $activeStore->decimal_places;
        $formatted_price = $currency_symbol.' '.number_format($price, $decimal_places);

        return $formatted_price;
    }

}
