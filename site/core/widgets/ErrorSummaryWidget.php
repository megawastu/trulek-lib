<?php

class ErrorSummaryWidget extends CWidget
{
    public $forms = array();
    public $errorSummary = '';

    public function init()
    {
       parent::init();
    }

    /**
     * Child Classes from Portlet must override this method to render its content
     * @method  renderContent
     */
    protected function renderContent()
    {
       $data = array();
       if (count($this->forms) > 0)
       {
           foreach ($this->forms as $form)
           {
              $this->errorSummary .= CHtml::errorSummary($form);
           }
           $data['errorSummary'] = $this->errorSummary;
       }
       $this->render(strtolower(__CLASS__), (isset($data) && count($data) > 0) ? $data : '');
    }

    public function run()
    {
        $this->renderContent();
    }
}


