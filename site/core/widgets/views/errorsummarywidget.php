<?php

if (isset($errorSummary) && $errorSummary != "")
{
    echo $errorSummary;
}

?>

<script type="text/javascript">
   jQuery(function(){
      jQuery('<div class="close"></div>').prependTo($(".errorSummary"));
      jQuery(".errorSummary .close").click(function(){
         jQuery(this).parent().remove();
      });
   });
</script>