<?php
   if (isset($menuItems) && count($menuItems) > 0) {
?>
<ul>
    <?php $i=1; $total=count($menuItems); foreach ($menuItems as $menuItem) { ?>
       <?php if ($menuItem->urlkey != "") { ?>
          <li class="<?php echo ($i == $total) ? ' last' : '' ?>">
             <?php echo CHtml::link($menuItem->title, array($menuItem->menu_link.'/'.$menuItem->urlkey)); ?>
          </li>
       <?php } else { ?>
          <li class="<?php echo ($i == $total) ? ' last' : '' ?>">
              <?php echo CHtml::link($menuItem->title, array($menuItem->menu_link.'/')); ?>
          </li>
       <?php } ?>
    <?php $i++; } ?>
</ul>
<?php } ?>
