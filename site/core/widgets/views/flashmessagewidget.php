<?php if (isset($this->message) && $this->message != "") { ?>
    <div class="message <?php echo $this->type; ?>">
       <div class="close"></div>
       <?php echo $this->message; ?>
    </div>
    <script type="text/javascript">
       jQuery(function(){
          $(".message .close").click(function(){
             $(this).parent().remove();
          });
       });
    </script>
<?php } ?>
