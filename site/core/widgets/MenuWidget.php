<?php

Yii::import('trulek.cms.core.menu.models.Menu');
Yii::import('trulek.cms.core.menu.models.MenuItem');

class MenuWidget extends CWidget {

    public $menuType;
    public $menu=null;
    public $recurseChildren=false;

    public function init() { parent::init(); }

    public function run() { $this->renderContent(); }

    protected function renderContent()
    {        
        $menu = Menu::model()->findByAttributes(array('unique_name' => $this->menuType));
        
        if ($this->menu != null)
        {
            $menu = $this->menu;
        }
        
        $menuItems = array();

        if ($menu != null) {
           $criteria = array();
           $condition = "`items`.`published`= (1)";
           if ($this->recurseChildren !== true) {
               $condition .= " AND `items`.`parent_id` IS NULL";
           }
                      
           $criteria['condition'] = $condition;
           $criteria['order'] = "`items`.`ordering` ASC";

           $menuItems = $menu->items($criteria);
        }
        
        $data['menu'] = $menu;
        $data['menuItems'] = $menuItems;

        $this->render(strtolower(__CLASS__), (isset($data) && count($data) > 0) ? $data : '');
    }
    
    protected function recursiveDisplayMenuItem($menuItems, $params=array())
    {
        $i = 1;
        $total = count($menuItems);
        
        $html = '<ul id="menu">';
        if (count($params) > 0) {
            $strParams='';
            foreach ($params as $attr => $value) {
                $strParams .= $attr.'="'.$value.'"';
            }
            $html = '<ul '.$strParams.'>';
        }
        
        foreach ($menuItems as $menuItem)
        {
            $url = Yii::app()->homeUrl . $menuItem->menu_link;
            if ($menuItem->urlkey != '') {
               $url .= '/'.$menuItem->urlkey;    
            }
            
            $class = ($i == $total) ? 'last' : '';
            $hasChildren = $this->_hasChildren($menuItem);
            $children = array();
            
            if ($hasChildren === true) {
                $criteria = new CDbCriteria();
                $criteria->condition = "`published` = (1)";
                $criteria->condition .= " AND `parent_id` = '".$menuItem->id."'";
                $criteria->order = "`ordering` ASC";
                $children = MenuItem::model()->findAll($criteria);
            }
            
            $html .= '<li class="'.$class.'">';
               $html .= '<a href="'.$url.'">'.Yii::t('system', $menuItem->title).'</a>';
               if ($children != null && count($children) > 0) {
                   $html .= $this->recursiveDisplayMenuItem($children);
               }
            $html .= '</li>';
            $i++;
        }
        
        $html .= '</ul>';
        
        return $html;
    }
    
    private function _hasChildren(MenuItem $menuItem)
    {
        $criteria = new CDbCriteria();
        $criteria->condition = "`parent_id` = '".$menuItem->id."'";
        $criteria->condition .= " AND `menu_id` = '".$menuItem->menu_id."'";
        $total = MenuItem::model()->count($criteria);
        
        if ($total > 0) {
            return true;
        }
        
        return false;
    }
}
