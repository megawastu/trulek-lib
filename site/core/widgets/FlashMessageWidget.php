<?php

class FlashMessageWidget extends CWidget
{    
    public $showTitle = false;
    public $message = '';
    public $type = 'success'; //success || error || warning || info

    public function init()
    {
        parent::init();
    }

    /**
     *
     * Child Classes from Portlet must override this method to render its content
     * @method  renderContent
     */
    protected function renderContent()
    {
       if(Yii::app()->user->hasFlash('success'))
       {
           $this->message = Yii::app()->user->getFlash('success');
           $this->type = 'success';
       }
       else if (Yii::app()->user->hasFlash('error'))
       {
           $this->message = Yii::app()->user->getFlash('error');
           $this->type = 'error';
       }
       else if (Yii::app()->user->hasFlash('info'))
       {
           $this->message = Yii::app()->user->getFlash('info');
           $this->type = 'info';
       }
       else if (Yii::app()->user->hasFlash('warning'))
       {
           $this->message = Yii::app()->user->getFlash('warning');
           $this->type = 'warning';
       }

       $this->render(strtolower(__CLASS__));
    }

    public function run()
    {
        $this->renderContent();
    }
}


