<?php

Yii::import('trulek.cms.core.menu.models.Menu');
Yii::import('trulek.cms.core.menu.models.MenuItem');

class SubmenuWidget extends CWidget
{
    public $urlkey;
    public $showHome=true;

    public function init() { parent::init(); }

    public function run() { $this->renderContent(); }

    protected function renderContent()
    {
        $menuItem = null;
        $menuItems = array();

        //get menu item
        $parentCriteria = new CDbCriteria();
        $parentCriteria->condition="`t`.`published`=(1)";
        $parentCriteria->condition.=" AND `t`.`urlkey`='".$this->urlkey."'";
        $menuItem = MenuItem::model()->findByAttributes(array('urlkey' => $this->urlkey));

        if ($menuItem != null) {
            //check for children
            //if it has children, store the parent in session
            $countCriteria = new CDbCriteria();
            $countCriteria->condition="`t`.`published`=(1)";
            $countCriteria->condition .= " AND `t`.`parent_id` = '".$menuItem->id."'";
            $totalChildren = MenuItem::model()->count($countCriteria);

            if ($totalChildren > 0) {
                Yii::app()->user->setState('parentMenuItem', $menuItem);
            } else {
                //before setting it to null, check if the menu has the same parent
                $sameParent=false;
                if (Yii::app()->user->hasState('parentMenuItem'))
                {
                    $sameParentCheckCriteria = new CDbCriteria();
                    $sameParentCheckCriteria->condition = "`t`.`published` = (1)";
                    $sameParentCheckCriteria->condition = "`t`.`parent_id` = '".Yii::app()->user->parentMenuItem->id."'";
                    $tempMenuItems = MenuItem::model()->findAll($sameParentCheckCriteria);
                    if ($tempMenuItems != null && count($tempMenuItems) > 0)
                    {
                        foreach ($tempMenuItems as $tempMenuItem) {
                            if ($tempMenuItem->id == $menuItem->id) {
                                $sameParent = true;
                            }
                        }
                    }
                }

                //set it to null when it has different parent
                if ($sameParent == false) {
                   Yii::app()->user->setState('parentMenuItem', null);
                }
            }

            if (Yii::app()->user->hasState('parentMenuItem')) {
                $childrenCriteria = new CDbCriteria();
                $childrenCriteria->condition="`t`.`published`=(1)";
                $childrenCriteria->condition.=" AND `t`.`parent_id` = '".Yii::app()->user->parentMenuItem->id."'";
                if ($this->showHome === true) {
                    $childrenCriteria->condition.=" OR `t`.`id` = '".Yii::app()->user->parentMenuItem->id."'";
                }
                $childrenCriteria->order = "`ordering` ASC";
                $menuItems = MenuItem::model()->findAll($childrenCriteria);
            }
        }
        
        
        $data['menuItems'] = $menuItems;

        $this->render(strtolower(__CLASS__), (isset($data) && count($data) > 0) ? $data : '');
    }
}
