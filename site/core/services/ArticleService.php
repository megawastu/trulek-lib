<?php

Yii::import('trulek.cms.core.article.models.Article');
Yii::import('trulek.cms.core.article.models.ArticleCategory');

class ArticleService extends CApplicationComponent {

    public function getArticleCategory($urlkey) {
        $modelInstance = null;
        $criteria = new CDbCriteria();
        $criteria->condition  = "`t`.`urlkey` = '".$urlkey."'";
        $criteria->condition .= " AND `t`.`published` = TRUE";
        $modelInstance = ArticleCategory::model()->find($criteria);
        return $modelInstance;
    }
    
    public function getArticle($urlkey) {
        $modelInstance = null;
        $criteria = new CDbCriteria();
        $criteria->condition  = "`t`.`urlkey` = '".$urlkey."'";
        $criteria->condition .= " AND `t`.`published` = TRUE";
        $modelInstance = Article::model()->find($criteria);
        return $modelInstance;
    }

    public function listArticles($category, $order="", $limit="", $offset="")
    {
        $modelInstances = array();
        
        $criteria = new CDbCriteria();
        $criteria->join = "INNER JOIN `article_category_articles` ON `article_category_articles`.`article_id` = `t`.`id`";
        $criteria->condition = "`t`.`published` = TRUE";
        $criteria->condition .= "`article_category_articles`.`article_category_id` = '".$category."'";
        
        if ($order != "") {
            $criteria->order = $order;
        }

        if ($limit != "") {
            $criteria->limit = $limit;
        }

        $modelInstances = Article::model()->findAll($criteria);

        return $modelInstances;
    }
}
