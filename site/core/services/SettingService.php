<?php

Yii::import('trulek.cms.core.setting.models.Setting');

class SettingService extends CApplicationComponent {

    public function getSetting()
    {
        $modelInstance = Setting::model()->last()->find();
        return $modelInstance;
    }

    public function getLogo()
    {
        $modelInstance = $this->getSetting();
        $filePath = Yii::app()->assetManager->baseUrl.'/'.Yii::app()->params['baseMediaFolder'].'images/no_image_'.Yii::app()->language.'.jpg';
        if ($modelInstance != null)
        {
            $filePath = Yii::app()->assetManager->baseUrl.'/'.Yii::app()->params['baseMediaFolder'].'setting/'.$modelInstance->logo;
        }
        return $filePath;
    }
}
