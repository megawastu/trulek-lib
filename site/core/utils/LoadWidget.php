<?php

Yii::import('trulek.cms.core.mywidget.models.MyWidget');
Yii::import('trulek.cms.core.template.models.TemplatePosition');
Yii::import('trulek.cms.core.menu.models.MenuItem');

class LoadWidget extends CWidget{
    
    public /**String**/ $position;
    public /**String**/ $urlkey = null;

    public function init() { parent::init(); }

    public function run() { $this->renderContent(); }

    protected function renderContent()
    {
        $templatePosition = TemplatePosition::model()->findByAttributes(array('position' => $this->position));
        
        $menuLink = $this->_parseMenuLink();        
        $menuItem = $this->_getMenuItem($menuLink, $this->urlkey);
        
        if ($menuItem != null)
        {
           $widgetCriteria = new CDbCriteria();
           $widgetCriteria->join        = "INNER JOIN `menu_item_widgets` ON `menu_item_widgets`.`widget_id` = `t`.`id`";
           $widgetCriteria->condition   = "`t`.`published` = '1'";
           $widgetCriteria->condition  .= " AND `t`.`position_id` = '".$templatePosition->id."'";
           $widgetCriteria->condition  .= " AND `menu_item_widgets`.`menu_item_id` = '".$menuItem->id."'";
           $widgetCriteria->order       = "`t`.`ordering` ASC";
           
           $widgets = MyWidget::model()->findAll($widgetCriteria);

           if ($widgets != null && count($widgets) > 0)
           {
               $this->_reflectWidgets($widgets);
           }
        }
    }

    private function _parseMenuLink()
    {
        $module = Yii::app()->controller->module;
        $controller = Yii::app()->controller;
        $action = Yii::app()->controller->action;
        $menuLink = null;

        if ($module == null) {
            if (Yii::app()->controller->action->id == "index") {
               $menuLink = $controller->id;
            } else {
               $menuLink = $controller->id . '/' . $action->id;
            }
        } else {
            if (Yii::app()->controller->action->id == "index") {
               $menuLink = $module->name . '/' . $controller->id;
            } else {
               $menuLink = $module->name . '/' . $controller->id . '/' . $action->id;
            }
        }

        return $menuLink;
    }

    private function _getMenuItem($menuLink, $urlkey=null)
    {
        //get menu item by menuLink
        $menuItemCriteria = new CDbCriteria();
        $menuItemCriteria->condition = "`menu_link`='".$menuLink."'";
        //if there is any urlkey assigned, get menu item by menuLink and also by urlkey
        if ($urlkey != null && $urlkey != "" && !empty($urlkey))
        {
            $menuItemCriteria->condition .= " AND `urlkey`='".$urlkey."'";
        }

        //if urlkey is null and action is not index, get menu item by controller
        if ($urlkey == null && Yii::app()->controller->action->id != "index") {
            $menuItemCriteria = new CDbCriteria();
            $menuItemCriteria->condition = "`menu_link`='".Yii::app()->controller->id."'";
        }
        
        $menuItem = MenuItem::model()->find($menuItemCriteria);

        return $menuItem;
    }

    private function _reflectWidgets($widgets=array())
    {
        if ($widgets != null && count($widgets) > 0) {
            foreach ($widgets as $myWidget)
            {
                $replaced = str_replace("_", " ", $myWidget->unique_name);
                $className = ucwords(strtolower($replaced));
                $className = str_replace(" ", "", $className);
                $classRef = new ReflectionClass($className);
                $widgetObject = $classRef->newInstance();
                $widgetObject->run();
            }
        }
    }    
}
